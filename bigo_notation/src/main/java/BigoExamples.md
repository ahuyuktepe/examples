### Constant Time Complexity Example
Example
```
IntFunction<Integer> function = (n) -> {
    int numberExecutions = 0;
    int m = n + 1;
    numberExecutions++;
    output.add("Given n: " + n);
    output.add("Number of Executions: " + numberExecutions);
    return m;
};
List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
for(Integer n: inputs) {
    function.apply(n);
}
```
Output
```
Given n: 5
Number of Executions: 1
Given n: 10
Number of Executions: 1
Given n: 100
Number of Executions: 1
Given n: 500
Number of Executions: 1
Given n: 10000
Number of Executions: 1
```
### Logarithmic Time Complexity Example
Example
```
IntFunction<Integer> function = (n) -> {
    int numberExecutions = 0;
    int m = 0;
    for(int i=1; i<=n; i = i * 2) {
        m += 1;
        numberExecutions++;
    }
    output.add("Given n: " + n);
    output.add("Number of Executions: " + numberExecutions);
    return m;
};
List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
for(Integer n: inputs) {
    function.apply(n);
}
```
Output
```
Given n: 5
Number of Executions: 3
Given n: 10
Number of Executions: 4
Given n: 100
Number of Executions: 7
Given n: 500
Number of Executions: 9
Given n: 10000
Number of Executions: 14
```
### Linear Time Complexity Example
Example
```
IntFunction<Integer> function = (n) -> {
    int numberExecutions = 0;
    int m = 0;
    for(int i=1; i<=n; i++) {
        m += 1;
        numberExecutions++;
    }
    output.add("Given n: " + n);
    output.add("Number of Executions: " + numberExecutions);
    return m;
};
List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
for(Integer n: inputs) {
    function.apply(n);
}
```
Output
```
Given n: 5
Number of Executions: 5
Given n: 10
Number of Executions: 10
Given n: 100
Number of Executions: 100
Given n: 500
Number of Executions: 500
Given n: 10000
Number of Executions: 10000
```
### Quadratic Time Complexity Example
Example
```
IntFunction<Integer> function = (n) -> {
    int numberExecutions = 0;
    int m = 0;
    for(int i=1; i<=n; i++) {
        for(int j=1; j<=n; j++) {
            m += 1;
            numberExecutions++;
        }
    }
    output.add("Given n: " + n);
    output.add("Number of Executions: " + numberExecutions);
    return m;
};
List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
for(Integer n: inputs) {
    function.apply(n);
}
```
Output
```
Given n: 5
Number of Executions: 25
Given n: 10
Number of Executions: 100
Given n: 100
Number of Executions: 10000
Given n: 500
Number of Executions: 250000
Given n: 10000
Number of Executions: 100000000
```
