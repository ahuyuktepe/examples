import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;

public class BigoExamples {

    public void exp_example1(List<String> output) {
        //summary: Constant Time Complexity Example
        IntFunction<Integer> function = (n) -> {
            int numberExecutions = 0;
            int m = n + 1;
            numberExecutions++;
            output.add("Given n: " + n);
            output.add("Number of Executions: " + numberExecutions);
            return m;
        };
        List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
        for(Integer n: inputs) {
            function.apply(n);
        }
    }//end

    public void exp_example2(List<String> output) {
        //summary: Logarithmic Time Complexity Example
        IntFunction<Integer> function = (n) -> {
            int numberExecutions = 0;
            int m = 0;
            for(int i=1; i<=n; i = i * 2) {
                m += 1;
                numberExecutions++;
            }
            output.add("Given n: " + n);
            output.add("Number of Executions: " + numberExecutions);
            return m;
        };
        List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
        for(Integer n: inputs) {
            function.apply(n);
        }
    }//end

    public void exp_example3(List<String> output) {
        //summary: Linear Time Complexity Example
        IntFunction<Integer> function = (n) -> {
            int numberExecutions = 0;
            int m = 0;
            for(int i=1; i<=n; i++) {
                m += 1;
                numberExecutions++;
            }
            output.add("Given n: " + n);
            output.add("Number of Executions: " + numberExecutions);
            return m;
        };
        List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
        for(Integer n: inputs) {
            function.apply(n);
        }
    }//end

    public void exp_example4(List<String> output) {
        //summary: Quadratic Time Complexity Example
        IntFunction<Integer> function = (n) -> {
            int numberExecutions = 0;
            int m = 0;
            for(int i=1; i<=n; i++) {
                for(int j=1; j<=n; j++) {
                    m += 1;
                    numberExecutions++;
                }
            }
            output.add("Given n: " + n);
            output.add("Number of Executions: " + numberExecutions);
            return m;
        };
        List<Integer> inputs = Arrays.asList(5, 10, 100, 500, 10000);
        for(Integer n: inputs) {
            function.apply(n);
        }
    }//end


}