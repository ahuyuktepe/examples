### Write Given String To File
Example
```
        String text = "This is a test file";
        OutputStream outputStream = new FileOutputStream("resources/output-stream-file.txt");
        byte[] bytesArr = text.getBytes(StandardCharsets.UTF_16);
        outputStream.write(bytesArr);
    }

```
Output
```
```
### Writing Object To File
Example
```
        //resources: resources.Vehicle
        // Writing Vehicle Type Object To File
        Vehicle vehicle = new Vehicle("Toyota", "Corolla");
        output.add("Source Object: " + vehicle);
        FileOutputStream fileOutputStream = new FileOutputStream("resources/output-stream-file.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(vehicle);
    }
```
Output
```
Source Object: Vehicle{ make='Toyota, model='Corolla}
```
