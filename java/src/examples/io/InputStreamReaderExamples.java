package examples.io;

import java.io.*;
import java.nio.Buffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class InputStreamReaderExamples {
    public void exp_inputStreamReader_1(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read Characters From File
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        char [] charactersInFile = new char[7];
        inputStreamReader.read(charactersInFile);
        String content = String.valueOf(charactersInFile);
        String charset = inputStreamReader.getEncoding();
        output.add("File Content("+charset+"): " + content);
    }

    public void exp_inputStreamReader_2(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read Characters From Byte Array As ASCII
        byte [] bytesArr = new byte[] { 0x21, 0x2F, 0x3F, 0x42, 0x44, 0x67 };
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytesArr);
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.US_ASCII);
        char [] characters = new char[5];
        reader.read(characters);
        String text = "";
        for(char val: characters) {
            String str = String.valueOf(val);
            text += str + " ";
        }
        output.add(text);
    }

    public void exp_inputStreamReader_3(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read Characters From Byte Array As UTF16
        byte [] bytesArr = new byte[] { 0x21, 0x2F, 0x3F, 0x42, 0x44, 0x67 };
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytesArr);
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_16);
        char [] characters = new char[6];
        reader.read(characters);
        String text = "";
        for(char val: characters) {
            String str = String.valueOf(val);
            text += str + " ";
        }
        output.add(text);
    }

    public void exp_bufferedReader(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read Characters via BufferedReader From Byte Array As UTF8
        byte [] bytesArr = new byte[] { 0x21, 0x2F, 0x3F, 0x42, 0x44, 0x67 };
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytesArr);
        InputStreamReader reader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        char [] characters = new char[6];
        bufferedReader.read(characters);
        String str = String.valueOf(characters);
        output.add("UTF 8 Encoded Characters: " + str);
    }

    public void exp_displayEncoding(List<String> output) throws IOException, FileNotFoundException {
        //summary: Display CharSet Used In Encoding
        byte[] bytesArr = new byte[]{0x21, 0x2F, 0x3F, 0x42, 0x44, 0x67};
        InputStream inputStream = new ByteArrayInputStream(bytesArr);
        // UTF8 Encoding
        InputStreamReader utf8Reader = new InputStreamReader(inputStream);
        output.add("UTF8 Reader Charset: " + utf8Reader.getEncoding());
        char[] chars = new char[6];
        utf8Reader.read(chars);
        String str = String.valueOf(chars);
        output.add("UTF8 Encoded Characters: " + str);
        // UTF16 Encoding
        String srcStr = "This is a test";
        bytesArr = srcStr.getBytes(StandardCharsets.UTF_16);
        char [] characters = new char[10];
        inputStream = new ByteArrayInputStream(bytesArr);
        InputStreamReader utf16Reader = new InputStreamReader(inputStream, StandardCharsets.UTF_16);
        output.add("UTF16 Reader Charset: " + utf16Reader.getEncoding());
        utf16Reader.read(characters);
        str = String.valueOf(characters);
        output.add("UTF16 Encoded Characters: " + str);
        // ISO_8859_1 Encoding
        bytesArr = new byte[]{0x21, 0x2F, 0x3F, 0x42, 0x44, 0x67};
        inputStream = new ByteArrayInputStream(bytesArr);
    }


}
