### Writing Given Encoded String To File
Example
```
        String text = "This is a test";
        byte[] bytesArr = text.getBytes(StandardCharsets.ISO_8859_1.name());
        OutputStream outputStream = new FileOutputStream("resources/output-stream-file.txt");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStream.write(bytesArr);
    }
```
Output
```
```
