package examples.io;

import classes.Vehicle;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class OutputStreamExamples {

    public void exp_fileOutputStream(List<String> output) throws IOException, FileNotFoundException {
        //summary: Write Given String To File
        String text = "This is a test file";
        OutputStream outputStream = new FileOutputStream("resources/output-stream-file.txt");
        byte[] bytesArr = text.getBytes(StandardCharsets.UTF_16);
        outputStream.write(bytesArr);
    }

    public void exp_objectOutputStream(List<String> output) throws IOException, ClassNotFoundException {
        //summary: Writing Object To File
        //resources: classes.Vehicle
        // Writing Vehicle Type Object To File
        Vehicle vehicle = new Vehicle("Toyota", "Corolla");
        output.add("Source Object: " + vehicle);
        FileOutputStream fileOutputStream = new FileOutputStream("resources/output-stream-file.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(vehicle);
    }
}
