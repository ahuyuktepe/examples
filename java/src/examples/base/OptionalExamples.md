### Optional Initialize With Non-Null Object
Example
```
Optional<String> optionalStr = Optional.of("This a test");
if(optionalStr.isPresent()) {
output.add("Optional object has value of '" + optionalStr.get() + "'.");
} else {
output.add("Optional object has null value.");
```
Output
```
Optional object has value of 'This a test'.
```
### Optional Initialize With Null Object
Example
```
Optional<String> optionalStr = Optional.ofNullable(null);
if(optionalStr.isEmpty()) {
output.add("Optional object has null value.");
} else {
output.add("Optional object has value of '" + optionalStr.get() + "'.");
```
Output
```
Optional object has null value.
```
### IfPresent Run Action
Example
```
Consumer<String> printValue = val -> {
output.add("Value: " + val);
};
Optional<String> optionalStr = Optional.of("This is a test");
optionalStr.ifPresent(printValue);
```
Output
```
Value: This is a test
```
### orElse Method
Example
```
// Testing Optional With Non-Null Value
Optional<String> optionalStr = Optional.of("This a test");
String value = optionalStr.orElse("Default string value");
output.add("Value: " + value);
// Testing Optional With Null Value
optionalStr = Optional.ofNullable(null);
value = optionalStr.orElse("Default string value");
output.add("Value: " + value);
```
Output
```
Value: This a test
Value: Default string value
```
### orElseThrow Method
Example
```
try {
Optional<String> optionalStr = Optional.of("This is a test");
String value = optionalStr.orElseThrow();
output.add("Value: " + value);
optionalStr = Optional.ofNullable(null);
value = optionalStr.orElseThrow();
} catch(NoSuchElementException exception) {
output.add(exception.getMessage());
```
Output
```
Value: This is a test
No value present
```
### orElseGet Method
Example
```
Supplier<String> valueSupplier = () -> {
return "This is a test";
};
Optional<String> optionalStr = Optional.ofNullable(null);
String value = optionalStr.orElseGet(valueSupplier);
output.add("Value: " + value);
```
Output
```
Value: This is a test
```
