package examples.function;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class PredicateExamples {
    public void exp_example1(List<String> output) throws IOException {
        //summary: Initialization Example
        Predicate<String> endWithN = (value) -> {
            return value.endsWith("n");
        };
        Stream<String> allNames = Arrays.asList("George", "John", "Tom", "Ellen").stream();
        Stream<String> filteredNames = allNames.filter(endWithN);
        filteredNames.forEach((name) -> output.add(name));
    }

    public void exp_example2(List<String> output) throws IOException {
        //summary: and Method Example
        Stream<String> allNames = Arrays.asList("George", "John", "Tom", "Ellen").stream();
        Predicate<String> endWithN = (value) -> {
            return value.endsWith("n");
        };
        Predicate<String> startsWithEandEndsWithN = endWithN.and((value) -> {
            return value.startsWith("E");
        });
        Stream<String> filteredNames = allNames.filter(startsWithEandEndsWithN);
        filteredNames.forEach((name) -> output.add(name));
    }

    public void exp_example3(List<String> output) throws IOException {
        //summary: or Method Example
        Stream<String> allNames = Arrays.asList("George", "John", "Tom", "Ellen").stream();
        Predicate<String> endWithN = (value) -> {
            return value.endsWith("n");
        };
        Predicate<String> startsWithEOrEndsWithN = endWithN.or((value) -> {
            return value.startsWith("E");
        });
        Stream<String> filteredNames = allNames.filter(startsWithEOrEndsWithN);
        filteredNames.forEach((name) -> output.add(name));
    }

    public void exp_example4(List<String> output) throws IOException {
        //summary: negate Method Example
        Stream<String> allNames = Arrays.asList("George", "John", "Tom", "Ellen").stream();
        Predicate<String> endWithN = (value) -> {
            return value.endsWith("n");
        };
        Predicate<String> notStartsWithEOrEndsWithN = endWithN.or((value) -> {
            return value.startsWith("E");
        }).negate();
        Stream<String> filteredNames = allNames.filter(notStartsWithEOrEndsWithN);
        filteredNames.forEach((name) -> output.add(name));
    }

    public void exp_example5(List<String> output) throws IOException {
        //summary: negate Method Example
        Stream<String> allNames = Arrays.asList("George", "John", "Tom", "Ellen").stream();
        Predicate<String> endWithN = (value) -> {
            return value.endsWith("n");
        };
        boolean doesEndWithN = endWithN.test("Ellen");
        output.add("Does 'Ellen' end with 'n': " + doesEndWithN);
    }
}