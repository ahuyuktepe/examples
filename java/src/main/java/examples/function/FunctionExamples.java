package examples.function;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public class FunctionExamples {

    public void exp_example1(List<String> output) throws IOException {
        //summary: Method apply Example
        Function<String, String> addRedColor = (String carDescription) -> {
            return carDescription + "(red)";
        };
        String carDesc = "Toyota Corolla";
        String redCarDesc = addRedColor.apply(carDesc);
        output.add("Car Description: " + carDesc);
        output.add("Red Car Description: " + redCarDesc);
    }

    public void exp_example2(List<String> output) throws IOException {
        //summary: Method andThen Example
        Function<String, String> addRedColor = (String carDescription) -> {
            return carDescription + "(red)";
        };
        Function<String, String> addRedWhiteBlueColor = addRedColor.andThen((desc) -> {return desc + "(blue)";});
        String carDesc = "Toyota Corolla";
        output.add("Car Description: " + carDesc);
        String coloredDesc = addRedWhiteBlueColor.apply(carDesc);
        output.add("Colored Car Description: " + coloredDesc);
    }

    public void exp_example3(List<String> output) throws IOException {
        //summary: Method compose Example
        Function<String, String> addRedColor = (String carDescription) -> {
            return carDescription + "(red)";
        };
        Function<String, String> addRedWhiteBlueColor = addRedColor.compose((desc) -> {return desc + "(blue)";});
        String carDesc = "Toyota Corolla";
        output.add("Car Description: " + carDesc);
        String coloredDesc = addRedWhiteBlueColor.apply(carDesc);
        output.add("Colored Car Description: " + coloredDesc);
    }
}
