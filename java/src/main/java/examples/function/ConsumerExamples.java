package examples.function;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExamples {

    public void exp_example1(List<String> output) throws IOException {
        //summary: Method accept Example
        Consumer<String> addRed = (value) -> {
            output.add(value + "(red)");
        };
        addRed.accept("Color: ");
    }

    public void exp_example2(List<String> output) throws IOException {
        //summary: Method andThen Example
        Consumer<String> addRed = (value) -> {
            output.add(value + "(red)");
        };
        Consumer<String> addRedAndBlue = addRed.andThen((value) -> {
            output.add(value + "(blue)");
        });
        addRedAndBlue.accept("Color: ");
    }
}
