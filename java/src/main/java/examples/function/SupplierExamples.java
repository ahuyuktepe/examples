package examples.function;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class SupplierExamples {
    public void exp_example1(List<String> output) throws IOException {
        //summary: Method get() Example
        Supplier<List<String>> valueSupplier = () -> {
            return Arrays.asList("Test10", "Test11", "Test12", "Test13");
        };
        List<String> values = valueSupplier.get();
        values.forEach((value) -> output.add(value));
    }

}
