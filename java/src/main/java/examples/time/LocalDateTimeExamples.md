### LocalTime Initialization
classes.Example
```
LocalDateTime localDateTime = LocalDateTime.now();
output.add("Now: " + localDateTime);
// Custom LocalDateTime format
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
localDateTime = LocalDateTime.parse("2021-01-01 10:02:22", formatter);
output.add("Parsed LocalDateTime: " + localDateTime);
// Build LocalDateTime from LocalDate
LocalDate localDate = LocalDate.now();
LocalTime localTime = LocalTime.now();
localDateTime = LocalDateTime.of(localDate, localTime);
output.add("Merged LocalDateTime: " + localDateTime);
```
Output
```
Now: 2021-04-04T20:18:26.685109300
Parsed LocalDateTime: 2021-01-01T10:02:22
Merged LocalDateTime: 2021-04-04T20:18:26.726085500
```
### Unix Timestamp Generation
classes.Example
```
LocalDateTime localDateTime = LocalDateTime.now();
output.add("Now: " + localDateTime);
ZoneOffset zoneOffset = ZoneOffset.ofTotalSeconds(0);
long timeStamp = localDateTime.toEpochSecond(zoneOffset);
output.add("Timestamp: " + timeStamp);
```
Output
```
Now: 2021-04-04T20:18:26.727084900
Timestamp: 1617567506
```
