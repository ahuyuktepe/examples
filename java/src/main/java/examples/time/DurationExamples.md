### Duration Initialization 1
classes.Example
```
LocalDateTime fromDateTime = LocalDateTime.now();
LocalDateTime toDateTime = fromDateTime.plusHours(3);
Duration diff = Duration.between(fromDateTime, toDateTime);
output.add("Difference From " + fromDateTime + " - " + toDateTime + " In Given Temporal Units");
output.add("In Hours: " + diff.toHours() + " hours.");
output.add("In Minutes: " + diff.toMinutes() + " minutes.");
output.add("In Seconds: " + diff.toSeconds() + " seconds.");
output.add("In Seconds: " + diff.toMillis() + " milli-seconds.");
output.add("In Nano-Seconds: " + diff.toNanos() + " nano-seconds.");
```
Output
```
Difference From 2021-04-04T20:17:57.124007900 - 2021-04-04T23:17:57.124007900 In Given Temporal Units
In Hours: 3 hours.
In Minutes: 180 minutes.
In Seconds: 10800 seconds.
In Seconds: 10800000 milli-seconds.
In Nano-Seconds: 10800000000000 nano-seconds.
```
### Duration Initialization 2
Duration is converted to string in given format(PTnHnMnS): S: seconds, M: minutes, H: hours

classes.Example
```
Duration fiveDays = Duration.ofDays(5);
output.add("5 days: " + fiveDays);
Duration threeHours = Duration.ofHours(3);
output.add("3 hours: " + threeHours);
Duration tenMinutes = Duration.ofMinutes(10);
output.add("10 minutes: " + tenMinutes);
```
Output
```
5 days: PT120H
3 hours: PT3H
10 minutes: PT10M
```
### Date Initialization 3
classes.Example
```
Duration oneDay = Duration.parse("PT24H");
output.add("1 day: " + oneDay);
output.add("1 day in hours: " + oneDay.toHours());
```
Output
```
1 day: PT24H
1 day in hours: 24
```
### Duration Conversion
classes.Example
```
Duration fiveDays = Duration.ofDays(5);
output.add("5 days: " + fiveDays);
long fiveDaysInSeconds = fiveDays.toSeconds();
output.add("5 days in seconds: " + fiveDaysInSeconds);
long fiveDaysInMilliseconds = fiveDays.toMillis();
output.add("5 days in milliseconds: " + fiveDaysInMilliseconds);
```
Output
```
5 days: PT120H
5 days in seconds: 432000
5 days in milliseconds: 432000000
```
### Adding To Duration
classes.Example
```
Duration fiveDays = Duration.ofDays(5);
output.add("5 days: " + fiveDays);
Duration fiveDaysAnd3Hours = fiveDays.plusHours(3);
output.add("5 days + 3 hours: " + fiveDaysAnd3Hours);
Duration sevenDays = fiveDays.plusDays(2);
output.add("5 days + 2 days: " + sevenDays);
Duration fiveDaysAnd20Mins = fiveDays.plusMinutes(20);
output.add("5 days + 20 minutes: " + fiveDaysAnd20Mins);
```
Output
```
5 days: PT120H
5 days + 3 hours: PT123H
5 days + 2 days: PT168H
5 days + 20 minutes: PT120H20M
```
### Subtracting From Duration
classes.Example
```
Duration fiveDays = Duration.ofDays(5);
output.add("5 days: " + fiveDays);
Duration fiveDaysMinus3Hours = fiveDays.minusHours(3);
output.add("5 days - 3 hours: " + fiveDaysMinus3Hours);
Duration fiveDaysMinus2Days = fiveDays.minusDays(2);
output.add("5 days - 2 days: " + fiveDaysMinus2Days);
Duration fiveDayMinus20Mins = fiveDays.minusMinutes(20);
output.add("5 days - 20 minutes: " + fiveDayMinus20Mins);
```
Output
```
5 days: PT120H
5 days - 3 hours: PT117H
5 days - 2 days: PT72H
5 days - 20 minutes: PT119H40M
```
