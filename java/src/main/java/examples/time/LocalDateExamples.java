package examples.time;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class LocalDateExamples {
    public void exp_init(List<String> output) {
        //summary: LocalDate Initialization
        LocalDate localDate = LocalDate.now();
        output.add("Local Date: " + localDate);
    }

    public void exp_parse(List<String> output) {
        try {
            //summary: From String To LocalDate
            String dateStr = "2021-01-01";
            LocalDate localDate = LocalDate.parse(dateStr);
            output.add("LocalDate: " + localDate);
        } catch(DateTimeException exception) {
            exception.printStackTrace();
        }
    }

    public void exp_parse_1(List<String> output) {
        try {
            //summary: From String To LocalDate With Formatter
            //description: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html
            String dateStr = "12-03-2020";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
            LocalDate localDate = LocalDate.parse(dateStr, formatter);
            output.add("LocalDate: " + localDate);
        } catch(DateTimeException exception) {
            exception.printStackTrace();
        }
    }

    public void exp_dateToStr(List<String> output) {
        //summary: From LocalDate Instance To String
        //description: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html
        LocalDate today = LocalDate.parse("2021-01-01", DateTimeFormatter.ISO_LOCAL_DATE);
        output.add("Today: " + today);
        // BASIC_ISO_DATE format
        DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
        output.add("BASIC_ISO_DATE Format: " + today.format(formatter));
        // ISO_LOCAL_DATE format
        formatter = DateTimeFormatter.ISO_LOCAL_DATE;
        output.add("ISO_LOCAL_DATE Format: " + today.format(formatter));
        // "MM-dd-yyyy" format
        formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        output.add("MM-dd-yyyyy: " + today.format(formatter));
        // "M-d-yy" format
        formatter = DateTimeFormatter.ofPattern("M-d-yy");
        output.add("M-d-yy: " + today.format(formatter));
    }

    public void exp_plus(List<String> output) {
        //summary: Plus Methods
        LocalDate today = LocalDate.now();
        output.add("Today: " + today);
        // Get copy of LocalDate instance with a day added
        LocalDate updatedDate = today.plusDays(1);
        output.add("Today + 1 day: " + updatedDate);
        // Get copy of LocalDate instance with 2 months added
        updatedDate = today.plusMonths(2);
        output.add("Today + 2 months: " + updatedDate);
        // Get copy of LocalDate instance with 3 years added
        updatedDate = today.plusYears(3);
        output.add("Today + 3 years: " + updatedDate);
    }

    public void exp_minus(List<String> output) {
        //summary: Minus Methods
        LocalDate today = LocalDate.now();
        output.add("Today: " + today);
        // Get copy of LocalDate instance with a day removed
        LocalDate updatedDate = today.minusDays(1);
        output.add("Today - 1 day: " + updatedDate);
        // Get copy of LocalDate instance with 2 months removed
        updatedDate = today.minusMonths(2);
        output.add("Today - 2 months: " + updatedDate);
        // Get copy of LocalDate instance with 3 years removed
        updatedDate = today.minusYears(3);
        output.add("Today - 3 years: " + updatedDate);
    }

    public void exp_compare(List<String> output) {
        //summary: Compare LocalDate Instances
        LocalDate date1 = LocalDate.parse("2020-01-01");
        LocalDate date2 = LocalDate.parse("2021-01-01");
        output.add("date1: " + date1);
        output.add("date2: " + date2);
        output.add("date1 is before date2: " + date1.isBefore(date2));
        output.add("date1 is after date2: " + date1.isAfter(date2));
    }

    public void exp_fromDateToDateTime(List<String> output) {
        //summary: From LocalDate To LocalDateTime
        LocalDate today = LocalDate.now();
        output.add("Today: " + today);
        // Hour and minute
        LocalDateTime localDateTime = today.atTime(14, 26);
        output.add("Today at 14:26" + localDateTime);
        // Hour, minute and seconds
        localDateTime = today.atTime(14, 26, 56);
        output.add("Today at 14:26:56" + localDateTime);
        // Add now time to date
        LocalTime now = LocalTime.now();
        LocalDateTime updatedDateTime = today.atTime(now);
        output.add("Today + now: " + updatedDateTime);
    }

}
