### Instant Initialization 1
classes.Example
```
Instant now = Instant.now();
output.add("Now:" + now);
output.add("Now in milliseconds: " + now.toEpochMilli());
```
Output
```
Now:2021-04-05T00:18:08.440569800Z
Now in milliseconds: 1617581888440
```
### Instant Initialization 2
classes.Example
```
Instant now = Instant.now();
output.add("Now:" + now);
output.add("Now in milliseconds: " + now.toEpochMilli());
```
Output
```
Now:2021-04-05T00:18:08.494539Z
Now in milliseconds: 1617581888494
```
### Instant Initialization From Milliseconds
classes.Example
```
Duration duration = Duration.ofDays(3);
long epochMilliSecondsOfDuration = duration.toMillis();
Instant instant = Instant.ofEpochMilli(epochMilliSecondsOfDuration);
output.add("Now:" + instant);
output.add("Now in milliseconds: " + epochMilliSecondsOfDuration);
```
Output
```
Now:1970-01-04T00:00:00Z
Now in milliseconds: 259200000
```
### Instant Initialization From Date String
classes.Example
```
Instant instant = Instant.parse("2021-01-10T00:00:00Z");
output.add("Instant:" + instant);
output.add("Instant in milliseconds: " + instant.toEpochMilli());
```
Output
```
Instant:2021-01-10T00:00:00Z
Instant in milliseconds: 1610236800000
```
### Instant Comparison
classes.Example
```
Instant instant1 = Instant.parse("2021-01-10T00:00:00Z");
Instant instant2 = Instant.parse("2021-01-12T00:00:00Z");
output.add("Instant 1:" + instant1);
output.add("Instant 2:" + instant2);
output.add("Instant 1 Is Before Instant 2: " + instant1.isBefore(instant2));
output.add("Instant 1 Is After Instant 2: " + instant1.isAfter(instant2));
```
Output
```
Instant 1:2021-01-10T00:00:00Z
Instant 2:2021-01-12T00:00:00Z
Instant 1 Is Before Instant 2: true
Instant 1 Is After Instant 2: false
```
### Instant Subtraction
classes.Example
```
Instant instant1 = Instant.parse("2021-01-10T00:00:00Z");
Instant instant2 = instant1.minusSeconds(40);
output.add("Instant:" + instant1);
output.add("Instant - 40 seconds: " + instant2);
```
Output
```
Instant:2021-01-10T00:00:00Z
Instant - 40 seconds: 2021-01-09T23:59:20Z
```
### Instant Addition
classes.Example
```
Instant instant1 = Instant.parse("2021-01-10T00:00:00Z");
Instant instant2 = instant1.plusSeconds(40);
output.add("Instant:" + instant1);
output.add("Instant + 40 seconds: " + instant2);
```
Output
```
Instant:2021-01-10T00:00:00Z
Instant + 40 seconds: 2021-01-10T00:00:40Z
```
