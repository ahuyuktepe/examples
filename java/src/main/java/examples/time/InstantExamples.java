package examples.time;

import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

public class InstantExamples {
    public void exp_init_1(List<String> output) {
        //summary: Instant Initialization 1
        Instant now = Instant.now();
        output.add("Now:" + now);
        output.add("Now in milliseconds: " + now.toEpochMilli());
    }

    public void exp_init_2(List<String> output) {
        //summary: Instant Initialization 2
        Instant now = Instant.now();
        output.add("Now:" + now);
        output.add("Now in milliseconds: " + now.toEpochMilli());
    }

    public void exp_init_3(List<String> output) {
        //summary: Instant Initialization From Milliseconds
        Duration duration = Duration.ofDays(3);
        long epochMilliSecondsOfDuration = duration.toMillis();
        Instant instant = Instant.ofEpochMilli(epochMilliSecondsOfDuration);
        output.add("Now:" + instant);
        output.add("Now in milliseconds: " + epochMilliSecondsOfDuration);
    }

    public void exp_init_4(List<String> output) {
        //summary: Instant Initialization From Date String
        Instant instant = Instant.parse("2021-01-10T00:00:00Z");
        output.add("Instant:" + instant);
        output.add("Instant in milliseconds: " + instant.toEpochMilli());
    }

    public void exp_comparison(List<String> output) {
        //summary: Instant Comparison
        Instant instant1 = Instant.parse("2021-01-10T00:00:00Z");
        Instant instant2 = Instant.parse("2021-01-12T00:00:00Z");
        output.add("Instant 1:" + instant1);
        output.add("Instant 2:" + instant2);
        output.add("Instant 1 Is Before Instant 2: " + instant1.isBefore(instant2));
        output.add("Instant 1 Is After Instant 2: " + instant1.isAfter(instant2));
    }

    public void exp_minus(List<String> output) {
        //summary: Instant Subtraction
        Instant instant1 = Instant.parse("2021-01-10T00:00:00Z");
        Instant instant2 = instant1.minusSeconds(40);
        output.add("Instant:" + instant1);
        output.add("Instant - 40 seconds: " + instant2);
    }

    public void exp_plus(List<String> output) {
        //summary: Instant Addition
        Instant instant1 = Instant.parse("2021-01-10T00:00:00Z");
        Instant instant2 = instant1.plusSeconds(40);
        output.add("Instant:" + instant1);
        output.add("Instant + 40 seconds: " + instant2);
    }

}
