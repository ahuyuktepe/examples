### LocalDate Initialization
classes.Example
```
LocalDate localDate = LocalDate.now();
output.add("Local Date: " + localDate);
```
Output
```
Local Date: 2021-04-04
```
### From String To LocalDate
classes.Example
```
try {
String dateStr = "2021-01-01";
LocalDate localDate = LocalDate.parse(dateStr);
output.add("LocalDate: " + localDate);
} catch(DateTimeException exception) {
exception.printStackTrace();
```
Output
```
LocalDate: 2021-01-01
```
### From String To LocalDate With Formatter
https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html

classes.Example
```
try {
String dateStr = "12-03-2020";
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
LocalDate localDate = LocalDate.parse(dateStr, formatter);
output.add("LocalDate: " + localDate);
} catch(DateTimeException exception) {
exception.printStackTrace();
```
Output
```
LocalDate: 2020-12-03
```
### From LocalDate Instance To String
https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html

classes.Example
```
LocalDate today = LocalDate.parse("2021-01-01", DateTimeFormatter.ISO_LOCAL_DATE);
output.add("Today: " + today);
// BASIC_ISO_DATE format
DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
output.add("BASIC_ISO_DATE Format: " + today.format(formatter));
// ISO_LOCAL_DATE format
formatter = DateTimeFormatter.ISO_LOCAL_DATE;
output.add("ISO_LOCAL_DATE Format: " + today.format(formatter));
// "MM-dd-yyyy" format
formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
output.add("MM-dd-yyyyy: " + today.format(formatter));
// "M-d-yy" format
formatter = DateTimeFormatter.ofPattern("M-d-yy");
output.add("M-d-yy: " + today.format(formatter));
```
Output
```
Today: 2021-01-01
BASIC_ISO_DATE Format: 20210101
ISO_LOCAL_DATE Format: 2021-01-01
MM-dd-yyyyy: 01-01-2021
M-d-yy: 1-1-21
```
### Plus Methods
classes.Example
```
LocalDate today = LocalDate.now();
output.add("Today: " + today);
// Get copy of LocalDate instance with a day added
LocalDate updatedDate = today.plusDays(1);
output.add("Today + 1 day: " + updatedDate);
// Get copy of LocalDate instance with 2 months added
updatedDate = today.plusMonths(2);
output.add("Today + 2 months: " + updatedDate);
// Get copy of LocalDate instance with 3 years added
updatedDate = today.plusYears(3);
output.add("Today + 3 years: " + updatedDate);
```
Output
```
Today: 2021-04-04
Today + 1 day: 2021-04-05
Today + 2 months: 2021-06-04
Today + 3 years: 2024-04-04
```
### Minus Methods
classes.Example
```
LocalDate today = LocalDate.now();
output.add("Today: " + today);
// Get copy of LocalDate instance with a day removed
LocalDate updatedDate = today.minusDays(1);
output.add("Today - 1 day: " + updatedDate);
// Get copy of LocalDate instance with 2 months removed
updatedDate = today.minusMonths(2);
output.add("Today - 2 months: " + updatedDate);
// Get copy of LocalDate instance with 3 years removed
updatedDate = today.minusYears(3);
output.add("Today - 3 years: " + updatedDate);
```
Output
```
Today: 2021-04-04
Today - 1 day: 2021-04-03
Today - 2 months: 2021-02-04
Today - 3 years: 2018-04-04
```
### Compare LocalDate Instances
classes.Example
```
LocalDate date1 = LocalDate.parse("2020-01-01");
LocalDate date2 = LocalDate.parse("2021-01-01");
output.add("date1: " + date1);
output.add("date2: " + date2);
output.add("date1 is before date2: " + date1.isBefore(date2));
output.add("date1 is after date2: " + date1.isAfter(date2));
```
Output
```
date1: 2020-01-01
date2: 2021-01-01
date1 is before date2: true
date1 is after date2: false
```
### From LocalDate To LocalDateTime
classes.Example
```
LocalDate today = LocalDate.now();
output.add("Today: " + today);
// Hour and minute
LocalDateTime localDateTime = today.atTime(14, 26);
output.add("Today at 14:26" + localDateTime);
// Hour, minute and seconds
localDateTime = today.atTime(14, 26, 56);
output.add("Today at 14:26:56" + localDateTime);
// Add now time to date
LocalTime now = LocalTime.now();
LocalDateTime updatedDateTime = today.atTime(now);
output.add("Today + now: " + updatedDateTime);
```
Output
```
Today: 2021-04-04
Today at 14:262021-04-04T14:26
Today at 14:26:562021-04-04T14:26:56
Today + now: 2021-04-04T20:18:18.090039300
```
