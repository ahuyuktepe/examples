package examples.time;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class LocalDateTimeExamples {
    public void exp_initTime(List<String> output) {
        //summary: LocalTime Initialization
        LocalDateTime localDateTime = LocalDateTime.now();
        output.add("Now: " + localDateTime);
        // Custom LocalDateTime format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        localDateTime = LocalDateTime.parse("2021-01-01 10:02:22", formatter);
        output.add("Parsed LocalDateTime: " + localDateTime);
        // Build LocalDateTime from LocalDate
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        localDateTime = LocalDateTime.of(localDate, localTime);
        output.add("Merged LocalDateTime: " + localDateTime);
    }

    public void exp_epochTime(List<String> output) {
        //summary: Unix Timestamp Generation
        LocalDateTime localDateTime = LocalDateTime.now();
        output.add("Now: " + localDateTime);
        ZoneOffset zoneOffset = ZoneOffset.ofTotalSeconds(0);
        long timeStamp = localDateTime.toEpochSecond(zoneOffset);
        output.add("Timestamp: " + timeStamp);
    }

}
