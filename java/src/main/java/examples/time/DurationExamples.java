package examples.time;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class DurationExamples {
    public void exp_init_1(List<String> output) {
        //summary: Duration Initialization 1
        LocalDateTime fromDateTime = LocalDateTime.now();
        LocalDateTime toDateTime = fromDateTime.plusHours(3);
        Duration diff = Duration.between(fromDateTime, toDateTime);
        output.add("Difference From " + fromDateTime + " - " + toDateTime + " In Given Temporal Units");
        output.add("In Hours: " + diff.toHours() + " hours.");
        output.add("In Minutes: " + diff.toMinutes() + " minutes.");
        output.add("In Seconds: " + diff.toSeconds() + " seconds.");
        output.add("In Seconds: " + diff.toMillis() + " milli-seconds.");
        output.add("In Nano-Seconds: " + diff.toNanos() + " nano-seconds.");
    }

    public void exp_init_2(List<String> output) {
        //summary: Duration Initialization 2
        //description: Duration is converted to string in given format(PTnHnMnS): S: seconds, M: minutes, H: hours
        Duration fiveDays = Duration.ofDays(5);
        output.add("5 days: " + fiveDays);
        Duration threeHours = Duration.ofHours(3);
        output.add("3 hours: " + threeHours);
        Duration tenMinutes = Duration.ofMinutes(10);
        output.add("10 minutes: " + tenMinutes);
    }

    public void exp_init_3(List<String> output) {
        //summary: Date Initialization 3
        Duration oneDay = Duration.parse("PT24H");
        output.add("1 day: " + oneDay);
        output.add("1 day in hours: " + oneDay.toHours());
    }

    public void exp_conversion(List<String> output) {
        //summary: Duration Conversion
        Duration fiveDays = Duration.ofDays(5);
        output.add("5 days: " + fiveDays);
        long fiveDaysInSeconds = fiveDays.toSeconds();
        output.add("5 days in seconds: " + fiveDaysInSeconds);
        long fiveDaysInMilliseconds = fiveDays.toMillis();
        output.add("5 days in milliseconds: " + fiveDaysInMilliseconds);
    }

    public void exp_addition(List<String> output) {
        //summary: Adding To Duration
        Duration fiveDays = Duration.ofDays(5);
        output.add("5 days: " + fiveDays);
        Duration fiveDaysAnd3Hours = fiveDays.plusHours(3);
        output.add("5 days + 3 hours: " + fiveDaysAnd3Hours);
        Duration sevenDays = fiveDays.plusDays(2);
        output.add("5 days + 2 days: " + sevenDays);
        Duration fiveDaysAnd20Mins = fiveDays.plusMinutes(20);
        output.add("5 days + 20 minutes: " + fiveDaysAnd20Mins);
    }

    public void exp_subtraction(List<String> output) {
        //summary: Subtracting From Duration
        Duration fiveDays = Duration.ofDays(5);
        output.add("5 days: " + fiveDays);
        Duration fiveDaysMinus3Hours = fiveDays.minusHours(3);
        output.add("5 days - 3 hours: " + fiveDaysMinus3Hours);
        Duration fiveDaysMinus2Days = fiveDays.minusDays(2);
        output.add("5 days - 2 days: " + fiveDaysMinus2Days);
        Duration fiveDayMinus20Mins = fiveDays.minusMinutes(20);
        output.add("5 days - 20 minutes: " + fiveDayMinus20Mins);
    }

}
