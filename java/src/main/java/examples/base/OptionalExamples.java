package examples.base;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class OptionalExamples {
    public void exp_init_1(List<String> output) {
        //summary: Optional Initialize With Non-Null Object
        Optional<String> optionalStr = Optional.of("This a test");
        if(optionalStr.isPresent()) {
            output.add("Optional object has value of '" + optionalStr.get() + "'.");
        } else {
            output.add("Optional object has null value.");
        }
    }

    public void exp_init_2(List<String> output) {
        //summary: Optional Initialize With Null Object
        Optional<String> optionalStr = Optional.ofNullable(null);
        if(optionalStr.isEmpty()) {
            output.add("Optional object has null value.");
        } else {
            output.add("Optional object has value of '" + optionalStr.get() + "'.");
        }
    }

    public void exp_runActionIfPresent(List<String> output) {
        //summary: IfPresent Run Action
        Consumer<String> printValue = val -> {
           output.add("Value: " + val);
        };
        Optional<String> optionalStr = Optional.of("This is a test");
        optionalStr.ifPresent(printValue);
    }

    public void exp_orElse(List<String> output) {
        //summary: orElse Method
        // Testing Optional With Non-Null Value
        Optional<String> optionalStr = Optional.of("This a test");
        String value = optionalStr.orElse("Default string value");
        output.add("Value: " + value);
        // Testing Optional With Null Value
        optionalStr = Optional.ofNullable(null);
        value = optionalStr.orElse("Default string value");
        output.add("Value: " + value);
    }

    public void exp_orElseThrow(List<String> output) {
        try {
            //summary: orElseThrow Method
            Optional<String> optionalStr = Optional.of("This is a test");
            String value = optionalStr.orElseThrow();
            output.add("Value: " + value);
            optionalStr = Optional.ofNullable(null);
            value = optionalStr.orElseThrow();
        } catch(NoSuchElementException exception) {
            output.add(exception.getMessage());
        }
    }

    public void exp_orElseGet(List<String> output) {
        //summary: orElseGet Method
        Supplier<String> valueSupplier = () -> {
            return "This is a test";
        };
        Optional<String> optionalStr = Optional.ofNullable(null);
        String value = optionalStr.orElseGet(valueSupplier);
        output.add("Value: " + value);
    }
}