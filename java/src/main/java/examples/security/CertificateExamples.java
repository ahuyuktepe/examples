package examples.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;

public class CertificateExamples {

    public void exp_loadCertificateFromFile(List<String> output) throws Exception {
        //summary: Load Certificate From File
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        File certFile = new File("resources/net/test-entity.crt");
        InputStream certInputStream = new FileInputStream(certFile);
        X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(certInputStream);
        Principal principal = certificate.getSubjectDN();
        output.add("Issuer: " + principal.getName());
        output.add("Valid From: " + certificate.getNotBefore());
        output.add("Valid To: " + certificate.getNotAfter());
        output.add("Serial Number: " +  certificate.getSerialNumber());
        output.add("Version:"  + certificate.getVersion());
        output.add("Signature Algorithm: " + certificate.getSigAlgName());
        output.add("Type: " + certificate.getType());
    }
}
