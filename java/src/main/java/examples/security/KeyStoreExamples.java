package examples.security;

import java.io.*;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

public class KeyStoreExamples {
    public void exp_readKeyStore(List<String> output) throws Exception {
        //summary: Read KeyStore Content
        // Load KeyStore File
        KeyStore keyStore = KeyStore.getInstance("JKS");
        File keyStorefile = new File("resources/net/keystore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        char [] password = "alp123".toCharArray();
        keyStore.load(inputStream, password);
        // Read KeyStore File Content
        output.add("Entry Count:" + keyStore.size());
        Enumeration<String> aliases = keyStore.aliases();
        while(aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            output.add("Alias: " + alias);
        }
    }

    public void exp_printCertificateDetailsInKeyStore(List<String> output) throws Exception {
        //summary: Print Certificate Details In KeyStore
        // Load KeyStore File
        KeyStore keyStore = KeyStore.getInstance("JKS");
        File keyStorefile = new File("resources/net/keystore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        char [] password = "alp123".toCharArray();
        keyStore.load(inputStream, password);
        // Read KeyStore File Content
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("test-entity1");
        if(certificate != null) {
            Principal principal = certificate.getSubjectDN();
            output.add("Issuer: " + principal.getName());
            output.add("Valid From: " + certificate.getNotBefore());
            output.add("Valid To: " + certificate.getNotAfter());
            output.add("Serial Number: " +  certificate.getSerialNumber());
            output.add("Version:"  + certificate.getVersion());
            output.add("Signature Algorithm: " + certificate.getSigAlgName());
        }
    }

    public void exp_saveCertificateFromWebsite(List<String> output) throws Exception {
        //summary: Extract Certificate From KeyStore
        // Load KeyStore File
        KeyStore keyStore = KeyStore.getInstance("JKS");
        File keyStorefile = new File("resources/net/keystore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        char [] password = "alp123".toCharArray();
        keyStore.load(inputStream, password);
        // Fetch Certificate
        Certificate certificate = keyStore.getCertificate("test-entity1");
        byte [] bytes = certificate.getEncoded();
        File certFile = new File("resources/net/test-entity.crt");
        OutputStream outputStream = new FileOutputStream(certFile);
        outputStream.write(bytes);
    }

    public void exp_removeCertificateFromKeyStore(List<String> output) throws Exception {
        //summary: Remove Certificate From KeyStore
        // Load KeyStore File
        KeyStore keyStore = KeyStore.getInstance("JKS");
        File keyStorefile = new File("resources/net/keystore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        char [] password = "alp123".toCharArray();
        keyStore.load(inputStream, password);
        // Remove Certificate
        output.add("Entry Count: " + keyStore.size());
        keyStore.deleteEntry("test-entity1");
        output.add("Removed entry 'test-entity1'.");
        output.add("Entry Count: " + keyStore.size());
    }

    public void exp_addCertificateToKeyStore(List<String> output) throws Exception {
        //summary: Add Certificate To KeyStore
        // Load KeyStore File
        KeyStore keyStore = KeyStore.getInstance("JKS");
        File keyStorefile = new File("resources/net/keystore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        char [] password = "alp123".toCharArray();
        keyStore.load(inputStream, password);
        // Load Certificate
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        File certFile = new File("resources/net/test-entity.crt");
        InputStream certInputStream = new FileInputStream(certFile);
        Certificate certificate = certificateFactory.generateCertificate(certInputStream);
        // Save Certificate In KeyStore
        output.add("Entry Count: " + keyStore.size());
        keyStore.setCertificateEntry("added-cert", certificate);
        output.add("Added new certificate to keystore.");
        output.add("Entry Count: " + keyStore.size());
        Enumeration<String> aliases = keyStore.aliases();
        while(aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            output.add("Alias: " + alias);
        }
    }
}