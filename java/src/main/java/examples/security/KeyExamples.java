package examples.security;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

public class KeyExamples {

    public void exp_generatePublicPrivateKeyPair(List<String> output) throws Exception {
        //summary: Generate Certificate
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();
        output.add("Key Pair Generated");
        output.add("Public Key Generating Algorithm: " + publicKey.getAlgorithm());
        output.add("Private Key Generating Algorithm: " + privateKey.getAlgorithm());
    }

    public void exp_encryptDecryptText(List<String> output) throws Exception {
        //summary: Encrypt Plain Text
        // Generate Key Pair
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();
        // Build Cipher
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        // Encrypt Text
        String plainText = "This is a test";
        byte[] bytes = plainText.getBytes();
        cipher.update(bytes);
        byte[] encryptedBytes = cipher.doFinal();
        String encryptedText = new String(encryptedBytes, "UTF-8");
        output.add("Plain Text: " + plainText);
        output.add("Encrypted Text: " + encryptedText);
        // Decrypt Text
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        cipher.update(encryptedBytes);
        byte [] decryptedBytes = cipher.doFinal();
        String decryptedText = new String(decryptedBytes, "UTF-8");
        output.add("Decrypted Text: " + decryptedText);
    }

}
