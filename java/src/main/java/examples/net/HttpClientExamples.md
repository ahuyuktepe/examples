### Send Http Request Via HttpClient
classes.Example
```
        // Build HttpClient
        HttpClient.Builder builder = HttpClient.newBuilder();
        builder.version(HttpClient.Version.HTTP_2);
        HttpClient client = builder.build();
        // Builder Http Request
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder();
        URI uri = URI.create("http://www.example-host.com/index.html");
        httpRequestBuilder.uri(uri);
        HttpRequest request = httpRequestBuilder.build();
        // Send Request
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        output.add("Status Code: " + response.statusCode());
        output.add("Response: " + response.body());
    }

```
Output
```
Status Code: 200
Response: <html>
  <head>
   <title>Test Page</title>
  </head>
  <body>
    <h2>Test Page</h2>
    <p>This is a test page.</p>
  </body>	  
</html>

```
### Send Https Request Via HttpClient
classes.Example
```
        // Build HttpClient
        HttpClient.Builder builder = HttpClient.newBuilder();
        builder.version(HttpClient.Version.HTTP_2);
        // Build KeyStore
        File keyStorefile = new File("resources/net/truststore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(inputStream, "alp123".toCharArray());
        // Build TrustManagerFactory
        String algorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory factory = TrustManagerFactory.getInstance(algorithm);
        factory.init(keyStore);
        TrustManager [] trustManagers = factory.getTrustManagers();
        // Build SSLContext
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagers, null);
        // Set SSLContext
        builder.sslContext(sslContext);
        // Builder Http Request
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder();
        URI uri = URI.create("https://www.example-host.com/index.html");
        httpRequestBuilder.uri(uri);
        HttpRequest request = httpRequestBuilder.build();
        HttpClient client = builder.build();
        // Send Request
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        output.add("Status Code: " + response.statusCode());
        output.add("Response: " + response.body());
    }
```
Output
```
Status Code: 200
Response: <html>
  <head>
   <title>Test Page</title>
  </head>
  <body>
    <h2>Test Page</h2>
    <p>This is a test page.</p>
  </body>	  
</html>

```
