package examples.net;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyStore;
import java.util.List;

public class HttpClientExamples {

    public void exp_httpClient_1(List<String> output) throws Exception {
        //summary: Send Http Request Via HttpClient
        // Build HttpClient
        HttpClient.Builder builder = HttpClient.newBuilder();
        builder.version(HttpClient.Version.HTTP_2);
        HttpClient client = builder.build();
        // Builder Http Request
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder();
        URI uri = URI.create("http://www.example-host.com/index.html");
        httpRequestBuilder.uri(uri);
        HttpRequest request = httpRequestBuilder.build();
        // Send Request
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        output.add("Status Code: " + response.statusCode());
        output.add("Response: " + response.body());
    }

    public void exp_httpClient_2(List<String> output) throws Exception {
        //summary: Send Https Request Via HttpClient
        // Build HttpClient
        HttpClient.Builder builder = HttpClient.newBuilder();
        builder.version(HttpClient.Version.HTTP_2);
        // Build KeyStore
        File keyStorefile = new File("resources/net/truststore.jks");
        InputStream inputStream = new FileInputStream(keyStorefile);
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(inputStream, "alp123".toCharArray());
        // Build TrustManagerFactory
        String algorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory factory = TrustManagerFactory.getInstance(algorithm);
        factory.init(keyStore);
        TrustManager [] trustManagers = factory.getTrustManagers();
        // Build SSLContext
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagers, null);
        // Set SSLContext
        builder.sslContext(sslContext);
        // Builder Http Request
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder();
        URI uri = URI.create("https://www.example-host.com/index.html");
        httpRequestBuilder.uri(uri);
        HttpRequest request = httpRequestBuilder.build();
        HttpClient client = builder.build();
        // Send Request
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        output.add("Status Code: " + response.statusCode());
        output.add("Response: " + response.body());
    }

    public void exp_httpClient_3(List<String> output) throws Exception {
        //summary: Send Http Request With Authentication
        // Build HttpClient
        HttpClient.Builder builder = HttpClient.newBuilder();
        builder.version(HttpClient.Version.HTTP_2);
        // Set Authentication Information
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                String username = "[USERNAME]";
                char[] password = "[PASSWORD]".toCharArray();
                return new PasswordAuthentication(username, password);
            }
        };
        builder.authenticator(authenticator);
        // Build Http Client
        HttpClient client = builder.build();
        // Builder Http Request
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder();
        URI uri = URI.create("http://www.example-host.com");
        httpRequestBuilder.uri(uri);
        HttpRequest request = httpRequestBuilder.build();
        // Send Request
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        output.add("Status Code: " + response.statusCode());
        output.add("Response: " + response.body());
    }
}
