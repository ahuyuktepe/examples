package examples.net;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

public class ApacheHttpClientExamples {

    public void exp_httpClient_1(List<String> output) throws Exception {
        //summary: Send Http Request Via Apache HttpClient
        // Build Http Client
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        Scanner scanner = new Scanner(inputStream);
        output.add("Response Content");
        String line = null;
        while(scanner.hasNext()) {
            line = scanner.nextLine();
            output.add(line);
        }
    }

    public void exp_httpClient_2(List<String> output) throws Exception {
        //summary: Send Https Request Via Apache HttpClient
        // Build SSL Context Builder
        SSLContextBuilder sslContextBuilder = SSLContexts.custom();
        // Load Key Store File
        File keyStorefile = new File("resources/net/truststore.jks");
        char [] password = "alp123".toCharArray();
        TrustSelfSignedStrategy strategy = new TrustSelfSignedStrategy();
        sslContextBuilder.loadTrustMaterial(keyStorefile, password, strategy);
        // Build SSL Context
        SSLContext sslContext = sslContextBuilder.build();
        // Build Http Client Builder With SSL Context
        HttpClientBuilder httpClientBuilder = HttpClients.custom();
        httpClientBuilder.setSSLContext(sslContext);
        // Http Client
        CloseableHttpClient httpClient = httpClientBuilder.build();
        // Build Http Request
        HttpGet httpGet = new HttpGet("https://www.example-host.com");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        Scanner scanner = new Scanner(inputStream);
        output.add("Response Content");
        String line = null;
        while(scanner.hasNext()) {
            line = scanner.nextLine();
            output.add(line);
        }
    }

    public void exp_httpClient_3(List<String> output) throws Exception {
        //summary: Read Request/Response Headers
        // Build Http Client
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com");
        httpGet.setHeader("Accept", "text/html");
        httpGet.setHeader("Cache-Control", "max-age=0");
        // Read Request Headers
        Header [] headers = httpGet.getAllHeaders();
        output.add("Request Headers");
        for(Header header: headers) {
            output.add(header.getName() + ": " + header.getValue());
        }
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response Headers
        headers = response.getAllHeaders();
        output.add("\nResponse Headers");
        for(Header header: headers) {
            output.add(header.getName() + ": " + header.getValue());
        }
    }

    public void exp_httpClient_4(List<String> output) throws Exception {
        //summary: Read Json Response
        // Build Http Client
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com/sample.json");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        String line = reader.readLine();
        while(line != null) {
            output.add(line);
            line = reader.readLine();
        }
    }

    public void exp_httpClient_5(List<String> output) throws Exception {
        //summary: Send Http Request With Basic Authentication
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("test", "test123");
        credentialsProvider.setCredentials(AuthScope.ANY, credentials);
        // Build Http Client
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        CloseableHttpClient httpClient = httpClientBuilder.build();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        Scanner scanner = new Scanner(inputStream);
        output.add("Status: "  + response.getStatusLine().getStatusCode());
        output.add("Status Reason: "  + response.getStatusLine().getReasonPhrase());
        output.add("Response Content");
        String line = null;
        while(scanner.hasNext()) {
            line = scanner.nextLine();
            output.add(line);
        }
    }
}
