### Send Http Request Via Apache HttpClient
classes.Example
```
        // Build Http Client
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        Scanner scanner = new Scanner(inputStream);
        output.add("Response Content");
        String line = null;
        while(scanner.hasNext()) {
            line = scanner.nextLine();
            output.add(line);
        }
    }

```
Output
```
Response Content
<html>
  <head>
   <title>Test Page</title>
  </head>
  <body>
    <h2>Test Page</h2>
    <p>This is a test page.</p>
  </body>	  
</html>
```
### Send Https Request Via Apache HttpClient
classes.Example
```
        // Build SSL Context Builder
        SSLContextBuilder sslContextBuilder = SSLContexts.custom();
        // Load Key Store File
        File keyStorefile = new File("resources/net/keystore.jks");
        char [] password = "alp123".toCharArray();
        TrustSelfSignedStrategy strategy = new TrustSelfSignedStrategy();
        sslContextBuilder.loadTrustMaterial(keyStorefile, password, strategy);
        // Build SSL Context
        SSLContext sslContext = sslContextBuilder.build();
        // Build Http Client Builder With SSL Context
        HttpClientBuilder httpClientBuilder = HttpClients.custom();
        httpClientBuilder.setSSLContext(sslContext);
        // Http Client
        CloseableHttpClient httpClient = httpClientBuilder.build();
        // Build Http Request
        HttpGet httpGet = new HttpGet("https://www.example-host.com");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        Scanner scanner = new Scanner(inputStream);
        output.add("Response Content");
        String line = null;
        while(scanner.hasNext()) {
            line = scanner.nextLine();
            output.add(line);
        }
    }

```
Output
```
Response Content
<html>
  <head>
   <title>Test Page</title>
  </head>
  <body>
    <h2>Test Page</h2>
    <p>This is a test page.</p>
  </body>	  
</html>
```
### Read Request/Response Headers
classes.Example
```
        // Build Http Client
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com");
        httpGet.setHeader("Accept", "text/html");
        httpGet.setHeader("Cache-Control", "max-age=0");
        // Read Request Headers
        Header [] headers = httpGet.getAllHeaders();
        output.add("Request Headers");
        for(Header header: headers) {
            output.add(header.getName() + ": " + header.getValue());
        }
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response Headers
        headers = response.getAllHeaders();
        output.add("\nResponse Headers");
        for(Header header: headers) {
            output.add(header.getName() + ": " + header.getValue());
        }
    }

```
Output
```
Request Headers
Accept: text/html
Cache-Control: max-age=0

Response Headers
Date: Sat, 17 Apr 2021 23:46:56 GMT
Server: Apache/2.4.46 (Unix) OpenSSL/1.1.1 PHP/7.4.16
Last-Modified: Sat, 17 Apr 2021 23:11:06 GMT
ETag: "8b-5c03336102156"
Accept-Ranges: bytes
Content-Length: 139
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: text/html
```
### Read Json Response
classes.Example
```
        // Build Http Client
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // Build Http Request
        HttpGet httpGet = new HttpGet("http://www.example-host.com/sample.json");
        // Send Http Request
        HttpResponse response = httpClient.execute(httpGet);
        // Read Response
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        String line = reader.readLine();
        while(line != null) {
            output.add(line);
            line = reader.readLine();
        }
    }
```
Output
```
{
  "status": "success",
  "data": [
    {"user_name":"test", "first_name": "Test", "last_name":"User"}  
  ]
}
```
