### Single Thread Execution
Example
```
ExecutorService executorService = null;
try {
    Runnable task1 = () -> {
        output.add("Executed Task 1");
    };
    executorService = Executors.newSingleThreadExecutor();
    executorService.execute(task1);
    // Wait until all tasks are done
    executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Executed Task 1
```
### Multi Thread Execution
Example
```
ExecutorService executorService = null;
try {
    Runnable task1 = () -> {
        output.add("Executed Task 1");
    };
    Runnable task2 = () -> {
        output.add("Executed Task 2");
    };
    executorService = Executors.newSingleThreadExecutor();
    executorService.execute(task1);
    executorService.execute(task2);
    // Wait until all tasks are done
    executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Executed Task 1
Executed Task 2
```
### Shutdown Executor Service Example
Example
```
ExecutorService executorService = null;
try {
    Runnable task1 = () -> {
        try {
            output.add("Task 1 is sleeping for 3 ms.");
            Thread.sleep(3000);
            output.add("Executed Task 1");
        } catch(InterruptedException exception) {
            output.add("Task 1 is interrupted.");
        }
    };
    executorService = Executors.newSingleThreadExecutor();
    executorService.execute(task1);
    output.add("Sleeping 1 ms.");
    Thread.sleep(1000);
    output.add("Is ExecutorService shutdown: " + executorService.isShutdown());
    output.add("Shutting down executor service.");
    executorService.shutdownNow();
    output.add("Is ExecutorService shutdown: " + executorService.isShutdown());
    // Wait until all tasks are done
    executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Sleeping 1 ms.
Task 1 is sleeping for 3 ms.
Is ExecutorService shutdown: false
Shutting down executor service.
Task 1 is interrupted.
Is ExecutorService shutdown: true
```
### Submitting Task Example
Example
```
ExecutorService executorService = null;
try {
    Runnable task1 = () -> {
        try {
            output.add("Task 1 is sleeping for 3 ms.");
            Thread.sleep(3000);
            output.add("Executed Task 1");
        } catch(InterruptedException exception) {
            output.add("Task 1 is interrupted.");
        }
    };
    executorService = Executors.newSingleThreadExecutor();
    output.add("Submitting task to be executed.");
    Future result = executorService.submit(task1);
    output.add("Fetching result.");
    Object obj = result.get();
    output.add("Fetched result.");
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Submitting task to be executed.
Fetching result.
Task 1 is sleeping for 3 ms.
Executed Task 1
Fetched result.
```
### Submitting Callable Task With Input Example
Dependencies
```
package resources;

import java.util.concurrent.Callable;

public class SampleCallableTask implements Callable<String> {
    private String text;

    public SampleCallableTask(String text) {
        this.text = text;
    }

    @Override
    public String call() throws Exception {
        return this.text;
    }
}
```
Example
```
ExecutorService executorService = null;
try {
    Callable<String> task1 = new SampleCallableTask("TestTask1");
    executorService = Executors.newSingleThreadExecutor();
    output.add("Submitting task to be executed.");
    Future<String> result = executorService.submit(task1);
    output.add("Fetching result.");
    Object obj = result.get();
    output.add("Fetched result.");
    if(obj != null) {
        output.add("Result: " + obj);
    }
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Submitting task to be executed.
Fetching result.
Fetched result.
Result: TestTask1
```
### Submitting Callable Task With Lambda Expression
Dependencies
```
package resources;

import java.util.concurrent.Callable;

public class SampleCallableTask implements Callable<String> {
    private String text;

    public SampleCallableTask(String text) {
        this.text = text;
    }

    @Override
    public String call() throws Exception {
        return this.text;
    }
}
```
Example
```
ExecutorService executorService = null;
try {
    executorService = Executors.newSingleThreadExecutor();
    output.add("Submitting task to be executed.");
    Future<String> result = executorService.submit(() -> {
        output.add("Executing task.");
        return "Task Generated Via Lambda Expression";
    });
    output.add("Fetching result.");
    Object obj = result.get();
    output.add("Fetched result.");
    if(obj != null) {
        output.add("Result: " + obj);
    }
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Submitting task to be executed.
Fetching result.
Executing task.
Fetched result.
Result: Task Generated Via Lambda Expression
```
