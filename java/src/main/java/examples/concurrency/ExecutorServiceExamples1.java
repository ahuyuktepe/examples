package examples.concurrency;

import resources.SampleCallableTask;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorServiceExamples1 {

    public void exp_example1(List<String> output) throws InterruptedException {
        //summary: Single Thread Execution
        ExecutorService executorService = null;
        try {
            Runnable task1 = () -> {
                output.add("Executed Task 1");
            };
            executorService = Executors.newSingleThreadExecutor();
            executorService.execute(task1);
            // Wait until all tasks are done
            executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example2(List<String> output) throws InterruptedException {
        //summary: Multi Thread Execution
        ExecutorService executorService = null;
        try {
            Runnable task1 = () -> {
                output.add("Executed Task 1");
            };
            Runnable task2 = () -> {
                output.add("Executed Task 2");
            };
            executorService = Executors.newSingleThreadExecutor();
            executorService.execute(task1);
            executorService.execute(task2);
            // Wait until all tasks are done
            executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example3(List<String> output) throws InterruptedException {
        //summary: Shutdown Executor Service Example
        ExecutorService executorService = null;
        try {
            Runnable task1 = () -> {
                try {
                    output.add("Task 1 is sleeping for 3 ms.");
                    Thread.sleep(3000);
                    output.add("Executed Task 1");
                } catch(InterruptedException exception) {
                    output.add("Task 1 is interrupted.");
                }
            };
            executorService = Executors.newSingleThreadExecutor();
            executorService.execute(task1);
            output.add("Sleeping 1 ms.");
            Thread.sleep(1000);
            output.add("Is ExecutorService shutdown: " + executorService.isShutdown());
            output.add("Shutting down executor service.");
            executorService.shutdownNow();
            output.add("Is ExecutorService shutdown: " + executorService.isShutdown());
            // Wait until all tasks are done
            executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example4(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Submitting Task Example
        ExecutorService executorService = null;
        try {
            Runnable task1 = () -> {
                try {
                    output.add("Task 1 is sleeping for 3 ms.");
                    Thread.sleep(3000);
                    output.add("Executed Task 1");
                } catch(InterruptedException exception) {
                    output.add("Task 1 is interrupted.");
                }
            };
            executorService = Executors.newSingleThreadExecutor();
            output.add("Submitting task to be executed.");
            Future result = executorService.submit(task1);
            output.add("Fetching result.");
            Object obj = result.get();
            output.add("Fetched result.");
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example5(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Submitting Callable Task With Input Example
        //resources: SampleCallableTask
        ExecutorService executorService = null;
        try {
            Callable<String> task1 = new SampleCallableTask("TestTask1");
            executorService = Executors.newSingleThreadExecutor();
            output.add("Submitting task to be executed.");
            Future<String> result = executorService.submit(task1);
            output.add("Fetching result.");
            Object obj = result.get();
            output.add("Fetched result.");
            if(obj != null) {
                output.add("Result: " + obj);
            }
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example6(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Submitting Callable Task With Lambda Expression
        //resources: SampleCallableTask
        ExecutorService executorService = null;
        try {
            executorService = Executors.newSingleThreadExecutor();
            output.add("Submitting task to be executed.");
            Future<String> result = executorService.submit(() -> {
                output.add("Executing task.");
                return "Task Generated Via Lambda Expression";
            });
            output.add("Fetching result.");
            Object obj = result.get();
            output.add("Fetched result.");
            if(obj != null) {
                output.add("Result: " + obj);
            }
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end
}