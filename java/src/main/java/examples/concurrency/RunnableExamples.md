### Runnable Example 1
Dependencies
```
package resources;

import java.util.List;

public class SampleTask implements Runnable  {
    private int delayInMs;
    private List<String> output;
    private String name;

    public SampleTask(List<String> output, String name, int delay) {
        delayInMs = delay;
        this.output = output;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            if(this.delayInMs != 0) {
                output.add("Task '" + this.name + "' is sleeping for " + delayInMs + " ms.");
                Thread.sleep(this.delayInMs);
            }
            output.add("Task '" + this.name + "' is completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
Example
```
Runnable sampleTask = new SampleTask(output, "Sample-Task-1", 1000);
Thread thread = new Thread(sampleTask);
thread.start();
thread.join();
```
Output
```
Task 'Sample-Task-1' is sleeping for 1000 ms.
Task 'Sample-Task-1' is completed.
```
### Runnable Example 2
Example
```
Runnable task = () -> {
    output.add("Task 'Sample-Task-1' is completed.");
};
Thread thread = new Thread(task);
thread.start();
thread.join();
```
Output
```
Task 'Sample-Task-1' is completed.
```
