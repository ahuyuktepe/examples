package examples.concurrency;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorServiceExamples3 {

    public void exp_example1(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Executing Tasks Via Fixed Size Thread Pool Executor
        ExecutorService executorService = null;
        try {
            Runnable task = () -> {
                output.add("Executing Task 1.");
            };
            executorService = Executors.newFixedThreadPool(3);
            executorService.execute(task);
            executorService.execute(task);
            executorService.execute(task);
            executorService.execute(task);
            // Wait until all tasks are done
            executorService.awaitTermination(2000, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example2(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Executing Tasks Via Unlimited Size Thread Pool Executor
        ExecutorService executorService = null;
        try {
            Runnable task = () -> {
                output.add("Executing Task 1.");
            };
            executorService = Executors.newFixedThreadPool(3);
            executorService.execute(task);
            executorService.execute(task);
            executorService.execute(task);
            executorService.execute(task);
            // Wait until all tasks are done
            executorService.awaitTermination(2000, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end
}