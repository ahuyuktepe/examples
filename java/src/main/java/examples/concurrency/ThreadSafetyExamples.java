package examples.concurrency;

import resources.NumberCounter;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadSafetyExamples {

    public void exp_example1(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Atomic Variable Example
        int max = 10;
        AtomicInteger counter = new AtomicInteger(0);
        Runnable increaseCounterTask = () -> {
            String currentThreadName = Thread.currentThread().getName();
            for(int i=1; i<= max; i++) {
                counter.getAndAdd(1);
                output.add("Thread '" + currentThreadName +"' increased counter by 1 to " + counter.get());
            }
        };
        Thread thread1 = new Thread(increaseCounterTask, "Thread 1");
        Thread thread2 = new Thread(increaseCounterTask, "Thread 2");
        Thread thread3 = new Thread(increaseCounterTask, "Thread 3");
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
        output.add("Current Counter: " + counter);
    }//end

    public void exp_example2(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Synchronized Block Example
        //resources: NumberCounter
        NumberCounter numberCounter = new NumberCounter();
        Runnable task = () -> {
            String currentThreadName = Thread.currentThread().getName();
            synchronized (numberCounter) {
                for(int i=1; i <= 5; i++) {
                    numberCounter.counter++;
                    output.add("Thread '" + currentThreadName +"' increased counter by 1 to " + numberCounter.counter);
                }
            }
        };
        Thread thread1 = new Thread(task, "Thread 1");
        Thread thread2 = new Thread(task, "Thread 2");
        Thread thread3 = new Thread(task, "Thread 3");
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
    }//end

    public void exp_example3(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Synchronized Method Example
        //resources: NumberCounter
        NumberCounter numberCounter = new NumberCounter();
        Runnable task = () -> {
            String currentThreadName = Thread.currentThread().getName();
            numberCounter.increase(currentThreadName, output);
        };
        Thread thread1 = new Thread(task, "Thread 1");
        Thread thread2 = new Thread(task, "Thread 2");
        Thread thread3 = new Thread(task, "Thread 3");
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
        output.add("Counter: " + NumberCounter.counter);
    }//end

    public void exp_example4(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Lock Interface Example
        NumberCounter numberCounter = new NumberCounter();
        ReentrantLock lock = new ReentrantLock();
        Runnable task = () -> {
            try {
                String currentThreadName = Thread.currentThread().getName();
                lock.lock();
                for(int i=1; i <= 5; i++) {
                    numberCounter.counter++;
                    output.add("Thread '" + currentThreadName +"' increased counter by 1 to " + numberCounter.counter);
                }
            } finally {
                lock.unlock();
            }
        };
        Thread thread1 = new Thread(task, "Thread 1");
        Thread thread2 = new Thread(task, "Thread 2");
        Thread thread3 = new Thread(task, "Thread 3");
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
        output.add("Counter: " + NumberCounter.counter);
    }//end

    public void exp_example5(List<String> output) throws InterruptedException, ExecutionException {
        //summary: ConcurrentLinkedQueue Example
        ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<String>();
        Runnable task = () -> {
            String currentThreadName = Thread.currentThread().getName();
            for(int i=1; i <= 5; i++) {
                queue.add(currentThreadName + " added " + i);
            }
        };
        Thread thread1 = new Thread(task, "Thread 1");
        Thread thread2 = new Thread(task, "Thread 2");
        Thread thread3 = new Thread(task, "Thread 3");
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
        String str = null;
        while((str = queue.poll()) != null) {
            output.add(str);
        }
    }//end
}
