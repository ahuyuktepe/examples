package examples.concurrency;

import resources.SampleThread;
import resources.SampleThreadFactory;

import java.util.List;

public class ThreadExamples {
    public void exp_example1(List<String> output) throws InterruptedException {
        //summary: Thread Example
        //resources: SampleThread
        Thread thread = new SampleThread(output, 1000);
        thread.setName("Example-Thread-1");
        thread.start();
        thread.join();
    }

    public void exp_example2(List<String> output) throws InterruptedException {
        //summary: getState Method Example
        //resources: SampleThread
        Thread thread = new SampleThread(output,1500);
        // Print Initial Status
        Thread.State state = thread.getState();
        output.add("Status: " + state.toString());
        // Print Status After Start
        thread.start();
        Thread.sleep(1000);
        state = thread.getState();
        output.add("Status: " + state.toString());
        // Print Status After Execution
        Thread.sleep(1000);
        state = thread.getState();
        output.add("Status: " + state.toString());
    }//end

    public void exp_example3(List<String> output) throws InterruptedException {
        //summary: interrupt Method Example
        //resources: SampleThread
        // Set low priority thread
        Thread lowPriorityThread = new SampleThread(output,1000);
        lowPriorityThread.setPriority(2);
        lowPriorityThread.setName("Low-Priority-Thread");
        // Set high priority thread
        Thread highPriorityThread = new SampleThread(output,1000);
        highPriorityThread.setPriority(5);
        highPriorityThread.setName("High-Priority-Thread");
        // Run threads
        lowPriorityThread.start();
        highPriorityThread.start();
        // Wait last thread to print output
        lowPriorityThread.join();
    }//end

    public void exp_example4(List<String> output) throws InterruptedException {
        //summary: join Method Example
        //resources: SampleThread
        Thread firstThread = new SampleThread(output,1000);
        firstThread.setName("First Thread");
        Thread secondThread = new Thread(() -> {
            try {
                output.add("Waiting For " + firstThread.getName() + " thread.");
                firstThread.join();
                output.add("Executing Second Thread");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "Second Thread");
        secondThread.start();
        firstThread.start();
        // Wait last thread to print output
        secondThread.join();
    }//end

    public void exp_example5(List<String> output) throws InterruptedException {
        //summary: Thread Generation Via ThreadFactory
        Runnable task = () -> {
            output.add("Executing Task 1.");
        };
        SampleThreadFactory threadFactory = new SampleThreadFactory();
        Thread thread = threadFactory.newThread(task);
        thread.run();
    }//end
}
