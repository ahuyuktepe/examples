### getState Method Example
Dependencies
```
package resources;

import java.util.List;

public class SampleThread extends Thread {
    private int delayInMs;
    private List<String> output;

    public SampleThread(List<String> output, int delay) {
        delayInMs = delay;
        this.output = output;
    }

    public SampleThread(Runnable runnable) {
        super(runnable);
    }

    @Override
    public void run() {
        try {
            if(this.delayInMs != 0) {
                output.add("Thread '" + this.getName() + "' is sleeping for " + delayInMs + " ms.");
                Thread.sleep(this.delayInMs);
            }
            output.add("Thread '" + this.getName() + "' is completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
Example
```
Thread thread = new SampleThread(output,1500);
// Print Initial Status
Thread.State state = thread.getState();
output.add("Status: " + state.toString());
// Print Status After Start
thread.start();
Thread.sleep(1000);
state = thread.getState();
output.add("Status: " + state.toString());
// Print Status After Execution
Thread.sleep(1000);
state = thread.getState();
output.add("Status: " + state.toString());
```
Output
```
Status: NEW
Thread 'Thread-0' is sleeping for 1500 ms.
Status: TIMED_WAITING
Thread 'Thread-0' is completed.
Status: TERMINATED
```
### interrupt Method Example
Dependencies
```
package resources;

import java.util.List;

public class SampleThread extends Thread {
    private int delayInMs;
    private List<String> output;

    public SampleThread(List<String> output, int delay) {
        delayInMs = delay;
        this.output = output;
    }

    public SampleThread(Runnable runnable) {
        super(runnable);
    }

    @Override
    public void run() {
        try {
            if(this.delayInMs != 0) {
                output.add("Thread '" + this.getName() + "' is sleeping for " + delayInMs + " ms.");
                Thread.sleep(this.delayInMs);
            }
            output.add("Thread '" + this.getName() + "' is completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
Example
```
// Set low priority thread
Thread lowPriorityThread = new SampleThread(output,1000);
lowPriorityThread.setPriority(2);
lowPriorityThread.setName("Low-Priority-Thread");
// Set high priority thread
Thread highPriorityThread = new SampleThread(output,1000);
highPriorityThread.setPriority(5);
highPriorityThread.setName("High-Priority-Thread");
// Run threads
lowPriorityThread.start();
highPriorityThread.start();
// Wait last thread to print output
lowPriorityThread.join();
```
Output
```
Thread 'High-Priority-Thread' is sleeping for 1000 ms.
Thread 'Low-Priority-Thread' is sleeping for 1000 ms.
Thread 'High-Priority-Thread' is completed.
Thread 'Low-Priority-Thread' is completed.
```
### join Method Example
Dependencies
```
package resources;

import java.util.List;

public class SampleThread extends Thread {
    private int delayInMs;
    private List<String> output;

    public SampleThread(List<String> output, int delay) {
        delayInMs = delay;
        this.output = output;
    }

    public SampleThread(Runnable runnable) {
        super(runnable);
    }

    @Override
    public void run() {
        try {
            if(this.delayInMs != 0) {
                output.add("Thread '" + this.getName() + "' is sleeping for " + delayInMs + " ms.");
                Thread.sleep(this.delayInMs);
            }
            output.add("Thread '" + this.getName() + "' is completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
Example
```
Thread firstThread = new SampleThread(output,1000);
firstThread.setName("First Thread");
Thread secondThread = new Thread(() -> {
    try {
        output.add("Waiting For " + firstThread.getName() + " thread.");
        firstThread.join();
        output.add("Executing Second Thread");
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
}, "Second Thread");
secondThread.start();
firstThread.start();
// Wait last thread to print output
secondThread.join();
```
Output
```
Thread 'First Thread' is sleeping for 1000 ms.
Waiting For First Thread thread.
Thread 'First Thread' is completed.
Executing Second Thread
```
### Thread Generation Via ThreadFactory
Example
```
Runnable task = () -> {
    output.add("Executing Task 1.");
};
SampleThreadFactory threadFactory = new SampleThreadFactory();
Thread thread = threadFactory.newThread(task);
thread.run();
```
Output
```
Executing Task 1.
```
