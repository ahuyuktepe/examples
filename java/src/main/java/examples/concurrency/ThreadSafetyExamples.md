### Atomic Variable Example
Example
```
int max = 10;
AtomicInteger counter = new AtomicInteger(0);
Runnable increaseCounterTask = () -> {
    String currentThreadName = Thread.currentThread().getName();
    for(int i=1; i<= max; i++) {
        counter.getAndAdd(1);
        output.add("Thread '" + currentThreadName +"' increased counter by 1 to " + counter.get());
    }
};
Thread thread1 = new Thread(increaseCounterTask, "Thread 1");
Thread thread2 = new Thread(increaseCounterTask, "Thread 2");
Thread thread3 = new Thread(increaseCounterTask, "Thread 3");
thread1.start();
thread2.start();
thread3.start();
thread1.join();
thread2.join();
thread3.join();
output.add("Current Counter: " + counter);
```
Output
```
Thread 'Thread 2' increased counter by 1 to 2
Thread 'Thread 2' increased counter by 1 to 4
Thread 'Thread 1' increased counter by 1 to 5
Thread 'Thread 2' increased counter by 1 to 6
Thread 'Thread 1' increased counter by 1 to 7
Thread 'Thread 2' increased counter by 1 to 8
Thread 'Thread 1' increased counter by 1 to 9
Thread 'Thread 2' increased counter by 1 to 10
Thread 'Thread 1' increased counter by 1 to 11
Thread 'Thread 2' increased counter by 1 to 12
Thread 'Thread 1' increased counter by 1 to 13
Thread 'Thread 2' increased counter by 1 to 14
Thread 'Thread 1' increased counter by 1 to 15
Thread 'Thread 1' increased counter by 1 to 17
Thread 'Thread 2' increased counter by 1 to 16
Thread 'Thread 2' increased counter by 1 to 19
Thread 'Thread 1' increased counter by 1 to 20
Thread 'Thread 2' increased counter by 1 to 21
Thread 'Thread 3' increased counter by 1 to 3
Thread 'Thread 3' increased counter by 1 to 22
Thread 'Thread 3' increased counter by 1 to 23
Thread 'Thread 3' increased counter by 1 to 24
Thread 'Thread 3' increased counter by 1 to 25
Thread 'Thread 3' increased counter by 1 to 26
Thread 'Thread 3' increased counter by 1 to 27
Thread 'Thread 3' increased counter by 1 to 28
Thread 'Thread 3' increased counter by 1 to 29
Thread 'Thread 3' increased counter by 1 to 30
Current Counter: 30
```
### Synchronized Block Example
Dependencies
```
package resources;

import java.util.List;

public class NumberCounter {
    public static int counter = 0;

    public synchronized void increase(String threadName, List<String> output) {
        for(int i=1; i <= 5; i++) {
            counter++;
            output.add("Thread '" + threadName +"' increased counter by 1 to " + counter);
        }
    }
}
```
Example
```
NumberCounter numberCounter = new NumberCounter();
Runnable task = () -> {
    String currentThreadName = Thread.currentThread().getName();
    synchronized (numberCounter) {
        for(int i=1; i <= 5; i++) {
            numberCounter.counter++;
            output.add("Thread '" + currentThreadName +"' increased counter by 1 to " + numberCounter.counter);
        }
    }
};
Thread thread1 = new Thread(task, "Thread 1");
Thread thread2 = new Thread(task, "Thread 2");
Thread thread3 = new Thread(task, "Thread 3");
thread1.start();
thread2.start();
thread3.start();
thread1.join();
thread2.join();
thread3.join();
```
Output
```
Thread 'Thread 1' increased counter by 1 to 1
Thread 'Thread 1' increased counter by 1 to 2
Thread 'Thread 1' increased counter by 1 to 3
Thread 'Thread 1' increased counter by 1 to 4
Thread 'Thread 1' increased counter by 1 to 5
Thread 'Thread 3' increased counter by 1 to 6
Thread 'Thread 3' increased counter by 1 to 7
Thread 'Thread 3' increased counter by 1 to 8
Thread 'Thread 3' increased counter by 1 to 9
Thread 'Thread 3' increased counter by 1 to 10
Thread 'Thread 2' increased counter by 1 to 11
Thread 'Thread 2' increased counter by 1 to 12
Thread 'Thread 2' increased counter by 1 to 13
Thread 'Thread 2' increased counter by 1 to 14
Thread 'Thread 2' increased counter by 1 to 15
```
### Synchronized Method Example
Dependencies
```
package resources;

import java.util.List;

public class NumberCounter {
    public static int counter = 0;

    public synchronized void increase(String threadName, List<String> output) {
        for(int i=1; i <= 5; i++) {
            counter++;
            output.add("Thread '" + threadName +"' increased counter by 1 to " + counter);
        }
    }
}
```
Example
```
NumberCounter numberCounter = new NumberCounter();
Runnable task = () -> {
    String currentThreadName = Thread.currentThread().getName();
    numberCounter.increase(currentThreadName, output);
};
Thread thread1 = new Thread(task, "Thread 1");
Thread thread2 = new Thread(task, "Thread 2");
Thread thread3 = new Thread(task, "Thread 3");
thread1.start();
thread2.start();
thread3.start();
thread1.join();
thread2.join();
thread3.join();
output.add("Counter: " + NumberCounter.counter);
```
Output
```
Thread 'Thread 1' increased counter by 1 to 16
Thread 'Thread 1' increased counter by 1 to 17
Thread 'Thread 1' increased counter by 1 to 18
Thread 'Thread 1' increased counter by 1 to 19
Thread 'Thread 1' increased counter by 1 to 20
Thread 'Thread 3' increased counter by 1 to 21
Thread 'Thread 3' increased counter by 1 to 22
Thread 'Thread 3' increased counter by 1 to 23
Thread 'Thread 3' increased counter by 1 to 24
Thread 'Thread 3' increased counter by 1 to 25
Thread 'Thread 2' increased counter by 1 to 26
Thread 'Thread 2' increased counter by 1 to 27
Thread 'Thread 2' increased counter by 1 to 28
Thread 'Thread 2' increased counter by 1 to 29
Thread 'Thread 2' increased counter by 1 to 30
Counter: 30
```
### Lock Interface Example
Example
```
NumberCounter numberCounter = new NumberCounter();
ReentrantLock lock = new ReentrantLock();
Runnable task = () -> {
    try {
        String currentThreadName = Thread.currentThread().getName();
        lock.lock();
        for(int i=1; i <= 5; i++) {
            numberCounter.counter++;
            output.add("Thread '" + currentThreadName +"' increased counter by 1 to " + numberCounter.counter);
        }
    } finally {
        lock.unlock();
    }
};
Thread thread1 = new Thread(task, "Thread 1");
Thread thread2 = new Thread(task, "Thread 2");
Thread thread3 = new Thread(task, "Thread 3");
thread1.start();
thread2.start();
thread3.start();
thread1.join();
thread2.join();
thread3.join();
output.add("Counter: " + NumberCounter.counter);
```
Output
```
Thread 'Thread 3' increased counter by 1 to 31
Thread 'Thread 3' increased counter by 1 to 32
Thread 'Thread 3' increased counter by 1 to 33
Thread 'Thread 3' increased counter by 1 to 34
Thread 'Thread 3' increased counter by 1 to 35
Thread 'Thread 2' increased counter by 1 to 36
Thread 'Thread 2' increased counter by 1 to 37
Thread 'Thread 2' increased counter by 1 to 38
Thread 'Thread 2' increased counter by 1 to 39
Thread 'Thread 2' increased counter by 1 to 40
Thread 'Thread 1' increased counter by 1 to 41
Thread 'Thread 1' increased counter by 1 to 42
Thread 'Thread 1' increased counter by 1 to 43
Thread 'Thread 1' increased counter by 1 to 44
Thread 'Thread 1' increased counter by 1 to 45
Counter: 45
```
### ConcurrentLinkedQueue Example
Example
```
ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<String>();
Runnable task = () -> {
    String currentThreadName = Thread.currentThread().getName();
    for(int i=1; i <= 5; i++) {
        queue.add(currentThreadName + " added " + i);
    }
};
Thread thread1 = new Thread(task, "Thread 1");
Thread thread2 = new Thread(task, "Thread 2");
Thread thread3 = new Thread(task, "Thread 3");
thread1.start();
thread2.start();
thread3.start();
thread1.join();
thread2.join();
thread3.join();
String str = null;
while((str = queue.poll()) != null) {
    output.add(str);
}
```
Output
```
Thread 3 added 1
Thread 3 added 2
Thread 3 added 3
Thread 3 added 4
Thread 3 added 5
Thread 1 added 1
Thread 1 added 2
Thread 1 added 3
Thread 1 added 4
Thread 1 added 5
Thread 2 added 1
Thread 2 added 2
Thread 2 added 3
Thread 2 added 4
Thread 2 added 5
```
