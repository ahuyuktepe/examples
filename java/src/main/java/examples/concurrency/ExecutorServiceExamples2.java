package examples.concurrency;

import java.sql.Time;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorServiceExamples2 {

    public void exp_example1(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Scheduling Single Task Example
        ScheduledExecutorService executorService = null;
        try {
            Runnable task = () -> {
                Instant now = Instant.now();
                output.add("Executing Task 1 at '" + now.toEpochMilli() +"'.");
            };
            executorService = Executors.newSingleThreadScheduledExecutor();
            Instant now = Instant.now();
            output.add("Scheduled Task 1 at '" + now.toEpochMilli() +"'.");
            executorService.schedule(task, 1500, TimeUnit.MILLISECONDS);
            // Wait until all tasks are done
            executorService.awaitTermination(2000, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

    public void exp_example2(List<String> output) throws InterruptedException, ExecutionException {
        //summary: Scheduling Single Task Multiple Times Example
        ScheduledExecutorService executorService = null;
        try {
            Runnable task = () -> {
                Instant now = Instant.now();
                output.add("Executing Task 1 at '" + now.toEpochMilli() +"'.");
            };
            executorService = Executors.newSingleThreadScheduledExecutor();
            Instant now = Instant.now();
            output.add("Scheduled Task 1 at '" + now.toEpochMilli() +"'.");
            executorService.scheduleWithFixedDelay(task, 100, 100, TimeUnit.MILLISECONDS);
            // Wait until all tasks are done
            executorService.awaitTermination(500, TimeUnit.MILLISECONDS);
        } finally {
            if(executorService != null) {
                executorService.shutdown();
            }
        }
    }//end

}