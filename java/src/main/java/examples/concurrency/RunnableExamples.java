package examples.concurrency;

import resources.SampleTask;
import java.util.List;

public class RunnableExamples {

    public void exp_example1(List<String> output) throws InterruptedException {
        //summary: Runnable Example 1
        //resources: SampleTask
        Runnable sampleTask = new SampleTask(output, "Sample-Task-1", 1000);
        Thread thread = new Thread(sampleTask);
        thread.start();
        thread.join();
    }//end

    public void exp_example2(List<String> output) throws InterruptedException {
        //summary: Runnable Example 2
        Runnable task = () -> {
            output.add("Task 'Sample-Task-1' is completed.");
        };
        Thread thread = new Thread(task);
        thread.start();
        thread.join();
    }//end
}