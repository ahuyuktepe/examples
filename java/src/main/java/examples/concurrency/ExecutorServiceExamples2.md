### Scheduling Single Task Example
Example
```
ScheduledExecutorService executorService = null;
try {
    Runnable task = () -> {
        Instant now = Instant.now();
        output.add("Executing Task 1 at '" + now.toEpochMilli() +"'.");
    };
    executorService = Executors.newSingleThreadScheduledExecutor();
    Instant now = Instant.now();
    output.add("Scheduled Task 1 at '" + now.toEpochMilli() +"'.");
    executorService.schedule(task, 1500, TimeUnit.MILLISECONDS);
    // Wait until all tasks are done
    executorService.awaitTermination(2000, TimeUnit.MILLISECONDS);
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Scheduled Task 1 at '1619926736338'.
Executing Task 1 at '1619926737862'.
```
### Scheduling Single Task Multiple Times Example
Example
```
ScheduledExecutorService executorService = null;
try {
    Runnable task = () -> {
        Instant now = Instant.now();
        output.add("Executing Task 1 at '" + now.toEpochMilli() +"'.");
    };
    executorService = Executors.newSingleThreadScheduledExecutor();
    Instant now = Instant.now();
    output.add("Scheduled Task 1 at '" + now.toEpochMilli() +"'.");
    executorService.scheduleWithFixedDelay(task, 100, 100, TimeUnit.MILLISECONDS);
    // Wait until all tasks are done
    executorService.awaitTermination(500, TimeUnit.MILLISECONDS);
} finally {
    if(executorService != null) {
        executorService.shutdown();
    }
}
```
Output
```
Scheduled Task 1 at '1619926738364'.
Executing Task 1 at '1619926738474'.
Executing Task 1 at '1619926738583'.
Executing Task 1 at '1619926738692'.
Executing Task 1 at '1619926738801'.
```
