package examples.util;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ComparatorExamples {

    public void exp_example1(List<String> output) throws IOException {
        //summary: Method of Example 2
        Comparator<Integer> ascendingOrder = (Integer val1, Integer val2) -> {
            if(val1 > val2) {
                return 1;
            } else if(val1 < val2) {
                return -1;
            } else {
                return 0;
            }
        };
        Stream<Integer> numbers = Stream.of(12, 101, 5);
        output.add("Original Numbers");
        numbers.forEach((val) -> output.add(val.toString()));
        numbers = Stream.of(12, 101, 5);
        Stream<Integer> sortedStream = numbers.sorted(ascendingOrder);
        output.add("Sorted Numbers(Ascending)");
        sortedStream.forEach((val) -> output.add(val.toString()));
    }

    public void exp_example2(List<String> output) throws IOException {
        //summary: Method reversed Example 2
        Comparator<Integer> ascendingOrder = (Integer val1, Integer val2) -> {
            if(val1 > val2) {
                return 1;
            } else if(val1 < val2) {
                return -1;
            } else {
                return 0;
            }
        };
        Comparator<Integer> descendingOrder = ascendingOrder.reversed();
        Stream<Integer> numbers = Stream.of(12, 101, 5);
        output.add("Original Numbers");
        numbers.forEach((val) -> output.add(val.toString()));
        numbers = Stream.of(12, 101, 5);
        Stream<Integer> sortedStream = numbers.sorted(descendingOrder);
        output.add("Sorted Numbers(Descending)");
        sortedStream.forEach((val) -> output.add(val.toString()));
    }
}