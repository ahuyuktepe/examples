package examples.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamExamples {

    public void exp_example1(List<String> output) throws IOException {
        //summary: Method anyMatch Example
        List<String> names = Arrays.asList("George", "John", "Tom", "Ellen");
        Stream<String> nameStream = names.stream();
        boolean anyEndsWithN = nameStream.anyMatch((name) -> { return name.endsWith("n"); });
        output.add("Names: " + names);
        output.add("Does any ends with 'n': " + anyEndsWithN);
    }

    public void exp_example2(List<String> output) throws IOException {
        //summary: Method allMatch Example
        List<String> names = Arrays.asList("George", "John", "Tom", "Ellen");
        Stream<String> nameStream = names.stream();
        boolean allEndsWithN = nameStream.allMatch((name) -> { return name.endsWith("n"); });
        output.add("Names: " + names);
        output.add("Does all end with 'n': " + allEndsWithN);
    }

    public void exp_example3(List<String> output) throws IOException {
        //summary: Method dropWhile Example
        List<String> names = Arrays.asList("George", "John", "Tom", "Ellen");
        Stream<String> nameStream = names.stream();
        output.add("Names: " + names);
        Stream<String> filteredStream = nameStream.filter((name) -> { return name.endsWith("n"); });
        output.add("Names ending with 'n'");
        filteredStream.forEach((value) -> { output.add(value); });
    }

    public void exp_example4(List<String> output) throws IOException {
        //summary: Method forEach Example
        List<String> names = Arrays.asList("George", "John", "Tom", "Ellen");
        Stream<String> nameStream = names.stream();
        output.add("Names:");
        nameStream.forEach((value) -> { output.add(value); });
    }

    public void exp_example5(List<String> output) throws IOException {
        //summary: Method map Example
        List<String> names = Arrays.asList("George", "John", "Tom", "Ellen");
        Stream<String> nameStream = names.stream();
        Stream<String> newNameStream = nameStream.map((name) -> { return "[" + name + "]"; });
        newNameStream.forEach((value) -> { output.add(value); });
    }

    public void exp_example6(List<String> output) throws IOException {
        //summary: Method of Example 1
        Stream<String> nameStream = Stream.of("George");
        nameStream.forEach((value) -> { output.add(value); });
    }

    public void exp_example7(List<String> output) throws IOException {
        //summary: Method of Example 2
        Stream<String> nameStream = Stream.of("George", "John", "Tom");
        nameStream.forEach((value) -> { output.add(value); });
    }

    public void exp_example8(List<String> output) throws IOException {
        //summary: Method generate Example
        Supplier<Integer> numberSupplier = () -> {
            Random random = new Random();
            return random.nextInt(10);
        };
        Stream<Integer> numberStream = Stream.generate(numberSupplier).limit(3);
        numberStream.forEach((value) -> { output.add(value.toString()); });
    }

    public void exp_example9(List<String> output) throws IOException {
        //summary: Method iterate Example
        List<Integer> numbers = Arrays.asList(10, 4, 12, 3, 5);
        Random random = new Random();
        Stream<Integer> numberStream  = Stream.iterate(1, (num) -> {
            int index = random.nextInt(5);
            return numbers.get(index);
        }).limit(5);
        numberStream.forEach((num) -> output.add(num.toString()));
    }
}