package examples.io;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class BufferedReaderExamples {
    public void exp_objectInputStream_1(List<String> output) throws IOException {
        //summary: Reading File Line By Line
        File srcFile = new File("resources/test-file.txt");
        InputStream fileInputStream = new FileInputStream(srcFile);
        InputStreamReader fileInputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader reader = new BufferedReader(fileInputStreamReader);
        String line = reader.readLine();
        while(line != null) {
            output.add(line);
            line = reader.readLine();
        }
    }

    public void exp_objectInputStream_2(List<String> output) throws IOException {
        //summary: Read Lines Via Stream
        File srcFile = new File("resources/test-file.txt");
        InputStream fileInputStream = new FileInputStream(srcFile);
        InputStreamReader fileInputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader reader = new BufferedReader(fileInputStreamReader);
        Stream<String> lines = reader.lines();
        Optional<String> firstLine = lines.findAny();
        String lineText = firstLine.get();
        output.add("First Line:" + lineText);
    }
}
