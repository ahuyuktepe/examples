package examples.io;

import resources.Vehicle;

import java.io.*;
import java.util.List;

public class InputStreamExamples {
    public void exp_fileInputStream_1(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read File By Line
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        byte [] bytesArr = new byte[7];
        fileInputStream.read(bytesArr);
        String str = "";
        for(byte byteVal: bytesArr) {
            char charVal = (char) byteVal;
            String strVal = String.valueOf(charVal);
            str += strVal;
        }
        output.add(str);
        fileInputStream.close();
    }

    public void exp_fileInputStream_2(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read All Content Into Byte Array
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        byte [] bytesArr = fileInputStream.readAllBytes();
        String str = "";
        for(byte byteVal: bytesArr) {
            char charVal = (char) byteVal;
            String strVal = String.valueOf(charVal);
            str += strVal;
        }
        output.add("File Content: " + str);
        fileInputStream.close();
    }

    public void exp_fileInputStream_3(List<String> output) throws IOException, FileNotFoundException {
        //summary: Read File Partially Into Byte Array
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        byte [] bytesArr = new byte[10];
        fileInputStream.skip(3);
        fileInputStream.read(bytesArr, 0, 4);
        String str = "";
        for(byte byteVal: bytesArr) {
            char charVal = (char) byteVal;
            String strVal = String.valueOf(charVal);
            str += strVal;
        }
        output.add("File Content: " + str);
        fileInputStream.close();
    }

    public void exp_fileInputStream_4(List<String> output) throws IOException, FileNotFoundException {
        //summary: Estimate Available Bytes In InputStream
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        int availableByteCount = fileInputStream.available();
        output.add("Available Byte Count: " + availableByteCount);
        fileInputStream.close();
    }

    public void exp_objectInputStream_1(List<String> output) throws IOException, ClassNotFoundException {
        //summary: Reading Object From File
        //resources: resources.Vehicle
        // Writing Vehicle Type Object To File
        Vehicle vehicle = new Vehicle("Toyota", "Corolla");
        output.add("Source Object: " + vehicle);
        FileOutputStream fileOutputStream = new FileOutputStream("resources/vehicle-data.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(vehicle);
        // Reading File Content To Object
        FileInputStream fileInputStream = new FileInputStream("resources/vehicle-data.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Object obj = objectInputStream.readObject();
        output.add("Deserialized Object: " + obj);
    }
}