package examples.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class OutputStreamWriterExamples {
    public void exp_objectInputStream_2(List<String> output) throws IOException, ClassNotFoundException {
        //summary: Writing Given Encoded String To File
        String text = "This is a test";
        byte[] bytesArr = text.getBytes(StandardCharsets.ISO_8859_1.name());
        OutputStream outputStream = new FileOutputStream("resources/output-stream-file.txt");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStream.write(bytesArr);
    }
}
