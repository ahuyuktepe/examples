### Read File By Line
classes.Example
```
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        byte [] bytesArr = new byte[7];
        fileInputStream.read(bytesArr);
        String str = "";
        for(byte byteVal: bytesArr) {
            char charVal = (char) byteVal;
            String strVal = String.valueOf(charVal);
            str += strVal;
        }
        output.add(str);
        fileInputStream.close();
    }

```
Output
```
Test Fi
```
### Read All Content Into Byte Array
classes.Example
```
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        byte [] bytesArr = fileInputStream.readAllBytes();
        String str = "";
        for(byte byteVal: bytesArr) {
            char charVal = (char) byteVal;
            String strVal = String.valueOf(charVal);
            str += strVal;
        }
        output.add("File Content: " + str);
        fileInputStream.close();
    }

```
Output
```
File Content: Test File
This is a test file.
Please ignore
```
### Read File Partially Into Byte Array
classes.Example
```
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        byte [] bytesArr = new byte[10];
        fileInputStream.skip(3);
        fileInputStream.read(bytesArr, 0, 4);
        String str = "";
        for(byte byteVal: bytesArr) {
            char charVal = (char) byteVal;
            String strVal = String.valueOf(charVal);
            str += strVal;
        }
        output.add("File Content: " + str);
        fileInputStream.close();
    }

```
Output
```
File Content: t Fi      
```
### Estimate Available Bytes In InputStream
classes.Example
```
        InputStream fileInputStream = new FileInputStream("resources/test-file.txt");
        int availableByteCount = fileInputStream.available();
        output.add("Available Byte Count: " + availableByteCount);
        fileInputStream.close();
    }

```
Output
```
Available Byte Count: 46
```
### Reading Object From File
classes.Example
```
        //resources: resources.Vehicle
        // Writing Vehicle Type Object To File
        Vehicle vehicle = new Vehicle("Toyota", "Corolla");
        output.add("Source Object: " + vehicle);
        FileOutputStream fileOutputStream = new FileOutputStream("resources/vehicle-data.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(vehicle);
        // Reading File Content To Object
        FileInputStream fileInputStream = new FileInputStream("resources/vehicle-data.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Object obj = objectInputStream.readObject();
        output.add("Deserialized Object: " + obj);
    }
```
Output
```
Source Object: Vehicle{ make='Toyota, model='Corolla}
Deserialized Object: Vehicle{ make='Toyota, model='Corolla}
```
