### Date Initialization 1
classes.Example
```
long milliSeconds = System.currentTimeMillis();
Date date = new Date(milliSeconds);
output.add("Current date in milli-seconds: " + milliSeconds);
output.add("Current date : " + date);
```
Output
```
Current date in milli-seconds: 1617582161387
Current date : 2021-04-04
```
### Date Initialization 2
classes.Example
```
long oneMinInMilliseconds = 60000;
long oneHourInMilliseconds = 60 * oneMinInMilliseconds;
long oneDayInMilliseconds = 24 * oneHourInMilliseconds;
long threeDaysInMillSeconds = 3 * oneDayInMilliseconds;
output.add("3 day in milli-seconds: " + threeDaysInMillSeconds);
Date threeDaysAfterOrigin = new Date(threeDaysInMillSeconds);
output.add("Jan 1, 1970 + 3 days: " + threeDaysAfterOrigin);
Date threeDaysBeforeOrigin = new Date(threeDaysInMillSeconds * -1);
output.add("Jan 1, 1970 - 3 days: " + threeDaysBeforeOrigin);
```
Output
```
3 day in milli-seconds: 259200000
Jan 1, 1970 + 3 days: 1970-01-03
Jan 1, 1970 - 3 days: 1969-12-28
```
### From java.sql.Date to java.time.LocalDate
classes.Example
```
long milliSeconds = System.currentTimeMillis();
Date date = new Date(milliSeconds);
output.add("java.sql.Date : " + date);
LocalDate localDate = date.toLocalDate();
output.add("java.time.LocalDate : " + date);
```
Output
```
java.sql.Date : 2021-04-04
java.time.LocalDate : 2021-04-04
```
