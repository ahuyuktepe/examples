package examples.sql;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class SqlDateExamples {
    public void exp_init_1(List<String> output) {
        //summary: Date Initialization 1
        long milliSeconds = System.currentTimeMillis();
        Date date = new Date(milliSeconds);
        output.add("Current date in milli-seconds: " + milliSeconds);
        output.add("Current date : " + date);
    }

    public void exp_init_2(List<String> output) {
        //summary: Date Initialization 2
        long oneMinInMilliseconds = 60000;
        long oneHourInMilliseconds = 60 * oneMinInMilliseconds;
        long oneDayInMilliseconds = 24 * oneHourInMilliseconds;
        long threeDaysInMillSeconds = 3 * oneDayInMilliseconds;
        output.add("3 day in milli-seconds: " + threeDaysInMillSeconds);
        Date threeDaysAfterOrigin = new Date(threeDaysInMillSeconds);
        output.add("Jan 1, 1970 + 3 days: " + threeDaysAfterOrigin);
        Date threeDaysBeforeOrigin = new Date(threeDaysInMillSeconds * -1);
        output.add("Jan 1, 1970 - 3 days: " + threeDaysBeforeOrigin);
    }

    public void exp_toLocalDate(List<String> output) {
        //summary: From java.sql.Date to java.time.LocalDate
        long milliSeconds = System.currentTimeMillis();
        Date date = new Date(milliSeconds);
        output.add("java.sql.Date : " + date);
        LocalDate localDate = date.toLocalDate();
        output.add("java.time.LocalDate : " + date);
    }
}