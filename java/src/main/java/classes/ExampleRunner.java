package classes;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExampleRunner {

    public Optional<List<Example>> fetchExamples(String clsPath) {
        try {
            String [] path = new String [] {
                System.getProperty("user.dir"), "src", "main", "java", clsPath.replace(".", File.separator)
            };
            String clsFilePath = String.join(File.separator, path);
            List<Example> examples = this.parseClass(clsFilePath + ".java");
            return Optional.of(examples);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
        return Optional.ofNullable(null);
    }

    private List<Example> parseClass(String filePath) throws Exception {
        List<Example> examples = new ArrayList<Example>();
        BufferedReader reader = getFileReader(filePath);
        String line = null;
        int indentCount = 0;
        boolean isInMethod = false;
        Example currentExample = null;
        while((line = reader.readLine()) != null) {
            if(isMethodDefinitionLine(line)) {
                isInMethod = true;
                currentExample = buildExample(line);
            } else if(isMethodEndLine(line)) {
                isInMethod = false;
                examples.add(currentExample);
            } else if(isSummaryLine(line)) {
                String text = line.replace("//summary:", "").trim();
                currentExample.setSummary(text);
                indentCount = getIndentCount(line);
            } else if(isDescriptionLine(line)) {
                String text = line.replace("//description:", "").trim();
                currentExample.setDescription(text);
            } else if(isResourcesLine(line)) {
                String text = line.replace("//resources:", "").trim();
                currentExample.setResources(text);
            } else if(isInMethod) {
                String trimmedLine = line.replaceAll("^\\s{"+indentCount+"}", "");
                currentExample.appendCodeLine(trimmedLine);
            }
        }
        return examples;
    }

    private int getIndentCount(String line) {
        Pattern pattern = Pattern.compile("^\\s*");
        Matcher matcher = pattern.matcher(line);
        boolean doesExist = matcher.find();
        if(doesExist) {
            return matcher.group(0).length();
        }
        return 0;
    }

    private Example buildExample(String line) throws Exception {
        Optional<String> optionalValue = fetchMethodName(line);
        if(optionalValue.isPresent()) {
            String methodName = optionalValue.get();
            Example example = new Example();
            example.setMethodName(methodName);
            return example;
        }
        throw new Exception("Method name is in line '" + line + "' not valid");
    }

    private boolean isMethodDefinitionLine(String line) {
        return line.contains("public void exp_");
    }

    private Optional<String> fetchMethodName(String line) {
        Pattern pattern = Pattern.compile("exp_(\\w|\\d)+", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(line);
        boolean doesExist = matcher.find();
        String methodName = null;
        if(doesExist) {
            methodName = matcher.group(0).replace("exp_", "");
        }
        return Optional.ofNullable(methodName);
    }

    private boolean isSummaryLine(String line) {
        return line.contains("//summary:");
    }

    private boolean isDescriptionLine(String line) {
        return line.contains("//description:");
    }

    private boolean isResourcesLine(String line) {
        return line.contains("//resources:");
    }

    private boolean isMethodEndLine(String line) {
        return line.contains("}//end");
    }

    private BufferedReader getFileReader(String filePath) throws IOException {
        // Read Class File Content
        File file = new File(filePath);
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamReader streamReader = new InputStreamReader(fileInputStream);
        BufferedReader reader = new BufferedReader(streamReader);
        return reader;
    }

    public void runExamplesAndSaveOutput(List<Example> examples, String clsName) throws Exception {
        // Instantiate Class
        Class<?> clazz = Class.forName(clsName);
        Constructor<?> constructor = clazz.getConstructor();
        Object instance = constructor.newInstance();
        List<String> output = new ArrayList<String>();
        // Execute Methods
        for(Example example: examples) {
            String methodName = example.getCallableMethodName();
            Method method = clazz.getMethod(methodName, List.class);
            method.invoke(instance, output);
            example.setOutput(output);
            output.clear();
        }
    }

    public void saveAsMarkup(List<Example> examples, String clsPath) {
        try {
            String [] path = new String [] {
                System.getProperty("user.dir"), "src", "main", "java", clsPath.replace(".", File.separator)
            };
            String clsFilePath = String.join(File.separator, path);
            String fileContent = "";
            for(Example example: examples) {
                fileContent += example.getMarkup();
            }
            File file = new File(clsFilePath + ".md");
            FileWriter writer = new FileWriter(file);
            writer.write(fileContent);
            writer.flush();
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }
}