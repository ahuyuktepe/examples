package resources;

import java.util.concurrent.ThreadFactory;

public class SampleThreadFactory implements ThreadFactory {
    @Override
    public Thread newThread(Runnable runnable) {
        return new Thread(runnable);
    }
}
