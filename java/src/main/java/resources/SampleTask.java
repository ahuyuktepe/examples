package resources;

import java.util.List;

public class SampleTask implements Runnable  {
    private int delayInMs;
    private List<String> output;
    private String name;

    public SampleTask(List<String> output, String name, int delay) {
        delayInMs = delay;
        this.output = output;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            if(this.delayInMs != 0) {
                output.add("Task '" + this.name + "' is sleeping for " + delayInMs + " ms.");
                Thread.sleep(this.delayInMs);
            }
            output.add("Task '" + this.name + "' is completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
