package resources;

import java.io.Serializable;

public class Vehicle implements Serializable {
    String make;
    String model;

    public Vehicle(String make, String model) {
        this.make = make;
        this.model = model;
    }

    @Override
    public String toString() {
        return "Vehicle{ make='" + make + ", model='" + model + "}";
    }
}
