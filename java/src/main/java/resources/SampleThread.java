package resources;

import java.util.List;

public class SampleThread extends Thread {
    private int delayInMs;
    private List<String> output;

    public SampleThread(List<String> output, int delay) {
        delayInMs = delay;
        this.output = output;
    }

    public SampleThread(Runnable runnable) {
        super(runnable);
    }

    @Override
    public void run() {
        try {
            if(this.delayInMs != 0) {
                output.add("Thread '" + this.getName() + "' is sleeping for " + delayInMs + " ms.");
                Thread.sleep(this.delayInMs);
            }
            output.add("Thread '" + this.getName() + "' is completed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
