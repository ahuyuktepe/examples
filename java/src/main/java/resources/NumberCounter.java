package resources;

import java.util.List;

public class NumberCounter {
    public static int counter = 0;

    public synchronized void increase(String threadName, List<String> output) {
        for(int i=1; i <= 5; i++) {
            counter++;
            output.add("Thread '" + threadName +"' increased counter by 1 to " + counter);
        }
    }
}
