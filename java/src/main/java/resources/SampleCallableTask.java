package resources;

import java.util.concurrent.Callable;

public class SampleCallableTask implements Callable<String> {
    private String text;

    public SampleCallableTask(String text) {
        this.text = text;
    }

    @Override
    public String call() throws Exception {
        return this.text;
    }
}
