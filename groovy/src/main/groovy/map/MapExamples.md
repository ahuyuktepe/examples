## Map Examples
```
def sourceMap = [
    username: 'test_user',
    first_name: 'Test',
    last_name: 'User',
    address: '100 Test Street Hillsborough NJ'
]
```

#### Counting key's containing '_name' in given map
```
sourceMap.count { key, value -> 
    key.toString().contains('_name') 
}
```
> 2

#### Is 'username' key value is 'test_user'
```
map.any { key, value -> 
    key == 'username' && value == 'test_user' 
}
```
> true

#### Count keys with ending with ['_name','name']
```
map.countBy { key, value ->
    if(key.toString().endsWith('_name')) {
        "Ends With '_name'"
    } else if(key.toString().endsWith('name')) {
        "Ends With 'name'"
    } else {
        "Other'"
    }
}
```
> [Ends With 'name':1, Ends With '_name':2, Other':1]

#### Converting map to list
```
List list = map.collect { key, value -> 
    key.toString().endsWith('_name') ? value : null 
}
```
> [null, Test, User, null]

#### Building a map from another map
```
Map newMap =  map.collectEntries { key, value -> 
    key.toString().endsWith('_name') ? [key: value] : [:] 
}
```
> [first_name:Test, last_name:User]

#### Filtering map
```
Map filteredMap = map.findAll { key, value -> 
    key.toString().endsWith('_name') 
}
```
> [first_name:Test, last_name:User]

#### Fetching first value from map whose associated key ends with '_name'
```
Object obj = map.findResult { key, value ->
    if (key.toString().endsWith('_name')) {
        return value
    }
}
```
> Test

#### Fetching first key value pair when condition satisfies
```
Map obj1 = map.findResult {
    if (it.key.toString().endsWith('_name')) {
        return it
    }
}
```
> [first_name:Test]

#### Fetching first object when condition satisfies with default value
```
Object obj1 = map.findResult('Default Value') { key, value ->
    if (key.toString().endsWith('_name1')) {
        return value
    }
}
```
> Default Value

Get entry from map if not found add default value for key and return
```
Object value = map.get('first_name', 'N/A')
```
> Test

#### Group given map into map according ending text of key
```
Map retMap = map.groupBy({
    it.key.toString().endsWith('name') ? "Key Ends With 'name'" : "Key Does Not End With 'name'"
})
```
> [Key Ends With 'name':[username:test_user, first_name:Test, last_name:User], Key Does Not End With 'name':[address:100 Test Street Hillsborough NJ]]

#### Inject into map values into given list with key's ends with 'name'
```
def result = map.inject(['George']) { givenList, entryKey, entryValue ->
    if(entryKey.toString().endsWith('name')) {
        return givenList + [entryValue]
    }
    return givenList
}
```
> [George, test_user, Test, User]

#### Get diff of 2 given maps
```
Map minusMap = [username: 'test_user']
Map diffMap = map - minusMap
```
> [first_name:Test, last_name:User, address:100 Test Street Hillsborough NJ]

#### Get diff of 2 given maps
```
Map plusMap = [username: 'test_user']
Map mergedMap = map - plusMap
```
> [username:updated_test_user, first_name:Test, last_name:User, address:100 Test Street Hillsborough NJ]


#### Sorting map by keys in descending order
```
map.sort {a, b -> a.key <=> b.key }
```
> [address:100 Test Street Hillsborough NJ, first_name:Test, last_name:User, username:test_user]

#### Fetching map entries by given list of keys
```
map.subMap(['username','first_name'])
```
> [username:test_user, first_name:Test]


#### Getting first 2 key/value pairs from given map
```
map.take(2)
```
> [username:test_user, first_name:Test]