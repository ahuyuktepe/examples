package map
// https://docs.groovy-lang.org/docs/latest/html/groovy-jdk/
def map = [
    username: 'test_user',
    first_name: 'Test',
    last_name: 'User',
    address: '100 Test Street Hillsborough NJ'
]

closureExamples(map)

def closureExamples(Map map) {
    print('''
def map = [
    username: 'test_user',
    first_name: 'Test',
    last_name: 'User',
    address: '100 Test Street Hillsborough NJ\'
]''')
    // count closure
    println("\n\nCounting key's containing '_name' in given map")
    int count = map.count {key, value -> key.toString().contains('_name') }
    println("```map.count {key, value -> key.toString().contains('_name') }```")
    println(": $count")

    // any closure
    println("\nIs 'username' key value is 'test_user'")
    boolean hasValue = map.any {key, value -> key == 'username' && value == 'test_user' }
    println("map.any {key, value -> key == 'username' && value == 'test_user' }")
    println(": $hasValue")

    // countBy closure
    println("\nCount keys with ending with ['_name','name']")
    def results = map.countBy { key, value ->
        if(key.toString().endsWith('_name')) {
            "Ends With '_name'"
        } else if(key.toString().endsWith('name')) {
            "Ends With 'name'"
        } else {
            "Other'"
        }
    }
    println("map.countBy { key, value ->\n" +
            "        if(key.toString().endsWith('_name')) {\n" +
            "            \"Ends With '_name'\"\n" +
            "        } else if(key.toString().endsWith('name')) {\n" +
            "            \"Ends With 'name'\"\n" +
            "        } else {\n" +
            "            \"Other'\"\n" +
            "        }\n" +
            "}")
    println(": $results")

    // collect closure
    println("\nConverting map to list")
    def list = map.collect { key, value -> key.toString().endsWith('_name') ? value : null }
    println("List list = map.collect { key, value -> key.toString().endsWith('_name') ? value : null }")
    println(": $list")

    // collectEntries closure
    println("\nConverting map to map")
    Map newMap = map.collectEntries { key, value -> key.toString().endsWith('_name') ? [(key): value] : [:] }
    println("Map newMap =  map.collectEntries { key, value -> key.toString().endsWith('_name') ? [key: value] : [:] }")
    println(": $newMap")

    // findAll closure
    println("\nFiltering map")
    Map filteredMap = map.findAll { key, value -> key.toString().endsWith('_name') }
    println("Map filteredMap = map.findAll { key, value -> key.toString().endsWith('_name') }")
    println(": $filteredMap")

    // findResult closure
    println("\nFetching first object when condition satisfies")
    Object obj = map.findResult { key, value ->
        if (key.toString().endsWith('_name')) {
            return value
        }
    }
    println("Object obj = map.findResult { key, value ->\n" +
            "  if (key.toString().endsWith('_name')) {\n" +
            "    return value\n" +
            "  }\n" +
            "}")
    println(": $obj")

    // findResult closure
    println("\nFetching first key value pair when condition satisfies")
    Map obj1 = map.findResult {
        if (it.key.toString().endsWith('_name')) {
            return [(it.key): it.value]
        }
    }
    println("Map obj1 = map.findResult {\n" +
            "  if (it.key.toString().endsWith('_name')) {\n" +
            "    return it\n" +
            "  }\n" +
            "}" +
            "}")
    println(": $obj1")

    // findResult closure with default
    println("\nFetching first object when condition satisfies with default value")
    Object obj2 = map.findResult('Default Value') { key, value ->
        if (key.toString().endsWith('_name1')) {
            return value
        }
    }
    println("Object obj1 = map.findResult('Default Value') { key, value ->\n" +
            "  if (key.toString().endsWith('_name1')) {\n" +
            "    return value\n" +
            "  }\n" +
            "}")
    println(": $obj2")

    // get closure
    println("\nGet entry from map")
    Object value = map.get('first_name')
    println("Object value = map.get('first_name', 'N/A')")
    println(": $value")

    // groupBy closure
    println("\nGroup given map into map according ending text of key")
    def map3 = map.groupBy({it.key.toString().endsWith('name')? "Key Ends With 'name'": "Key Does Not End With 'name'" })
    println("Map retMap = map.groupBy({\n" +
            "  it.key.toString().endsWith('name')? \"Key Ends With 'name'\" : \"Key Does Not End With 'name'\"\n" +
            "})")
    print(": $map3")

    // inject closure
    println("\n\nInject into map values into given list with key's ends with 'name'")
    def result = map.inject(['George']) { givenList, entryKey, entryValue ->
        if(entryKey.toString().endsWith('name')) {
            return givenList + [entryValue]
        }
        return givenList
    }
    println("def result = map.inject(['George']) { givenList, entryKey, entryValue ->\n" +
            "  if(entryKey.toString().endsWith('name')) {\n" +
            "    return givenList + [entryValue]\n" +
            "  }\n" +
            "  return givenList\n" +
            "}")
    println(": $result")

    // minus method
    println("\n\n Get diff of 2 given maps")
    Map minusMap = [username: 'test_user']
    Map diffMap = map - minusMap
    println("Map minusMap = [username: 'test_user']\n" +
            "Map diffMap = map - minusMap")
    println(": $diffMap")

    // plus method
    println("\n\n Get diff of 2 given maps")
    Map plusMap = [username: 'updated_test_user']
    Map mergedMap = map + plusMap
    println("Map plusMap = [username: 'test_user']\n" +
            "Map mergedMap = map - plusMap")
    println(": $mergedMap")

    // sort method
    println("\n\n Sorting map by keys in descending order")
    Map sortedMap = map.sort {a, b -> a.key <=> b.key }
    println("map.sort {a, b -> a.key <=> b.key }")
    println(": $sortedMap")

    // subMap method
    println("\n\n Fetching map entries by given list of keys")
    Map retMap = map.subMap(['username','first_name'])
    println("map.subMap(['username','first_name'])")
    println(": $retMap")

    // subMap method
    println("\n\n Getting first 2 key/value pairs from given map")
    retMap = map.take(2)
    println("map.take(2)")
    println(": $retMap")
}
