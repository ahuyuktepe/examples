### List Examples
```
List sourceList = ['john', 'edward', 'george', 'johnny']
```

#### Finding strings starting with 'joh'
```
def filteredList = sourceList.findAll { String element -> 
    element.startsWith('joh') 
}
```
> [john, johnny]

#### Getting first element from list
```
def element = sourceList.first()
```
> john

#### Getting elements from index 0 to 2(inclusive)
```
def filteredList = sourceList.getAt(0..2)
```
> [john, edward, george]

#### Getting elements from index 0 to 2(exclusive)
```
def filteredList = sourceList.getAt(0..<2)
```
> [john, edward]

#### Getting elements from index 0 to 2 via int type params
```
int from = 0
int to = 1
def filteredList = sourceList.getAt(from..<to)
```    
> [john]

#### Getting all elements but the last one
```
def filteredList = sourceList.init()
```
> [john, edward, george]

#### Getting all elements but the first one
```
def filteredList = sourceList.tail()
```
> [edward, george, johnny]

#### Getting first 2 elements from beginning of the list
```
def filteredList = sourceList.take(2)
```
> [john, edward]

#### Getting last 2 elements from end of the list
```
def filteredList = sourceList.takeRight(2)
```
> [george, johnny]