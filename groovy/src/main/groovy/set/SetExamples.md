### Set Examples

#### Visiting each element
```
Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()
String respStr = ''
sourceSet.each {
    respStr += " $it" |
}
```
> george | test | test1 | user |

#### Visiting each element with index of each element
```
Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()
String respStr = ''
sourceSet.eachWithIndex { element, index ->
    respStr += "[$index: $element] "
}
```
> [0: george] [1: test] [2: test1] [3: user]

#### Find all elements starting with 'geo'
```
Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()
Set<String> elements = sourceSet.findAll { 
    it.startsWith('geo') 
}
```
> [george]

#### Subtract one set from another set
```
Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()
Set elementsToRemove = ['george']
Set diffElements = sourceSet - elementsToRemove
```
> [test, test1, user]


#### Merge 2 sets into new set
```
Set elementsToAdd = ['orange', 'car']
Set mergedElemetns = sourceSet + elementsToAdd
```
> [orange, george, test, test1, user, car]

#### Add element to the end of set
```
Set elements = ['test']
Set newSet = elements << 'orange'
```
> [test, orange]

#### Find common elements in given 2 sets
```
Set elements = ['orange' ,'apple', 'car', 'test'] as Set
Set commonElements = sourceSet.intersect(elements)
```
> [test]

#### Split given set into 2 sets by starting string
```
Set elements = ['test1' ,'apple', 'car', 'test', 'test2'] as Set
List listOfSets = elements.split {it.startsWith('test') }
```
> [[test1, test, test2], [apple, car]]