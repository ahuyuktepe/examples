package set

Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()

// each closure
println("Visiting each element")
String respStr = ""
sourceSet.each {
    respStr += "$it | "
}
println("Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()")
println("String respStr = ''\n" +
        "sourceSet.each {\n" +
        "    respStr += \" \$it\" | \n" +
        "}")
println("> $respStr")

// eachWithIndex closure
println("\nVisiting each element with index of each element")
respStr = ""
sourceSet.eachWithIndex { element, index ->
    respStr += "[$index: $element] "
}
println("Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()")
println("String respStr = ''\n" +
        "sourceSet.eachWithIndex { element, index ->\n" +
        "    respStr += \"[\$index: \$element] \"\n" +
        "}")
println("> $respStr")

// findAll closure
println("\nFind all elements starting with 'geo'")
Set<String> elements = sourceSet.findAll { it.startsWith('geo') }
println("Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()")
println("Set<String> elements = sourceSet.findAll { it.startsWith('geo') }")
println("> $elements")

// minus closure
println("\nSubtract one set from another set")
Set elementsToRemove = ['george']
Set diffElements = sourceSet - elementsToRemove
println("Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()\n" +
        "Set elementsToRemove = ['george']\n" +
        "Set diffElements = sourceSet - elementsToRemove")
println("> $diffElements")

// plus closure
println("\nMerge 2 sets into new set")
Set elementsToAdd = ['orange', 'car']
Set mergedElements = sourceSet + elementsToAdd
println("Set sourceSet = ['test', 'test1', 'user', 'george'].toSet()")
println("Set elementsToAdd = ['orange', 'car']\n" +
        "Set mergedElemetns = sourceSet + elementsToAdd")
println("> $mergedElements")

// leftShift
println("\nAdd element to the end of set")
elements = ['test'].toSet()
Set newSet = elements << 'orange'
println("Set elements = ['test']\n" +
        "Set newSet = sourceSet << 'orange'")
println("> $newSet")

// intersect closure
println("\nFind common elements in given 2 sets")
elements = ['orange' ,'apple', 'car', 'test'] as Set
Set commonElements = sourceSet.intersect(elements)
println("Set elements = ['orange' ,'apple', 'car', 'test'] as Set\n" +
        "Set commonElements = sourceSet.intersect(elements)")
println("> $commonElements")

// split closure
println("\nSplit given set into 2 sets by starting string")
elements = ['test1' ,'apple', 'car', 'test', 'test2'] as Set
List listOfSets = elements.split {it.startsWith('test') }
println("Set elements = ['test1' ,'apple', 'car', 'test', 'test2'] as Set\n" +
        "List listOfSets = elements.split {it.startsWith('test') }")
println("> $listOfSets")

