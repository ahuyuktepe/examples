// https://docs.groovy-lang.org/docs/latest/html/groovy-jdk/
filterList()

def filterList() {
    List list = ['john', 'edward', 'george', 'johnny']
    println(list)

    println("\nFinding strings starting with 'joh'")
    def filteredList = list.findAll { String element-> element.startsWith('joh') }
    println(filteredList)

    println("\nGetting first element from list")
    def element = list.first()
    println(element)

    println("\nGetting elements from index 0 to 2(inclusive)")
    filteredList = list.getAt(0..2)
    println(filteredList)

    println("\nGetting elements from index 0 to 2(exclusive)")
    filteredList = list.getAt(0..<2)
    println(filteredList)

    println("\nGetting elements from index 0 to 2 via int type params")
    int from = 0
    int to = 1
    filteredList = list.getAt(from..<to)
    println(filteredList)

    println("\nGetting all elements but the last one")
    filteredList = list.init()
    println(filteredList)

    println("\nGetting all elements but the first one")
    filteredList = list.tail()
    println(filteredList)

    println("\nGetting first 2 elements from beginning of the list")
    filteredList = list.take(2)
    println(filteredList)

    println("\nGetting first 2 elements from end of the list")
    filteredList = list.takeRight(2)
    println(filteredList)

}

def eachClosure() {
    List list = ['a', 'b', 'c']
    println(list)

    println("\nPrinting each element")
    list.each { println("Element: ${it}") }

    println("\nPrintln each element with index")
    list.eachWithIndex{ String element, int i -> println("Element: ${element}, index: ${i}")}
}

def removeFromList() {
    List list = [0, 1, 2, 3, 4]
    println(list)

    println("Removing 2 elements from beginning of the list")
    def updatedList = list.drop(2)
    println(updatedList)

    println("Removing 2 elements from end of the list")
    updatedList = list.dropRight(2)
    println(updatedList)

    println("Removing elements less then 3")
    updatedList = list.dropWhile {it < 3}
    println(updatedList)

    def elementsToRemove = [3, 4]
    println("Removing elements less then $elementsToRemove")
    updatedList = list - elementsToRemove
    println(updatedList)


}

def addToList() {
    List list = [1]
    println(list)

    println("Adding 2")
    list << 2
    println(list)

    println("Popping element")
    list.pop()
    println(list)

    println("Merging lists")
    List list1 = [3, 4]
    list += list1
    print(list)
}

def immutableListExample() {
    try {
        List originalList = [1, 2, 3]
        List immutableList = originalList.asImmutable()
        immutableList.add(4)
    } catch(UnsupportedOperationException exception) {
        exception.printStackTrace()
    }
}

