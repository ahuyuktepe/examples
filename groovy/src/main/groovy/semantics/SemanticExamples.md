#### Labeled Statement
```
given:
    def x = [1, 2, 3, 4]
when:
    def y = x[0]
then:
    def isEqual = (y == 1)
```
> true

#### GPath Example
```
def users = [
    [firstName: 'Test 1', lastName: 'User 1'],
    [firstName: 'Test 2', lastName: 'User 2'],
    [firstName: 'Test 3', lastName: 'User 3'],
]
def firstNames = users.firstName
```
> [Test 1, Test 2, Test 3]

#### Groovy Truth Example
Non-empty Collections and arrays are true.
```
boolean falseIfEmptyList = []
```
> false

Non-empty Maps are evaluated to true.
```
boolean trueIfNonEmptyList = [1, 2, 3]
```
> true

Non-empty Maps are evaluated to true.
```
boolean falseIfEmptyMap = [:]
```
> false

```
boolean trueIfNonEmptyMap = [a: 1]
```
> true

Non-empty Strings, GStrings and CharSequences are coerced to true.
```
boolean falseIfEmptyString = ''
```
> false

```
boolean falseIfEmptyString = ''
```
boolean trueIfNonEmptyString = 'test'
> true

Non-zero numbers are true.

```
boolean trueIfNumberNonZero = 1
```
> true

```
boolean falseIfNumberisZero = 0
```
> false

#### The asBoolean Method Example
In order to customize whether groovy evaluates your object to true or false implement the asBoolean() method.

```
class Vehicle {
    String color
    boolean asBoolean() {
        return color == 'red'
    }
}
def vehicle = new Vehicle(color: 'blue')
boolean isVehicleRed = vehicle
```
> false

```
def vehicle = new Vehicle(color: 'red')
boolean isVehicleRed = vehicle
```
> true

#### TupleConstructor Annotation Example
```
@groovy.transform.TupleConstructor
class User {
    String firstName
    String lastName
    
    String toString() {
        return firstName + ' ' + lastName
    }
}
def user = new User('Test','User')
```
> Test User