
// Labeled statement example
given:
    def x = [1, 2, 3, 4]
when:
    def y = x[0]
then:
    def isEqual = (y == 1)

    println('''given:
    def x = [1, 2, 3, 4]
when:
    def y = x[0]
then:
    def isEqual = (y == 1)''')
    println("> $isEqual")

// Gpath Example
println("GPath Example")
def users = [
    [firstName: 'Test 1', lastName: 'User 1'],
    [firstName: 'Test 2', lastName: 'User 2'],
    [firstName: 'Test 3', lastName: 'User 3'],
]
def firstNames = users.firstName
println('''
def users = [
    [firstName: 'Test 1', lastName: 'User 1'],
    [firstName: 'Test 2', lastName: 'User 2'],
    [firstName: 'Test 3', lastName: 'User 3'],
]
def firstNames = users.firstName
''')
println(">: $firstNames")

// Groovy Truth Example
println("\nGroovy Truth Example")
println("\nNon-empty Collections and arrays are true.")
boolean falseIfEmptyList = []
println("boolean falseIfEmptyList = []")
println("> $falseIfEmptyList")

println("\nNon-empty Maps are evaluated to true.")
boolean trueIfNonEmptyList = [1, 2, 3]
println("boolean trueIfNonEmptyList = [1, 2, 3]")
println("> $trueIfNonEmptyList")

println("\nNon-empty Maps are evaluated to true.")
boolean falseIfEmptyMap = [:]
println("boolean falseIfEmptyMap = [:]")
println("> $falseIfEmptyMap")
boolean trueIfNonEmptyMap = [a: 1]
println("boolean trueIfNonEmptyMap = [a: 1]")
println("> $trueIfNonEmptyMap")

println("Non-empty Strings, GStrings and CharSequences are coerced to true.")
boolean falseIfEmptyString = ''
println("boolean falseIfEmptyString = ''")
println("> $falseIfEmptyString")

boolean trueIfNonEmptyString = 'test'
println("boolean trueIfNonEmptyString = 'test'")
println("> $trueIfNonEmptyString")

println("Non-zero numbers are true.")
boolean trueIfNumberNonZero = 1
println("boolean trueIfNumberNonZero = 1")
println("> $trueIfNumberNonZero")
println("boolean falseIfNumberisZero = 0")
boolean falseIfNumberisZero = 0
println("> $falseIfNumberisZero")

println("The asBoolean Method Example")
println("In order to customize whether groovy evaluates your object to true or false implement the asBoolean() method.")
class Vehicle {
    String color

    boolean asBoolean() {
        return color == 'red'
    }
}
def vehicle = new Vehicle(color: 'blue')
boolean isVehicleRed = vehicle
println('''
class Vehicle {
    String color

    boolean asBoolean() {
        return color == 'red\'
    }
}
def vehicle = new Vehicle(color: 'blue')
boolean isVehicleRed = vehicle
''')
println("> $isVehicleRed")

vehicle = new Vehicle(color: 'red')
isVehicleRed = vehicle
println('''
def vehicle = new Vehicle(color: 'red')
boolean isVehicleRed = vehicle
''')
println("> $isVehicleRed")

println("TupleContructor Annotation Example")
@groovy.transform.TupleConstructor
class User {
    String firstName
    String lastName

    String toString() {
        return firstName + ' ' + lastName
    }
}
def user = new User('Test','User')
println(/
@groovy.transform.TupleConstructor
class User {
    String firstName
    String lastName

    String toString() {
        return firstName + ' ' + lastName
    }
}
def user = new User('Test','User')
/)
println("> $user")