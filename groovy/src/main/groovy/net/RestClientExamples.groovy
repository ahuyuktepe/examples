package net

import groovyx.net.http.RESTClient
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.ssl.SSLSocketFactory
import java.security.KeyStore

restClientHttpExampleWithAuth()

def restClientHttpExample() {
    println("RestClient Http Request Example")
    RESTClient restClient = new RESTClient("http://www.example-host.com")
    // Send Request
    def response = restClient.get(path: '/index.html')
    println("Status: ${response.status}")
    println("Content: ${response.data}")
}

def restClientHttpsExample() {
    println("RestClient Https Request Example")
    RESTClient restClient = new RESTClient("https://www.example-host.com")
    // Build KeyStore
    File keyStorefile = new File("resources/truststore.jks");
    InputStream inputStream = new FileInputStream(keyStorefile);
    KeyStore keyStore = KeyStore.getInstance("JKS");
    keyStore.load(inputStream, "alp123".toCharArray());
    // Set Connection Scheme
    SSLSocketFactory sslSocketFactory = new SSLSocketFactory(keyStore)
    Scheme connectionScheme = new Scheme("https", 443, sslSocketFactory);
    restClient.client.connectionManager.schemeRegistry.register(connectionScheme)
    // Send Request
    def response = restClient.get(path: '/index.html')
    println("Status: ${response.status}")
    println("Content: ${response.data}")
}

def restClientHttpExampleWithAuth() {
    println("RestClient Http Request Example With Authentication")
    RESTClient restClient = new RESTClient("http://www.example-host.com")
    restClient.auth.basic("[USERNAME]", "[PASSWORD]")
    // Send Request
    def response = restClient.get(path: '/')
    println("Status: ${response.status}")
    println("Content: ${response.data}")
}
