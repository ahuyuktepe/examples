package net

import groovyx.net.http.HTTPBuilder
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.ssl.SSLSocketFactory
import java.security.KeyStore

httpBuilderHttpExampleWithAuth()

def httpBuilderHttpExample() {
    println("HttpBuilder Http Example")
    HTTPBuilder httpBuilder = new HTTPBuilder('http://www.example-host.com')
    httpBuilder.get(path: '/index.html') { response, reader ->
        println("Status: ${response.statusLine}")
        println("Content: $reader")
    }
}

def httpBuilderHttpsExample() {
    println("HttpBuilder Https Example")
    HTTPBuilder httpBuilder = new HTTPBuilder('https://www.example-host.com')
    // Build KeyStore
    File keyStorefile = new File("resources/truststore.jks")
    InputStream inputStream = new FileInputStream(keyStorefile);
    KeyStore keyStore = KeyStore.getInstance("JKS");
    keyStore.load(inputStream, "alp123".toCharArray());
    // Set Connection Scheme
    SSLSocketFactory sslSocketFactory = new SSLSocketFactory(keyStore)
    Scheme connectionScheme = new Scheme("https", 443, sslSocketFactory)
    httpBuilder.client.connectionManager.schemeRegistry.register(connectionScheme)
    // Send REquest
    httpBuilder.get(path: '/index.html') { response, reader ->
        println("Status: ${response.statusLine}")
        println("Content: $reader")
    }
}

def httpBuilderHttpExampleWithAuth() {
    println("HttpBuilder Http Example With Authentication")
    HTTPBuilder httpBuilder = new HTTPBuilder('http://www.example-host.com')
    httpBuilder.auth.basic("[USERNAME]", "[PASSWORD]")
    httpBuilder.get(path: '/json-response-example') { response, reader ->
        println("Status: ${response.statusLine}")
        println("Content: $reader")
    }
}