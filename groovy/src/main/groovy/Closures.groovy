println("=== Closure Examples ===")

def addition = { a, b -> a + b }
def result = addition(1, 2)
println('Result: ' + result)

def suffix = { "Welcome $it" }
print(suffix('Test User'))

