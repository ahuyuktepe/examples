
println("Number Type Suffix Examples")

def bigIntVal = 40G
println("\nBig Integer: G or g")
println("def bigIntVal = 40G")
println("Class: $bigIntVal.class | Value: $bigIntVal")

println("\nLong: L or l")
def longVal = 40L
println("def longVal = 40I")
println("Class: $longVal.class | Value: $longVal")

println("\nInteger: I or i")
def intVal = 40I
println("def intValue = 40I")
println("Class: $intVal.class | Value: $intVal")

def doubleVal = 40D
println("\nDouble: D or d")
println("def doubleVal = 40D")
println("Class: $doubleVal.class | Value: $doubleVal")

def floatVal = 40F
println("\nDouble: F or f")
println("def floatVal = 40F")
println("Class: $floatVal.class | Value: $floatVal")

println("\nString Defining Examples")
def name = 'Test User'

def str1 = '''
This is a test string.
Please ignore
'''
println(str1)
def str2 = """
This is a test string.
Please ignore.
\$name = $name
"""
println(str2)
def str3 = /
This is a test string.
Please ignore.
name = $name
/
println(str3)
def str4 = $/
This is a test string.
Please ignore.
Name: $name
/$