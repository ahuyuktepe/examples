package metaprogramming

import groovy.time.TimeCategory
import groovy.transform.ToString

class Class1 {
    /** This method is called if non existing method is called within this class */
    def methodMissing(String name, def args) {
        println("You called a method '$name' with args '$args' but it does not exist in class 'Class1'")
    }

    def propertyMissing(String name) {
        println("You tried to reach property '$name' within class 'Class1' but it does not exist.")
    }
}
Class1 cls1 = new Class1()
cls1.testMethod('test', 1, 2)
cls1.test

// Category Class Example

use(TimeCategory) {
    def today = new Date()
    def threeMonthsAgo = today - 3.months
    println("Today: $today | 3 Months Ago: $threeMonthsAgo")
}

// Custom Category Class Example
@Category(String)
class ProgramDurationCategory {
    int getMilliseconds() {
        String srcVal = this
        if(srcVal.endsWith('s')) {
            int val = srcVal.substring(0, srcVal.size() - 1).toInteger()
            return val * 1000
        }
        return 0
    }
}
use(ProgramDurationCategory) {
    int mseconds = '10s'.milliseconds
    println("10s == $mseconds")
}

// AST Transformation Examples
/*
Compile-time metaprogramming in Groovy allows code generation at compile-time. Those transformations are altering the
Abstract Syntax Tree (AST) of a program, which is why in Groovy we call it AST transformations. AST transformations
allow you to hook into the compilation process, modify the AST and continue the compilation process to generate
regular bytecode.
 */

// Code Generation Transformation

@ToString
class Person {
    String firstName
    String lastName
}
Person person = new Person(firstName: 'Test', lastName: 'User')
println("Person: $person")