#### Ternary Operator Example
The ternary operator is a shortcut expression that is equivalent to an if/else branch
assigning some value to a variable.
```
String userType = firstName == 'test' ? 'Test User' : 'Regular User'
```
> Test User

#### Elvis Assignment Operator Example
The Elvis operator is a shortening of the ternary operator. This operator user Groovy truth in condition statement
and returns value if it resolves to true otherwise it returns given value.
```
String firstName = 'test'
String userFirstName = firstName ?: 'Regular User'
```
> test

#### Safe Navigation Operator Example
The Safe Navigation operator is used to avoid a NullPointerException.
```
User user = null
String userFirstName = user?.firstName
```
> null

### Getter Method Access Operator Example
```
User userObj = new User(firstName: 'Test', lastName: 'User')
String firstName = userObj.firstName
```
> First Name: Test

#### Direct Field Access Operator Example
```
User userObj = new User(firstName: 'Test', lastName: 'User')
String firstName = userObj.@firstName
```
> Test

#### Spread Operator Example
```
List users = [
new User('Test 1', 'User 1'),
new User('Test 2', 'User 2'),
new User('Test 3', 'User 3'),
new User('Test 4', 'User 4')
]
List firstNamesByGetterMethod = users*.firstName
List firstNamesByDirectFieldAccess = users*.firstName
```
> firstNamesByGetterMethod         : [First Name: Test 1, First Name: Test 2, First Name: Test 3, First Name: Test 4]
>
> firstNamesByDirectFieldAccess    : [Test 1, Test 2, Test 3, Test 4]

#### Spread Operator While Calling Method
```
def concat = { String a, String b, String c ->
return "$a - $b - $c"
}
List cities = ['New York', 'London', 'Istanbul']
String dashSeparatedCities = concat(*cities)
```
> New York - London - Istanbul

#### Spread List Operator
```
List list1 = [4, 5, 6]
List list2 = [1, 2, 3, *list1, 7, 8]
```
> list2: [1, 2, 3, 4, 5, 6, 7, 8]

#### Range Operator
```
List range = 0..5
```
> [0, 1, 2, 3, 4, 5]
```
List range = 0..<5
```
> [0, 1, 2, 3, 4]

#### Spaceship Operator
The spaceship operator (<=>) delegates to the compareTo method:
```
int val1 = 10
int val2 = 15
int result = val1 <=> val2
```
> -1

#### Subscript Operator Example
```
List list1 = [0, 1, 2, 3, 4, 5]
def subList = list1[1..4]
```
> [1, 2, 3, 4]

```
List list1 = [0, 1, 2, 3, 4, 5]
list1[1..3] = [3, 3, 3]
```
> [0, 3, 3, 3, 4, 5]

#### Safe Index Operator Example
```
Groovy 3.0.0 introduces safe indexing operator
List names = [0, 1, 2, 3]
def val = list1?[5]
```
> null

#### Coercion Operator Example
```
Integer x = 100
String strVal = x as String
```
> 100