package operators

class User {
    String firstName
    String lastName

    User(String firstName, String lastName) {
        this.firstName = firstName
        this.lastName = lastName
    }

    String getFirstName() {
        return "First Name: $firstName"
    }
}


println("Ternary Operator Example\n" +
        "The ternary operator is a shortcut expression that is equivalent to an if/else branch\n" +
        "assigning some value to a variable.")
String firstName = 'test'
String userType = firstName == 'test' ? 'Test User' : 'Regular User'
println('Ternary operator example')
println("String userType = firstName == 'test' ? 'Test User' : 'Regular User'")
println("> $userType")

println("\nElvis Assignment Operator Example\n" +
        "The Elvis operator is a shortening of the ternary operator. This operator user Groovy truth in condition statement\n" +
        "and returns value if it resolves to true otherwise it returns given value.")
String userFirstName = firstName ?: 'Regular User'
println("String firstName = 'test'\n" +
        "String userFirstName = firstName ?: 'Regular User'")
println("> $userFirstName")

// Object Operators

println("\nSafe Navigation Operator Example\n" +
        "The Safe Navigation operator is used to avoid a NullPointerException. ")
def user = null
userFirstName = user?.firstName
println("User user = null\n" +
        "String userFirstName = user?.firstName")
println("> $userFirstName")

println("\nGetter Method Access Operator Example")
User userObj = new User('Test', 'User')
firstName = userObj.firstName
println("User userObj = new User(firstName: 'Test', lastName: 'User')\n" +
        "String firstName = userObj.firstName")
println("> $firstName")

println("\nDirect Field Access Operator Example")
firstName = userObj.@firstName
println("User userObj = new User(firstName: 'Test', lastName: 'User')\n" +
        "String firstName = userObj.@firstName")
println("> $firstName")

println("\nSpread Operator Example")
List users = [
   new User('Test 1', 'User 1'),
   new User('Test 2', 'User 2'),
   new User('Test 3', 'User 3'),
   new User('Test 4', 'User 4')
]
List firstNamesByGetterMethod = users*.firstName
List firstNamesByDirectFieldAccess = users*.@firstName
println("List users = [\n" +
        "   new User('Test 1', 'User 1'),\n" +
        "   new User('Test 2', 'User 2'),\n" +
        "   new User('Test 3', 'User 3'),\n" +
        "   new User('Test 4', 'User 4')\n" +
        "]\n" +
        "List firstNamesByGetterMethod = users*.firstName\n" +
        "List firstNamesByDirectFieldAccess = users*.firstName")
println("> firstNamesByGetterMethod         : $firstNamesByGetterMethod")
println("> firstNamesByDirectFieldAccess    : $firstNamesByDirectFieldAccess")

println("\nSpread Operator While Calling Method")
def concat = { String a, String b, String c ->
    return "$a - $b - $c"
}
List cities = ['New York', 'London', 'Istanbul']
String dashSeparatedCities = concat(*cities)
println("def concat = { String a, String b, String c ->\n" +
        "    return \"\$a - \$b - \$c\"\n" +
        "}\n" +
        "List cities = ['New York', 'London', 'Istanbul']\n" +
        "String dashSeparatedCities = concat(*cities)")
println("> $dashSeparatedCities")

println("\nSpread List Operator")
println("List list1 = [4, 5, 6]\n" +
        "List list2 = [1, 2, 3, *list1, 7, 8]")
List list1 = [4, 5, 6]
List list2 = [1, 2, 3, *list1, 7, 8]
println("> list2: $list2")

println("\nRange Operator")
List range = 0..5
println("List range = 0..5")
println("> $range")
range = 0..<5
println("List range = 0..<5")
println("> $range")

println("\nSpaceship Operator")
println("The spaceship operator (<=>) delegates to the compareTo method:")
int val1 = 10
int val2 = 15
int result = val1 <=> val2
println("int val1 = 10\n" +
        "int val2 = 15\n" +
        "int result = val1 <=> val2")
println("> $result")

println("Subscript Operator Example")
list1 = [0, 1, 2, 3, 4, 5]
def subList = list1[1..4]
println("List list1 = [0, 1, 2, 3, 4, 5]\n" +
        "def subList = list1[1..4]")
println("> $subList")

list1[1..3] = [3, 3, 3]
println("\nList list1 = [0, 1, 2, 3, 4, 5]\n" +
        "list1[1..3] = [3, 3, 3]")
println("> $list1")

println("Safe Index Operator Example")
println("Groovy 3.0.0 introduces safe indexing operator")
List names = [0, 1, 2, 3]
def val = names?[4]
println("List names = [0, 1, 2, 3]\n" +
        "def val = list1?[5]")
println("> $val")

println("\nCoercion Operator Example")
Integer x = 100
String strVal = x as String
println("Integer x = 100\n" +
        "String strVal = x as String")
println("> $strVal")
