### Push Operation Example
Dependencies
```
package resources;

public class CustomStack<T> {
     private CustomNode<T> top;
     private int size;

     public CustomStack() {
         top = null;
         size = 0;
     }

     public CustomStack(T [] values) {
         top = null;
         size = 0;
         for(T value: values) {
             push(value);
         }
     }

     public void push(T value) {
         CustomNode<T> newNode = new CustomNode<>(null, value);
         if(isEmpty()) {
            top = newNode;
         } else {
            newNode.setNext(top);
            top = newNode;
         }
         size++;
     }

     public T pop() {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> retNode = top;
         top = top.getNext();
         size--;
         return retNode.getData();
     }

     public T peek() {
         if(isEmpty()) {
             return null;
         }
         return top.getData();
     }

     public int size() {
         return size;
     }

     @Override
     public String toString() {
         String str = null;
         if(isEmpty()) {
             str = "";
         } else {
             str = "";
             CustomNode<T> currentNode = top;
             while(!currentNode.isLast()) {
                 str += currentNode.getData() + ", ";
                 currentNode = currentNode.getNext();
             }
             str += currentNode.getData().toString();
         }
         return "[" + str + "]";
     }

     // Private Methods

     private boolean isEmpty() {
         return top == null;
     }
}
```
```
package resources;

public class CustomNode<T> {
    private CustomNode<T> next;
    private T data;

    public CustomNode(CustomNode<T> next, T data) {
        this.next = next;
        this.data = data;
    }

    public CustomNode<T> getNext() {
        return next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setNext(CustomNode<T> next) {
        this.next = next;
    }

    public boolean isLast() {
        return next == null;
    }

    public boolean equalsToData(T data) {
        if(data == null && this.data == null) {
            return true;
        } else if(data != null && data.equals(this.data)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object trgNode) {
        if(trgNode == null) {
            return false;
        }
        T trgData = ((CustomNode<T>) trgNode).getData();
        return equalsToData(trgData);
    }
}
```
Example
```
CustomStack<String> stack = new CustomStack<>();
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Variables
String value = "test1";
// Add
stack.push(value);
output.add("\nAdded value '" + value + "' to the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Add
value = "test2";
stack.push(value);
output.add("\nAdded value '" + value + "' to the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Add
value = "test3";
stack.push(value);
output.add("\nAdded value '" + value + "' to the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
```
Output
```
Size: 0
Stack: []

Added value 'test1' to the top of the stack.
Size: 1
Stack: [test1]

Added value 'test2' to the top of the stack.
Size: 2
Stack: [test2, test1]

Added value 'test3' to the top of the stack.
Size: 3
Stack: [test3, test2, test1]
```
### Pop Operation Example
Example
```
String [] values = new String [] {"test1", "test2", "test3"};
CustomStack<String> stack = new CustomStack<>(values);
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Pop
String value = stack.pop();
output.add("\nPopped element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Pop
value = stack.pop();
output.add("\nPopped element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Pop
value = stack.pop();
output.add("\nPopped element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Pop
value = stack.pop();
output.add("\nPopped element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
```
Output
```
Size: 3
Stack: [test3, test2, test1]

Popped element 'test3' from the top of the stack.
Size: 2
Stack: [test2, test1]

Popped element 'test2' from the top of the stack.
Size: 1
Stack: [test1]

Popped element 'test1' from the top of the stack.
Size: 0
Stack: []

Popped element 'null' from the top of the stack.
Size: 0
Stack: []
```
### Peek Operation Example
Example
```
String [] values = new String [] {"test1", "test2", "test3"};
CustomStack<String> stack = new CustomStack<>(values);
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Peek
String value = stack.peek();
output.add("\nPeeked element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Peek
value = stack.peek();
output.add("\nPeeked element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
```
Output
```
Size: 3
Stack: [test3, test2, test1]

Peeked element 'test3' from the top of the stack.
Size: 3
Stack: [test3, test2, test1]

Peeked element 'test3' from the top of the stack.
Size: 3
Stack: [test3, test2, test1]
```
### Pop & Push Operations Example
Example
```
String [] values = new String [] {"test1", "test2", "test3"};
CustomStack<String> stack = new CustomStack<>(values);
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Pop
String value = stack.pop();
output.add("\nPopped element '"+value+"' from the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
// Add
value = "test4";
stack.push(value);
output.add("\nAdded value '" + value + "' to the top of the stack.");
output.add("Size: " + stack.size());
output.add("Stack: " + stack);
```
Output
```
Size: 3
Stack: [test3, test2, test1]

Popped element 'test3' from the top of the stack.
Size: 2
Stack: [test2, test1]

Added value 'test4' to the top of the stack.
Size: 3
Stack: [test4, test2, test1]
```
