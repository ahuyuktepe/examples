import resources.CustomSinglyLinkedList;
import java.util.List;

public class SinglyLinkedListExamples {

    public void exp_example1(List<String> output) {
        //summary: Add Operation Example
        //resources: CustomSinglyLinkedList,CustomNode
        CustomSinglyLinkedList<String> linkedList = new CustomSinglyLinkedList<>();
        output.add("Size: " + linkedList.size());
        output.add("List: " + linkedList);
        // Add Values
        String [] values = new String [] {"test1","test2","test3","test4","test5"};
        for(String value: values) {
            linkedList.add(value);
            output.add("\nAdded value '" + value + "'.");
            output.add("Size: " + linkedList.size());
            output.add("List: " + linkedList);
        }
    }//end

    public void exp_example2(List<String> output) {
        //summary: Remove Operation Example
        String [] values = new String [] {"test1","test2","test3","test4","test5"};
        CustomSinglyLinkedList<String> linkedList = new CustomSinglyLinkedList<>(values);
        output.add("Size: " + linkedList.size());
        output.add("List: " + linkedList);
        // Remove
        String value = "test2";
        linkedList.remove(value);
        output.add("\nRemoved value '" + value + "'.");
        output.add("Size: " + linkedList.size());
        output.add("List: " + linkedList);
        // Remove
        value = "test5";
        linkedList.remove(value);
        output.add("\nRemoved value '" + value + "'.");
        output.add("Size: " + linkedList.size());
        output.add("List: " + linkedList);
        // Remove
        value = "test1";
        linkedList.remove(value);
        output.add("\nRemoved value '" + value + "'.");
        output.add("Size: " + linkedList.size());
        output.add("List: " + linkedList);
    }//end

    public void exp_example3(List<String> output) {
        //summary: Get Operation Example
        String [] values = new String [] {"test1","test2","test3","test4","test5"};
        CustomSinglyLinkedList<String> linkedList = new CustomSinglyLinkedList<>(values);
        output.add("Size: " + linkedList.size());
        output.add("List: " + linkedList);
        // Get
        int index = 3;
        String value = linkedList.get(index);
        output.add("\nFetched element '" + value + "' at index '" + index + "'.");
        // Get
        index = 0;
        value = linkedList.get(index);
        output.add("\nFetched element '" + value + "' at index '" + index + "'.");
    }//end
}