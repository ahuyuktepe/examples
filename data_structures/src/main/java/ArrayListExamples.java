import resources.CustomArrayList;
import java.util.List;

public class ArrayListExamples {

    public void exp_example1(List<String> output) {
        //summary: Add Operation Example
        //resources: CustomArrayList
        CustomArrayList<String> arrayList = new CustomArrayList<>();
        String [] values = new String [] {"test1", "test2", "test3"};
        int index = 0;
        for(String value: values) {
            arrayList.add(value);
            output.add("\nAdded '" + value + "' into index '" + index + "'.");
            output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
            output.add("List: "  + arrayList);
            index++;
        }
    }//end

    public void exp_example2(List<String> output) {
        //summary: Add By Index Example
        CustomArrayList<String> arrayList = new CustomArrayList<>();
        String [] values = new String [] {"test1", "test2", "test3"};
        int index = 0;
        for(String value: values) {
            arrayList.add(value, index);
            output.add("\nAdded '" + value + "' into index '" + index + "'.");
            output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
            output.add("List: "  + arrayList);
            index++;
        }
    }//end

    public void exp_example3(List<String> output) {
        //summary: Add By Index In Between Example
        String [] values = new String [] {"test1", "test2", "test3"};
        CustomArrayList<String> arrayList = new CustomArrayList<>(values);
        // Initial
        output.add("List: "  + arrayList);
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        // Add Element
        arrayList.add("test2.1", 2);
        output.add("\nAdded 'test2.1' into index '2'.");
        output.add("List: "  + arrayList);
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        // Add Element
        arrayList.add("test1.1", 1);
        output.add("\nAdded 'test1.1' into index '1'.");
        output.add("List: "  + arrayList);
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        // Add Element
        arrayList.add("test1.2", 1);
        output.add("\nAdded 'test1.2' into index '1'.");
        output.add("List: "  + arrayList);
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        // Add Element
        arrayList.add("test0", 0);
        output.add("\nAdded 'test0' into index '0'.");
        output.add("List: "  + arrayList);
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
    }//end

    public void exp_example4(List<String> output) {
        //summary: Remove Operation
        // Initialize
        String [] values = new String [] {"test1", "test2", "test3", "test4", "test5"};
        CustomArrayList<String> arrayList = new CustomArrayList<>(values);
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        output.add("List: "  + arrayList);
        // Remove
        arrayList.remove("test2");
        output.add("\nRemoved 'test2'.");
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        output.add("List: "  + arrayList);
        // Remove
        arrayList.remove("test5");
        output.add("\nRemoved 'test5'.");
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        output.add("List: "  + arrayList);
        // Remove
        arrayList.remove("test1");
        output.add("\nRemoved 'test1'.");
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        output.add("List: "  + arrayList);
        // Remove Non-Existing Element
        arrayList.remove("test2");
        output.add("\nRemoved 'test2'.");
        output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
        output.add("List: "  + arrayList);
    }//end
}