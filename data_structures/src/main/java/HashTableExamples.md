### Insert Example
Dependencies
```
package resources;

@SuppressWarnings("unchecked")
public class CustomHashTable<K, V> {
    private static final int STORAGE_SIZE = 100;
    private CustomHashEntry<K, V>[] storage = null;

    public CustomHashTable() {
        this.storage = new CustomHashEntry[STORAGE_SIZE];
    }

    public void put(K key, V value) throws Exception {
        if(key == null) {
            throw new Exception("Key can not be null");
        }
        int hashCode = this.calculateHashCode(key);
        int index = this.compressHashCodeIntoArrayIndex(hashCode);
        boolean doesExist = this.storage[index] != null;
        if(doesExist) {
            index = this.getNextEmptyIndex(value, index);
        }
        CustomHashEntry<K, V> entry = new CustomHashEntry<>(key, value);
        this.storage[index] = entry;
    }

    /* This method fixes collision via linear probing algorithm */
    private int getNextEmptyIndex(V value, int trgIndex) throws Exception {
        if(trgIndex < 0 || trgIndex > STORAGE_SIZE) {
            throw new Exception("Invalid index");
        }
        boolean isSpotFull = true;
        int nextAvailableIndex = trgIndex;
        while(isSpotFull) {
            if(!isSpotFull) {
                return nextAvailableIndex;
            }
            nextAvailableIndex = ++nextAvailableIndex % STORAGE_SIZE;
            if(nextAvailableIndex == trgIndex) {
                throw new Exception("No empty spot left in array");
            }
            CustomHashEntry<K, V> elementInNextSpot = this.storage[nextAvailableIndex];
            isSpotFull =  elementInNextSpot != null && !elementInNextSpot.isDeleted();
        }
        return nextAvailableIndex;
    }

    public V get(K key) throws Exception {
        int index = this.findIndexByKey(key);
        if(index < 0) {
            return null;
        }
        CustomHashEntry<K, V> entry = this.storage[index];
        return entry == null ? null : entry.getValue();
    }

    private int findIndexByKey(K key) throws Exception {
        int hashCode = this.calculateHashCode(key);
        int initialIndex = this.compressHashCodeIntoArrayIndex(hashCode);
        int currentIndex = initialIndex;
        CustomHashEntry<K, V> entry = this.storage[currentIndex];
        while(true) {
            if(entry == null || key.equals(entry.getKey())) {
                break;
            }
            currentIndex = ++currentIndex % STORAGE_SIZE;
            entry = this.storage[currentIndex];
            if(currentIndex == initialIndex) {
                currentIndex = -1;
                break;
            }
        }
        return currentIndex;
    }

    public void delete(K key) throws Exception {
        int index = this.findIndexByKey(key);
        if(index >= 0) {
            CustomHashEntry<K, V> entry = this.storage[index];
            entry.setAsDeleted();
        }
    }

    private int calculateHashCode(K key) {
        // Note: Implemented a simple hash function to test collision
        // For more efficent HashTable this hash function can be improved.
        return key.toString().length() * 10;
    }

    private int compressHashCodeIntoArrayIndex(int hashCode) {
        return hashCode % STORAGE_SIZE;
    }
}
```
```
package resources;

public class CustomHashEntry<K, V> {
    private K key;
    private V value;
    // Marker to be used when element deleted.
    private boolean isDeleted = false;

    public CustomHashEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setAsDeleted() {
        this.key = null;
        this.value = null;
        this.isDeleted = true;
    }
}
```
Example
```
try {
    CustomHashTable<String, String> names = new CustomHashTable<>();
    names.put("Test", "User");
    output.add("Inserted ['Test':'User']");
    String lastName = names.get("Test");
    output.add("Fetched value for key 'Test': " + lastName);
} catch(Exception exception) {
    exception.printStackTrace();
}
```
Output
```
Inserted ['Test':'User']
Fetched value for key 'Test': User
```
### Delete Example
Example
```
try {
    CustomHashTable<String, String> names = new CustomHashTable<>();
    String key = "Test";
    // Insert
    names.put(key, "User");
    output.add("Inserted ['"+key+"':'User']");
    String value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
    // Delete
    names.delete(key);
    output.add("Deleted value by key '"+key+"'.");
    // Fetch Deleted Value
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
} catch(Exception exception) {
    exception.printStackTrace();
}
```
Output
```
Inserted ['Test':'User']
Fetched value for key 'Test': User
Deleted value by key 'Test'.
Fetched value for key 'Test': null
```
### Multiple Insert With Same Key Example
Example
```
try {
    CustomHashTable<String, String> names = new CustomHashTable<>();
    String key = "Tst1";
    String value = "User 1";
    names.put(key, value);
    output.add("Inserted ['"+key+"':'"+value+"']");
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
    // Insert
    key = "Tst2";
    value = "User 2";
    names.put(key, value);
    output.add("Inserted ['"+key+"':'"+value+"']");
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
} catch(Exception exception) {
    exception.printStackTrace();
}
```
Output
```
Inserted ['Tst1':'User 1']
Fetched value for key 'Tst1': User 1
Inserted ['Tst2':'User 2']
Fetched value for key 'Tst2': User 2
```
### Multiple Insert With Same Key Example (With Deleted)
Example
```
try {
    CustomHashTable<String, String> names = new CustomHashTable<>();
    String key = "Tst1";
    String value = "User 1";
    names.put(key, value);
    output.add("Inserted ['"+key+"':'"+value+"']");
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
    // Insert
    key = "Tst2";
    value = "User 2";
    names.put(key, value);
    output.add("Inserted ['"+key+"':'"+value+"']");
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
    // Insert
    key = "Tst3";
    value = "User 3";
    names.put(key, value);
    output.add("Inserted ['"+key+"':'"+value+"']");
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
    // Delete
    key = "Tst2";
    names.delete("Tst2");
    output.add("Deleted entry with key '"+key+"'.");
    // Fetch
    key = "Tst3";
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
    // Fetch
    key = "Tst2";
    value = names.get(key);
    output.add("Fetched value for key '"+key+"': " + value);
} catch(Exception exception) {
    exception.printStackTrace();
}
```
Output
```
Inserted ['Tst1':'User 1']
Fetched value for key 'Tst1': User 1
Inserted ['Tst2':'User 2']
Fetched value for key 'Tst2': User 2
Inserted ['Tst3':'User 3']
Fetched value for key 'Tst3': User 3
Deleted entry with key 'Tst2'.
Fetched value for key 'Tst3': User 3
Fetched value for key 'Tst2': null
```
