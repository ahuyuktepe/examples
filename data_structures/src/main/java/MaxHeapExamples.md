### Add Operation Example
Example
```
MaxHeap<Integer> heap = new MaxHeap<Integer>(8);
heap.add(18);
heap.add(66);
heap.add(20);
heap.add(28);
heap.add(90);
heap.add(10);
heap.add(5);
output.add(heap.toString());
```
Output
```
 90(1)
  28(2)
   8(3)
    5(4)
   20(3)
  66(2)
   18(3)
   10(3)

```
### Find Operation Example
Example
```
MaxHeap<Integer> heap = new MaxHeap<Integer>(8);
heap.add(18);
heap.add(66);
heap.add(20);
heap.add(28);
heap.add(90);
heap.add(10);
heap.add(5);
output.add(heap.toString());
Integer foundData = heap.find(90);
output.add("Found: " + foundData);
```
Output
```
 90(1)
  28(2)
   8(3)
    5(4)
   20(3)
  66(2)
   18(3)
   10(3)

Found: 90
```
### Remove Operation Example
Example
```
MaxHeap<Integer> heap = new MaxHeap<Integer>(8);
heap.add(18);
heap.add(66);
heap.add(20);
heap.add(28);
heap.add(90);
heap.add(10);
heap.add(5);
output.add(heap.toString());
Integer removedData = heap.remove();
output.add("Removed Data: " + removedData);
output.add(heap.toString());
removedData = heap.remove();
output.add("Removed Data: " + removedData);
output.add(heap.toString());
removedData = heap.remove();
output.add("Removed Data: " + removedData);
output.add(heap.toString());
```
Output
```
 90(1)
  28(2)
   8(3)
    5(4)
   20(3)
  66(2)
   18(3)
   10(3)

Removed Data: 90
 66(1)
  28(2)
   8(3)
   20(3)
  18(2)
   5(3)
   10(3)

Removed Data: 66
 28(1)
  20(2)
   8(3)
   10(3)
  18(2)
   5(3)

Removed Data: 28
 20(1)
  10(2)
   8(3)
   5(3)
  18(2)

```
