import resources.BinarySearchTree;
import resources.BinaryTree;
import resources.CompleteBinaryTree;
import resources.TreeNode;

import java.util.List;

public class BinarySearchTreeExamples {
    public void exp_example1(List<String> output) {
        //summary: Add Operation Example
        BinarySearchTree<Integer> tree = new BinarySearchTree<>(10);
        tree.add(7);
        tree.add(12);
        tree.add(11);
        tree.add(15);
        tree.add(5);
        tree.add(8);
        output.add(tree.toString());
    }//end

    public void exp_example2(List<String> output) {
        //summary: Find Operation Example
        BinarySearchTree<Integer> tree = new BinarySearchTree<>(10);
        tree.add(7);
        tree.add(12);
        tree.add(11);
        tree.add(15);
        tree.add(5);
        tree.add(8);
        output.add(tree.toString());
        TreeNode<Integer> foundNode = tree.find(5);
        output.add("Found Node: " + foundNode);
    }//end

    public void exp_example3(List<String> output) {
        //summary: Delete Operation Example
        BinarySearchTree<Integer> tree = new BinarySearchTree<>(10);
        tree.add(7);
        tree.add(12);
        tree.add(11);
        tree.add(15);
        tree.add(5);
        tree.add(8);
        output.add(tree.toString());
        Integer removedData = tree.delete(12);
        output.add("Removed Data: " + removedData);
        output.add(tree.toString());
    }//end
}