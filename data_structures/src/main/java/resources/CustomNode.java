package resources;

public class CustomNode<T> {
    private CustomNode<T> next;
    private T data;

    public CustomNode(CustomNode<T> next, T data) {
        this.next = next;
        this.data = data;
    }

    public CustomNode<T> getNext() {
        return next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setNext(CustomNode<T> next) {
        this.next = next;
    }

    public boolean isLast() {
        return next == null;
    }

    public boolean equalsToData(T data) {
        if(data == null && this.data == null) {
            return true;
        } else if(data != null && data.equals(this.data)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object trgNode) {
        if(trgNode == null) {
            return false;
        }
        T trgData = ((CustomNode<T>) trgNode).getData();
        return equalsToData(trgData);
    }

    @Override
    public String toString() {
       return "[Node: " + data + "]";
    }
}