package resources;

@SuppressWarnings("unchecked")
public class CustomArrayList<T> {
    // Capacity of array when created
    private static final int INITIAL_ARRAY_CAPACITY = 10;
    private T [] sourceArr;
    // Number of available spots in array.
    private int currentCapacity = 0;
    // Number of elements.
    private int currentSize;

    public CustomArrayList() {
        sourceArr = (T[]) new Object[INITIAL_ARRAY_CAPACITY];
        currentCapacity = INITIAL_ARRAY_CAPACITY;
    }

    public CustomArrayList(int initialCapacity) {
        sourceArr = (T[]) new Object[INITIAL_ARRAY_CAPACITY];
        currentCapacity = initialCapacity;
    }

    public CustomArrayList(T[] initialValues) {
        sourceArr = initialValues;
        currentCapacity = currentSize = sourceArr.length;
    }

    public void add(T element) {
        if(isFull()) {
            increaseSize();
        }
        sourceArr[currentSize] = element;
        currentSize++;
    }

    public void add(T element, int index) throws IndexOutOfBoundsException {
        if(index > currentSize || index < 0) {
            throw new IndexOutOfBoundsException();
        } else if(isFull()) {
            increaseSize();
        }
        shiftToRightFromIndex(index);
        sourceArr[index] = element;
        currentSize++;
    }

    public void remove(T element) {
        if(isEmpty()) {
            return;
        }
        for(int i=0; i<currentCapacity; i++) {
            T currentElement = sourceArr[i];
            if(currentElement.equals(element)) {
                shiftToLeft(i);
                currentCapacity--;
                break;
            }
        }
    }

    public T get(int index) {
        return sourceArr[index];
    }

    public int size() {
        return currentSize;
    }

    public int capacity() {
        return currentCapacity;
    }

    @Override
    public String toString() {
        String str = "";
        for(int i=0; i<currentCapacity; i++) {
            T element = sourceArr[i];
            str += ", " + element;
        }
        if(currentCapacity > 0) {
           str = str.substring(2);
        }
        return "[" + str + "]";
    }

    private boolean isEmpty() {
        return currentSize == 0;
    }

    private void shiftToLeft(int index) {
        if(isLastIndex(index)) {
            sourceArr[currentSize - 1] = null;
        }
        for(int i=index+1; i<currentSize; i++) {
            sourceArr[i-1] = sourceArr[i];
        }
        sourceArr[currentSize - 1] = null;
    }

    private boolean isLastIndex(int index) {
        return index == (currentSize - 1);
    }

    private void shiftToRightFromIndex(int index) {
        if(isEmpty()) {
            return;
        }
        for(int i=currentSize; i > index; i--) {
            sourceArr[i] = sourceArr[i-1];
        }
        sourceArr[index] = null;
    }

    private boolean isFull() {
        return currentSize == currentCapacity;
    }

    private void increaseSize() {
        int newCapacity = currentCapacity * 2;
        T[] tempArr = (T[]) new Object[newCapacity];
        for(int i=0; i<currentCapacity; i++) {
            tempArr[i] = sourceArr[i];
        }
        sourceArr = tempArr;
        currentCapacity = newCapacity;
    }
}