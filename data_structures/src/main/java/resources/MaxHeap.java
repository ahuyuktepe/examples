package resources;

import java.util.ArrayList;
import java.util.List;

public class MaxHeap<T extends Comparable> {
    private List<T> values;

    public MaxHeap(T data) {
        values = new ArrayList<T>(20);
        add(data);
    }

    public void add(T data) {
        if(data != null) {
            if(values.isEmpty()) {
                values.add(data);
            } else {
                values.add(data);
                orderHeapFromBottom(values.size() - 1);
            }
        }
    }

    private void orderHeapFromBottom(int childIndex) {
        if(childIndex == 0) {
            return;
        }
        boolean isLeftChild = (childIndex % 2) == 1;
        int parentIndex = (childIndex - 2) / 2;
        if(isLeftChild) {
            parentIndex = (childIndex - 1) / 2;
        }
        T childData = values.get(childIndex);
        T parentData = values.get(parentIndex);
        boolean isParentSmaller = parentData.compareTo(childData) < 0;
        if(isParentSmaller) {
            // Swap parent and child node location in array list
            values.set(childIndex, parentData);
            values.set(parentIndex, childData);
            orderHeapFromBottom(parentIndex);
        }
    }

    public T find(T data) {
        if(values.isEmpty()) {
            return null;
        }
        return findData(0, data);
    }

    private T findData(int index, T data) {
        T currentValue = values.get(index);
        int comparisonVal = currentValue.compareTo(data);
        if(comparisonVal == 0) {
            return currentValue;
        } else if(comparisonVal > 0) {
            return null;
        }
        int leftChildIndex = 2 * index + 1;
        int rightChildIndex = 2 * index + 2;

        T foundData = null;
        if(leftChildIndex < values.size()) {
            foundData = findData(leftChildIndex, data);
        }
        if(foundData != null) {
            return foundData;
        }
        if(rightChildIndex < values.size()) {
            return findData(rightChildIndex, data);
        }
        return null;
    }

    /**
     * In order to keep heap a complete tree delete operation always removes the root node
     * from the top.
     */
    public T remove() {
        if(values.isEmpty()) {
            return null;
        }
        T retData = values.get(0);
        T lastData = values.get(values.size() - 1);
        values.remove(values.size() - 1);
        values.set(0, lastData);
        orderHeapFromTop(0);
        return retData;
    }

    private void orderHeapFromTop(int parentIndex) {
        int leftChildIndex = 2 * parentIndex + 1;
        int rightChildIndex = 2 * parentIndex + 2;

        if(parentIndex >= values.size()) {
            return;
        } else if(leftChildIndex >= values.size()) {
            return;
        }
        // Find max child data and index
        T parentData = values.get(parentIndex);
        T leftData = values.get(leftChildIndex);
        T maxChildData = leftData;
        int maxChildIndex = leftChildIndex;

        if(rightChildIndex < values.size()) {
            T rightData = values.get(rightChildIndex);
            if(rightData.compareTo(leftData) > 0) {
                maxChildData = rightData;
                maxChildIndex = rightChildIndex;
            }
        }
        // Swap max child and parent
        if(parentData.compareTo(maxChildData) < 0) {
            values.set(parentIndex, maxChildData);
            values.set(maxChildIndex, parentData);
            orderHeapFromTop(maxChildIndex);
        }
    }

    @Override
    public String toString() {
        if(values.isEmpty()) {
            return "N/A";
        }
        StringBuilder builder = new StringBuilder();
        traverseNodesAndBuildString(0, builder, 1);
        return builder.toString();
    }

    private void traverseNodesAndBuildString(int index, StringBuilder builder, int indentCount) {
        if(index >= values.size()) {
            return;
        }

        String indentation = " ".repeat(indentCount);
        T value = values.get(index);
        builder.append(indentation + value.toString() + "(" + indentCount + ")" + '\n');

        int leftChildIndex = (2 * index) + 1;
        int rightChildIndex = (2 * index) + 2;

        ++indentCount;
        traverseNodesAndBuildString(leftChildIndex, builder, indentCount);
        traverseNodesAndBuildString(rightChildIndex, builder, indentCount);
    }
}