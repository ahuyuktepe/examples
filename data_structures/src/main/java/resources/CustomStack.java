package resources;

public class CustomStack<T> {
     private CustomNode<T> top;
     private int size;

     public CustomStack() {
         top = null;
         size = 0;
     }

     public CustomStack(T [] values) {
         top = null;
         size = 0;
         for(T value: values) {
             push(value);
         }
     }

     public void push(T value) {
         CustomNode<T> newNode = new CustomNode<>(null, value);
         if(isEmpty()) {
            top = newNode;
         } else {
            newNode.setNext(top);
            top = newNode;
         }
         size++;
     }

     public T pop() {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> retNode = top;
         top = top.getNext();
         size--;
         return retNode.getData();
     }

     public T peek() {
         if(isEmpty()) {
             return null;
         }
         return top.getData();
     }

     public CustomNode<T> get(T value) {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> currentNode = top;
         T currentData = null;
         while(currentNode != null) {
            currentData = currentNode.getData();
            if(currentData.equals(value)) {
                break;
            }
            currentNode = currentNode.getNext();
         }
         return currentNode;
     }

     public int size() {
         return size;
     }

     @Override
     public String toString() {
         String str = null;
         if(isEmpty()) {
             str = "";
         } else {
             str = "";
             CustomNode<T> currentNode = top;
             while(!currentNode.isLast()) {
                 str += currentNode.getData() + ", ";
                 currentNode = currentNode.getNext();
             }
             str += currentNode.getData().toString();
         }
         return "[" + str + "]";
     }

     // Private Methods

     private boolean isEmpty() {
         return top == null;
     }
}
