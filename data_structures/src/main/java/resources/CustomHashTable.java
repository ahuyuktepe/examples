package resources;

@SuppressWarnings("unchecked")
public class CustomHashTable<K, V> {
    private static final int STORAGE_SIZE = 100;
    private CustomHashEntry<K, V>[] storage = null;

    public CustomHashTable() {
        this.storage = new CustomHashEntry[STORAGE_SIZE];
    }

    public void put(K key, V value) throws Exception {
        if(key == null) {
            throw new Exception("Key can not be null");
        }
        int hashCode = this.calculateHashCode(key);
        int index = this.compressHashCodeIntoArrayIndex(hashCode);
        boolean doesExist = this.storage[index] != null;
        if(doesExist) {
            index = this.getNextEmptyIndex(value, index);
        }
        CustomHashEntry<K, V> entry = new CustomHashEntry<>(key, value);
        this.storage[index] = entry;
    }

    /* This method fixes collision via linear probing algorithm */
    private int getNextEmptyIndex(V value, int trgIndex) throws Exception {
        if(trgIndex < 0 || trgIndex > STORAGE_SIZE) {
            throw new Exception("Invalid index");
        }
        boolean isSpotFull = true;
        int nextAvailableIndex = trgIndex;
        while(isSpotFull) {
            if(!isSpotFull) {
                return nextAvailableIndex;
            }
            nextAvailableIndex = ++nextAvailableIndex % STORAGE_SIZE;
            if(nextAvailableIndex == trgIndex) {
                throw new Exception("No empty spot left in array");
            }
            CustomHashEntry<K, V> elementInNextSpot = this.storage[nextAvailableIndex];
            isSpotFull =  elementInNextSpot != null && !elementInNextSpot.isDeleted();
        }
        return nextAvailableIndex;
    }

    public V get(K key) throws Exception {
        int index = this.findIndexByKey(key);
        if(index < 0) {
            return null;
        }
        CustomHashEntry<K, V> entry = this.storage[index];
        return entry == null ? null : entry.getValue();
    }

    private int findIndexByKey(K key) throws Exception {
        int hashCode = this.calculateHashCode(key);
        int initialIndex = this.compressHashCodeIntoArrayIndex(hashCode);
        int currentIndex = initialIndex;
        CustomHashEntry<K, V> entry = this.storage[currentIndex];
        while(true) {
            if(entry == null || key.equals(entry.getKey())) {
                break;
            }
            currentIndex = ++currentIndex % STORAGE_SIZE;
            entry = this.storage[currentIndex];
            if(currentIndex == initialIndex) {
                currentIndex = -1;
                break;
            }
        }
        return currentIndex;
    }

    public void delete(K key) throws Exception {
        int index = this.findIndexByKey(key);
        if(index >= 0) {
            CustomHashEntry<K, V> entry = this.storage[index];
            entry.setAsDeleted();
        }
    }

    private int calculateHashCode(K key) {
        // Note: Implemented a simple hash function to test collision
        // For more efficent HashTable this hash function can be improved.
        return key.toString().length() * 10;
    }

    private int compressHashCodeIntoArrayIndex(int hashCode) {
        return hashCode % STORAGE_SIZE;
    }
}
