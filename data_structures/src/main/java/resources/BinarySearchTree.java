package resources;

/**
 * Complexity of BinarySearchTree class
 * Space Complexity: O(n)
 */
public class BinarySearchTree<T extends Comparable> extends BinaryTree<T> {
    private TreeNode<T> deletedNode = null;
    public BinarySearchTree(T data) {
        root = new TreeNode<T>(data);
    }

    @Override
    public void add(T data) {
        if(data != null) {
            TreeNode<T> node = new TreeNode<T>(data);
            if (isEmpty()) {
               root = node;
            } else {
               addNode(root, data);
            }
        }
    }

    public void addNode(TreeNode<T> node, T data) {
        int comparisonValue = node.getData().compareTo(data);
        if(comparisonValue > 0) {
            if(!node.hasLeftChild()) {
                node.setLeft(new TreeNode<>(data));
            } else {
                addNode(node.getLeftChild(), data);
            }
        } else if(comparisonValue < 0) {
            if(!node.hasRightChild()) {
                node.setRight(new TreeNode<>(data));
            } else {
                addNode(node.getRightChild(), data);
            }
        }
    }

    @Override
    public TreeNode<T> find(T data) {
        if(isEmpty()) {
            return null;
        }
        return findNode(root, data);
    }

    private TreeNode<T> findNode(TreeNode<T> node, T data) {
        int comparisonValue = node.getData().compareTo(data);
        if(comparisonValue > 0) {
            return findNode(node.getLeftChild(), data);
        } else if(comparisonValue < 0) {
            return findNode(node.getRightChild(), data);
        } else {
            return node;
        }
    }

    public T delete(T data) {
        if(isEmpty()) {
            return null;
        }
        deleteNode(root, data);
        if(deletedNode == null) {
            return null;
        }
        return deletedNode.getData();
    }

    private TreeNode<T> deleteNode(TreeNode<T> currentNode, T data) {
        if(currentNode == null) {
            deletedNode = null;
            return null;
        }

        int comparisonValue = currentNode.getData().compareTo(data);
        if(comparisonValue < 0) {
            TreeNode<T> returnedNode = deleteNode(currentNode.getRightChild(), data);
            currentNode.setRight(returnedNode);
            return currentNode;
        } else if(comparisonValue > 0) {
            TreeNode<T> returnedNode = deleteNode(currentNode.getLeftChild(), data);
            currentNode.setLeft(returnedNode);
            return currentNode;
        }
        deletedNode = currentNode;
        // Current node is to be deleted
        if(!currentNode.hasLeftChild()) {
            return currentNode.getRightChild();
        } else if(!currentNode.hasRightChild()) {
            return currentNode.getLeftChild();
        } else {
            if(!currentNode.getLeftChild().hasRightChild()) {
                // Left child of current node does not have right child.
                // Replace left child data of current node with current node's data.
                T leftChildData = currentNode.getLeftChild().getData();
                currentNode.setData(leftChildData);
                // Replace current node's left child with left child's left child
                currentNode.setLeft(currentNode.getLeftChild().getLeftChild());
            } else {
                // Find largest node in the left side of the current node then replace
                // the found node's data with current node's data.
                TreeNode<T> largestNodeInLeftTree = findLargestNode(currentNode);
                currentNode.setData(largestNodeInLeftTree.getData());
            }
            return currentNode;
        }
    }

    private TreeNode<T> findLargestNode(TreeNode<T> currentNode) {
        if(!currentNode.getRightChild().hasRightChild()) {
            currentNode.setRight(currentNode.getRightChild().getLeftChild());
            return currentNode.getRightChild();
        }
        return findLargestNode(currentNode.getRightChild());
    }
}
