package resources;

public abstract class BinaryTree<T> {
    protected TreeNode<T> root;

    public TreeNode<T> getRoot() {
        return root;
    }

    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        preOrderTraverse(root, 1, buffer);
        return buffer.toString();
    }

    private void preOrderTraverse(TreeNode<T> node, int numberOfWhiteSpaces, StringBuffer buffer) {
        for(int i=1; i<=numberOfWhiteSpaces;i++) {
            buffer.append(" ");
        }
        if(node == null) {
            buffer.append("x\n");
        } else {
            buffer.append(node.getData() + "(" + numberOfWhiteSpaces +")");
            buffer.append("\n");
            preOrderTraverse(node.getLeftChild(), numberOfWhiteSpaces+1, buffer);
            preOrderTraverse(node.getRightChild(), numberOfWhiteSpaces+1, buffer);
        }
    }

    public abstract void add(T data);

    public abstract TreeNode<T> find(T data);

}