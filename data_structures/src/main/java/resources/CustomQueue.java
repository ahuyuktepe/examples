package resources;

public class CustomQueue<T> {
     private CustomNode<T> top;
     private CustomNode<T> tail;
     private int size;

     public CustomQueue() {
         top = tail = null;
         size = 0;
     }

     public CustomQueue(T [] values) {
         top = tail = null;
         size = 0;
         for(T value: values) {
             push(value);
         }
     }

     public void push(T value) {
         CustomNode<T> newNode = new CustomNode<>(null, value);
         if(isEmpty()) {
            top = tail = newNode;
         } else {
            tail.setNext(newNode);
            tail = newNode;
         }
         size++;
     }

     public T pop() {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> retNode = top;
         top = top.getNext();
         size--;
         return retNode.getData();
     }

     public CustomNode<T> get(T value) {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> currentNode = top;
         T currentData = null;
         while(currentNode != null) {
            currentData = currentNode.getData();
            if(currentData.equals(value)) {
                break;
            }
            currentNode = currentNode.getNext();
         }
         return currentNode;
     }

     public int size() {
         return size;
     }

     @Override
     public String toString() {
         String str = null;
         if(isEmpty()) {
             str = "";
         } else {
             str = "";
             CustomNode<T> currentNode = top;
             while(!currentNode.isLast()) {
                 str += currentNode.getData() + ", ";
                 currentNode = currentNode.getNext();
             }
             str += currentNode.getData().toString();
         }
         return "[" + str + "]";
     }

     // Private Methods

     private boolean isEmpty() {
         return top == null;
     }
}
