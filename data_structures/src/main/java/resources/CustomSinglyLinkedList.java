package resources;

import java.util.Optional;

public class CustomSinglyLinkedList<T> {
    private CustomNode<T> head;
    private CustomNode<T> tail;
    private int size;

    public CustomSinglyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    }

    public CustomSinglyLinkedList(T [] values) {
        head = null;
        tail = null;
        size = 0;
        for(T value: values) {
            add(value);
        }
    }

    public void add(T value) {
        CustomNode<T> newNode = new CustomNode<>(null, value);
        if(isEmpty()) {
            head = tail = newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;
        }
        size++;
    }

    public void remove(T value) {
        if(isEmpty()) {
            return;
        }
        CustomNode<T> currentNode = head;
        CustomNode<T> prevNode = head;
        while(currentNode != null) {
            if(currentNode.equalsToData(value)) {
                if(currentNode.equals(head)) {
                    head = currentNode.getNext();
                } else {
                    prevNode.setNext(currentNode.getNext());
                }
                size--;
                break;
            }
            prevNode = currentNode;
            currentNode = currentNode.getNext();
        }
    }

    public T get(int index) throws IndexOutOfBoundsException {
        Optional<CustomNode<T>> optionalValue = getNodeByIndex(index);
        return optionalValue.isPresent() ? optionalValue.get().getData() : null;
    }

    public void set(T data, int index) throws IndexOutOfBoundsException {
        Optional<CustomNode<T>> optionalValue = getNodeByIndex(index);
        if(optionalValue.isPresent()) {
            CustomNode<T> trgNode = optionalValue.get();
            trgNode.setData(data);
        }
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        String str = null;
        if(isEmpty()) {
            str = "";
        } else {
            str = "";
            CustomNode<T> currentNode = head;
            while(!currentNode.isLast()) {
                str += currentNode.getData() + ", ";
                currentNode = currentNode.getNext();
            }
            str += currentNode.getData().toString();
        }
        return "[" + str + "]";
    }

    // Private Methods

    private boolean isEmpty() {
        return head == null;
    }

    private Optional<CustomNode<T>> getNodeByIndex(int index) {
        if(isEmpty()) {
            return Optional.ofNullable(null);
        } else if(index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        int currentIndex = 0;
        CustomNode<T> currentNode = head;
        while(currentIndex != index) {
            currentNode = currentNode.getNext();
            currentIndex++;
        }
        return Optional.ofNullable(currentNode);
    }
}
