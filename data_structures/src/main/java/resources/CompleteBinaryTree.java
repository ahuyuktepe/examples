package resources;

import java.util.ArrayList;
import java.util.List;

public class CompleteBinaryTree<T> extends BinaryTree<T> {
    protected List<TreeNode<T>> nodes;

    public CompleteBinaryTree(T data) {
        nodes = new ArrayList<TreeNode<T>>(20);
        root = new TreeNode<T>(data);
        nodes.add(0, root);
    }

    /**
     * Time Complexity  : O(1)
     * Space Complexity : O(n)
     */
    @Override
    public void add(T data) {
        TreeNode<T> node = new TreeNode<T>(data);
        nodes.add(node);
        if(!isEmpty()) {
            if(isNextLeftChild()) {
                int parentIndex = (nodes.size() - 1) / 2;
                nodes.get(parentIndex).setLeft(node);
            } else {
                int parentIndex = (nodes.size() - 2) / 2;
                nodes.get(parentIndex).setRight(node);
            }
        }
    }

    public TreeNode<T> delete() {
        if(isEmpty()) {
            return null;
        }
        if(isNodeToBeDeletedLeftChild()) {
            int parentIndex = (nodes.size() - 1) / 2;
            nodes.get(parentIndex).setLeft(null);
        } else {
            int parentIndex = (nodes.size() - 2) / 2;
            nodes.get(parentIndex).setRight(null);
        }
        TreeNode<T> deletedNode = nodes.get(nodes.size() - 1);
        nodes.add((nodes.size() - 1), null);
        return deletedNode;
    }

    /**
     * Using Tree Node Class
     * Time Complexity  : O(n)
     * Space Complexity : O(n)
     *
     * Using Array
     * Time Complexity  : O(n)
     * Space Complexity : O(n)
     */
    public TreeNode<T> find(T data) {
        if(isEmpty()) {
            return null;
        }
        return findNodeUsingTreeNodes(root, data);
    }

    /**
     * Implementing using linked tree nodes.
     */
    public TreeNode<T> findNodeUsingTreeNodes(TreeNode<T> node, T data) {
        if(node == null) {
            return null;
        } else if(node.getData().equals(data)) {
            return node;
        }
        TreeNode<T> returnNode = findNodeUsingTreeNodes(node.getLeftChild(), data);
        if(returnNode != null) {
            return returnNode;
        }
        return findNodeUsingTreeNodes(node.getRightChild(), data);
    }

    /**
     * Implementing using implicit array.
     */
    public TreeNode<T> findNodeUsingArray(T data) {
        for(TreeNode<T> node: nodes) {
            if(node.getData().equals(data)) {
                return node;
            }
        }
        return null;
    }

    protected boolean isNextLeftChild() {
        return nodes.size() % 2 == 1;
    }

    protected boolean isNodeToBeDeletedLeftChild() {
        return (nodes.size() - 1) % 2 == 1;
    }

}