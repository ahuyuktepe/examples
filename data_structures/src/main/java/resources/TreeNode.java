package resources;

public class TreeNode<T> {
    private TreeNode<T> left;
    private TreeNode<T> right;
    private T data;

    public TreeNode(T data) {
        left = null;
        right = null;
        this.data = data;
    }

    public void setLeft(TreeNode<T> node) {
        left = node;
    }

    public void setRight(TreeNode<T> node) {
        right = node;
    }

    public void setData(T data) {
        this.data = data;
    }

    public TreeNode<T> getLeftChild() {
        return left;
    }

    public TreeNode<T> getRightChild() {
        return right;
    }

    public T getData() {
        return data;
    }

    public boolean isLeaf() {
        return left == null && right == null;
    }

    public boolean hasLeftChild() {
        return left != null;
    }

    public boolean hasRightChild() {
        return right != null;
    }

    public boolean hasBotthChildren() {
        return hasRightChild() && hasRightChild();
    }

    @Override
    public String toString() {
        return "TreeNode[" + data.toString() + "]";
    }
}