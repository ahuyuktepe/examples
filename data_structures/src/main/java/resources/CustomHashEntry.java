package resources;

public class CustomHashEntry<K, V> {
    private K key;
    private V value;
    // Marker to be used when element deleted.
    private boolean isDeleted = false;

    public CustomHashEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setAsDeleted() {
        this.key = null;
        this.value = null;
        this.isDeleted = true;
    }
}
