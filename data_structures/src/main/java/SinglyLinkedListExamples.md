### Add Operation Example
Dependencies
```
package resources;

import java.util.Optional;

public class CustomSinglyLinkedList<T> {
    private CustomNode<T> head;
    private CustomNode<T> tail;
    private int size;

    public CustomSinglyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    }

    public CustomSinglyLinkedList(T [] values) {
        head = null;
        tail = null;
        size = 0;
        for(T value: values) {
            add(value);
        }
    }

    public void add(T value) {
        CustomNode<T> newNode = new CustomNode<>(null, value);
        if(isEmpty()) {
            head = tail = newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;
        }
        size++;
    }

    public void remove(T value) {
        if(isEmpty()) {
            return;
        }
        CustomNode<T> currentNode = head;
        CustomNode<T> prevNode = head;
        while(currentNode != null) {
            if(currentNode.equalsToData(value)) {
                if(currentNode.equals(head)) {
                    head = currentNode.getNext();
                } else {
                    prevNode.setNext(currentNode.getNext());
                }
                size--;
                break;
            }
            prevNode = currentNode;
            currentNode = currentNode.getNext();
        }
    }

    public T get(int index) throws IndexOutOfBoundsException {
        Optional<CustomNode<T>> optionalValue = getNodeByIndex(index);
        return optionalValue.isPresent() ? optionalValue.get().getData() : null;
    }

    public void set(T data, int index) throws IndexOutOfBoundsException {
        Optional<CustomNode<T>> optionalValue = getNodeByIndex(index);
        if(optionalValue.isPresent()) {
            CustomNode<T> trgNode = optionalValue.get();
            trgNode.setData(data);
        }
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        String str = null;
        if(isEmpty()) {
            str = "";
        } else {
            str = "";
            CustomNode<T> currentNode = head;
            while(!currentNode.isLast()) {
                str += currentNode.getData() + ", ";
                currentNode = currentNode.getNext();
            }
            str += currentNode.getData().toString();
        }
        return "[" + str + "]";
    }

    // Private Methods

    private boolean isEmpty() {
        return head == null;
    }

    private Optional<CustomNode<T>> getNodeByIndex(int index) {
        if(isEmpty()) {
            return Optional.ofNullable(null);
        } else if(index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        int currentIndex = 0;
        CustomNode<T> currentNode = head;
        while(currentIndex != index) {
            currentNode = currentNode.getNext();
            currentIndex++;
        }
        return Optional.ofNullable(currentNode);
    }
}
```
```
package resources;

public class CustomNode<T> {
    private CustomNode<T> next;
    private T data;

    public CustomNode(CustomNode<T> next, T data) {
        this.next = next;
        this.data = data;
    }

    public CustomNode<T> getNext() {
        return next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setNext(CustomNode<T> next) {
        this.next = next;
    }

    public boolean isLast() {
        return next == null;
    }

    public boolean equalsToData(T data) {
        if(data == null && this.data == null) {
            return true;
        } else if(data != null && data.equals(this.data)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object trgNode) {
        if(trgNode == null) {
            return false;
        }
        T trgData = ((CustomNode<T>) trgNode).getData();
        return equalsToData(trgData);
    }
}
```
Example
```
CustomSinglyLinkedList<String> linkedList = new CustomSinglyLinkedList<>();
output.add("Size: " + linkedList.size());
output.add("List: " + linkedList);
// Add Values
String [] values = new String [] {"test1","test2","test3","test4","test5"};
for(String value: values) {
    linkedList.add(value);
    output.add("\nAdded value '" + value + "'.");
    output.add("Size: " + linkedList.size());
    output.add("List: " + linkedList);
}
```
Output
```
Size: 0
List: []

Added value 'test1'.
Size: 1
List: [test1]

Added value 'test2'.
Size: 2
List: [test1, test2]

Added value 'test3'.
Size: 3
List: [test1, test2, test3]

Added value 'test4'.
Size: 4
List: [test1, test2, test3, test4]

Added value 'test5'.
Size: 5
List: [test1, test2, test3, test4, test5]
```
### Remove Operation Example
Example
```
String [] values = new String [] {"test1","test2","test3","test4","test5"};
CustomSinglyLinkedList<String> linkedList = new CustomSinglyLinkedList<>(values);
output.add("Size: " + linkedList.size());
output.add("List: " + linkedList);
// Remove
String value = "test2";
linkedList.remove(value);
output.add("\nRemoved value '" + value + "'.");
output.add("Size: " + linkedList.size());
output.add("List: " + linkedList);
// Remove
value = "test5";
linkedList.remove(value);
output.add("\nRemoved value '" + value + "'.");
output.add("Size: " + linkedList.size());
output.add("List: " + linkedList);
// Remove
value = "test1";
linkedList.remove(value);
output.add("\nRemoved value '" + value + "'.");
output.add("Size: " + linkedList.size());
output.add("List: " + linkedList);
```
Output
```
Size: 5
List: [test1, test2, test3, test4, test5]

Removed value 'test2'.
Size: 4
List: [test1, test3, test4, test5]

Removed value 'test5'.
Size: 3
List: [test1, test3, test4]

Removed value 'test1'.
Size: 2
List: [test3, test4]
```
### Get Operation Example
Example
```
String [] values = new String [] {"test1","test2","test3","test4","test5"};
CustomSinglyLinkedList<String> linkedList = new CustomSinglyLinkedList<>(values);
output.add("Size: " + linkedList.size());
output.add("List: " + linkedList);
// Get
int index = 3;
String value = linkedList.get(index);
output.add("\nFetched element '" + value + "' at index '" + index + "'.");
// Get
index = 0;
value = linkedList.get(index);
output.add("\nFetched element '" + value + "' at index '" + index + "'.");
```
Output
```
Size: 5
List: [test1, test2, test3, test4, test5]

Fetched element 'test4' at index '3'.

Fetched element 'test1' at index '0'.
```
