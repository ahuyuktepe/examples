import classes.Example;
import classes.ExampleRunner;

import java.util.*;

public class Main {
    private static final Map<String, String> configuration = new HashMap<String, String>();
    private static final ExampleRunner runner = new ExampleRunner();

    public static void main(String[] args) {
        try {
            Main main = new Main();
            main.setConfiguration();
            Optional<List<Example>> examples = main.parseExamples();
            if(examples.isPresent()) {
                List<Example> allExamples = examples.get();
                List<Example> examplesToRun = main.getExamplesToRun(allExamples);
                main.runExamplesAndSaveOutput(examplesToRun);
                main.handleOutput(examplesToRun);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void setConfiguration() {
        configuration.put("classPath", "BinaryTreeExamples");
        configuration.put("isDisplayOnly", "false");
        configuration.put("executeLastExample", "false");
    }

    private Optional<List<Example>> parseExamples() {
        ExampleRunner runner = new ExampleRunner();
        String classPath = configuration.get("classPath").toString();
        Optional<List<Example>> optionalExamples = runner.fetchExamples(classPath);
        return optionalExamples;
    }

    private List<Example> getExamplesToRun(List<Example> allExamples) throws Exception {
        List<Example> examplesToRun = null;
        if(isExecuteLast()) {
            int indexOfLastExp = allExamples.size() - 1;
            Example lastExample = allExamples.get(indexOfLastExp);
            examplesToRun = Arrays.asList(lastExample);
        } else {
            examplesToRun = allExamples;
        }
        return examplesToRun;
    }

    private void runExamplesAndSaveOutput(List<Example> examples) throws Exception {
        String classPath = configuration.get("classPath");
        runner.runExamplesAndSaveOutput(examples, classPath);
    }

    private void handleOutput(List<Example> examples) {
        String classPath = configuration.get("classPath");
        for (Example example : examples) {
            if(isDisplayOnly()) {
                StringBuilder builder = new StringBuilder();;
                builder.append("\n" + example.getSummary() + "\n");
                builder.append("=============================================================");
                builder.append(example.getOutput());
                System.out.println(builder.toString());
            } else {
                runner.saveAsMarkup(examples, classPath);
            }
        }
    }

    private boolean isDisplayOnly() {
        String value = configuration.get("isDisplayOnly");
        return Boolean.parseBoolean(value);
    }

    private boolean isExecuteLast() {
        String value = configuration.get("executeLastExample");
        return Boolean.parseBoolean(value);
    }
}