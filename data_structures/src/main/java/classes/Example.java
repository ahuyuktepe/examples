package classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Example {
    String methodName;
    List<String> code;
    List<String> output;
    String summary;
    String description;
    String resources;

    public String getDescription() {
        return description;
    }

    public String getResources() {
        return resources;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getCallableMethodName() {
        return "exp_" + methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getCode() {
        return "\n" + String.join("\n", code);
    }

    public void appendCodeLine(String code) {
        if(this.code == null) {
            this.code = new ArrayList<String>();
        }
        this.code.add(code);
    }

    public void setOutput(List<String> output) {
        this.output = new ArrayList<String>();
        this.output.addAll(output);
    }

    public String getOutput() {
        return "\n" + String.join("\n", output);
    }

    public String getMarkup() {
        StringBuilder builder = new StringBuilder();
        builder.append("### " + summary + "\n");
        // Print Description
        if(description != null) {
            builder.append(description + "\n\n");
        }
        // Print Resources
        if(resources != null) {
            builder.append("Dependencies\n");
            String [] arr = resources.split(",");
            for(String path: arr) {
                String content = readResourceContent(path);
                builder.append("```\n");
                builder.append(content);
                builder.append("```\n");
            }
        }
        // Print Code
        builder.append("Example\n");
        builder.append("```\n");
        for(String line: code) {
            builder.append(line + "\n");
        }
        builder.append("```\n");
        // Print Output
        builder.append("Output\n");
        builder.append("```\n");
        for(String line: output) {
            builder.append(line + "\n");
        }
        builder.append("```\n");
        return builder.toString();
    }

    public String readResourceContent(String clsPath) {
        String response = "";
        try {
            String fileName = clsPath.replace(".", File.separator) + ".java";
            List<String> pathAsList = Arrays.asList(System.getProperty("user.dir"), "src", "main", "java", "resources", fileName);
            String filePath = String.join(File.separator, pathAsList);
            // Read Class File Content
            File file = new File(filePath);
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader streamReader = new InputStreamReader(fileInputStream);
            BufferedReader reader = new BufferedReader(streamReader);
            String line = null;
            while((line = reader.readLine()) != null) {
                line.trim();
                response += line + "\n";
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return response;
    }
}