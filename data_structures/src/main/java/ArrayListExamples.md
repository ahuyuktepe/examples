### Add Operation Example
Dependencies
```
package resources;

@SuppressWarnings("unchecked")
public class CustomArrayList<T> {
    // Capacity of array when created
    private static final int INITIAL_ARRAY_CAPACITY = 10;
    private T [] sourceArr;
    // Number of available spots in array.
    private int currentCapacity = 0;
    // Number of elements.
    private int currentSize;

    public CustomArrayList() {
        sourceArr = (T[]) new Object[INITIAL_ARRAY_CAPACITY];
        currentCapacity = INITIAL_ARRAY_CAPACITY;
    }

    public CustomArrayList(int initialCapacity) {
        sourceArr = (T[]) new Object[INITIAL_ARRAY_CAPACITY];
        currentCapacity = initialCapacity;
    }

    public CustomArrayList(T[] initialValues) {
        sourceArr = initialValues;
        currentCapacity = currentSize = sourceArr.length;
    }

    public void add(T element) {
        if(isFull()) {
            increaseSize();
        }
        sourceArr[currentSize] = element;
        currentSize++;
    }

    public void add(T element, int index) throws IndexOutOfBoundsException {
        if(index > currentSize || index < 0) {
            throw new IndexOutOfBoundsException();
        } else if(isFull()) {
            increaseSize();
        }
        shiftToRightFromIndex(index);
        sourceArr[index] = element;
        currentSize++;
    }

    public void remove(T element) {
        if(isEmpty()) {
            return;
        }
        for(int i=0; i<currentCapacity; i++) {
            T currentElement = sourceArr[i];
            if(currentElement.equals(element)) {
                shiftToLeft(i);
                currentCapacity--;
                break;
            }
        }
    }

    public T get(int index) {
        return sourceArr[index];
    }

    public int size() {
        return currentSize;
    }

    public int capacity() {
        return currentCapacity;
    }

    @Override
    public String toString() {
        String str = "";
        for(int i=0; i<currentCapacity; i++) {
            T element = sourceArr[i];
            str += ", " + element;
        }
        if(currentCapacity > 0) {
           str = str.substring(2);
        }
        return "[" + str + "]";
    }

    private boolean isEmpty() {
        return currentSize == 0;
    }

    private void shiftToLeft(int index) {
        if(isLastIndex(index)) {
            sourceArr[currentSize - 1] = null;
        }
        for(int i=index+1; i<currentSize; i++) {
            sourceArr[i-1] = sourceArr[i];
        }
        sourceArr[currentSize - 1] = null;
    }

    private boolean isLastIndex(int index) {
        return index == (currentSize - 1);
    }

    private void shiftToRightFromIndex(int index) {
        if(isEmpty()) {
            return;
        }
        for(int i=currentSize; i > index; i--) {
            sourceArr[i] = sourceArr[i-1];
        }
        sourceArr[index] = null;
    }

    private boolean isFull() {
        return currentSize == currentCapacity;
    }

    private void increaseSize() {
        int newCapacity = currentCapacity * 2;
        T[] tempArr = (T[]) new Object[newCapacity];
        for(int i=0; i<currentCapacity; i++) {
            tempArr[i] = sourceArr[i];
        }
        sourceArr = tempArr;
        currentCapacity = newCapacity;
    }
}
```
Example
```
CustomArrayList<String> arrayList = new CustomArrayList<>();
String [] values = new String [] {"test1", "test2", "test3"};
int index = 0;
for(String value: values) {
    arrayList.add(value);
    output.add("\nAdded '" + value + "' into index '" + index + "'.");
    output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
    output.add("List: "  + arrayList);
    index++;
}
```
Output
```

Added 'test1' into index '0'.
Capacity: 10, Size: 1
List: [test1, null, null, null, null, null, null, null, null, null]

Added 'test2' into index '1'.
Capacity: 10, Size: 2
List: [test1, test2, null, null, null, null, null, null, null, null]

Added 'test3' into index '2'.
Capacity: 10, Size: 3
List: [test1, test2, test3, null, null, null, null, null, null, null]
```
### Add By Index Example
Example
```
CustomArrayList<String> arrayList = new CustomArrayList<>();
String [] values = new String [] {"test1", "test2", "test3"};
int index = 0;
for(String value: values) {
    arrayList.add(value, index);
    output.add("\nAdded '" + value + "' into index '" + index + "'.");
    output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
    output.add("List: "  + arrayList);
    index++;
}
```
Output
```

Added 'test1' into index '0'.
Capacity: 10, Size: 1
List: [test1, null, null, null, null, null, null, null, null, null]

Added 'test2' into index '1'.
Capacity: 10, Size: 2
List: [test1, test2, null, null, null, null, null, null, null, null]

Added 'test3' into index '2'.
Capacity: 10, Size: 3
List: [test1, test2, test3, null, null, null, null, null, null, null]
```
### Add By Index In Between Example
Example
```
String [] values = new String [] {"test1", "test2", "test3"};
CustomArrayList<String> arrayList = new CustomArrayList<>(values);
// Initial
output.add("List: "  + arrayList);
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
// Add Element
arrayList.add("test2.1", 2);
output.add("\nAdded 'test2.1' into index '2'.");
output.add("List: "  + arrayList);
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
// Add Element
arrayList.add("test1.1", 1);
output.add("\nAdded 'test1.1' into index '1'.");
output.add("List: "  + arrayList);
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
// Add Element
arrayList.add("test1.2", 1);
output.add("\nAdded 'test1.2' into index '1'.");
output.add("List: "  + arrayList);
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
// Add Element
arrayList.add("test0", 0);
output.add("\nAdded 'test0' into index '0'.");
output.add("List: "  + arrayList);
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
```
Output
```
List: [test1, test2, test3]
Capacity: 3, Size: 3

Added 'test2.1' into index '2'.
List: [test1, test2, test2.1, test3, null, null]
Capacity: 6, Size: 4

Added 'test1.1' into index '1'.
List: [test1, test1.1, test2, test2.1, test3, null]
Capacity: 6, Size: 5

Added 'test1.2' into index '1'.
List: [test1, test1.2, test1.1, test2, test2.1, test3]
Capacity: 6, Size: 6

Added 'test0' into index '0'.
List: [test0, test1, test1.2, test1.1, test2, test2.1, test3, null, null, null, null, null]
Capacity: 12, Size: 7
```
### Remove Operation
Example
```
// Initialize
String [] values = new String [] {"test1", "test2", "test3", "test4", "test5"};
CustomArrayList<String> arrayList = new CustomArrayList<>(values);
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
output.add("List: "  + arrayList);
// Remove
arrayList.remove("test2");
output.add("\nRemoved 'test2'.");
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
output.add("List: "  + arrayList);
// Remove
arrayList.remove("test5");
output.add("\nRemoved 'test5'.");
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
output.add("List: "  + arrayList);
// Remove
arrayList.remove("test1");
output.add("\nRemoved 'test1'.");
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
output.add("List: "  + arrayList);
// Remove Non-Existing Element
arrayList.remove("test2");
output.add("\nRemoved 'test2'.");
output.add("Capacity: " + arrayList.capacity() + ", Size: " + arrayList.size());
output.add("List: "  + arrayList);
```
Output
```
Capacity: 5, Size: 5
List: [test1, test2, test3, test4, test5]

Removed 'test2'.
Capacity: 4, Size: 5
List: [test1, test3, test4, test5]

Removed 'test5'.
Capacity: 3, Size: 5
List: [test1, test3, test4]

Removed 'test1'.
Capacity: 2, Size: 5
List: [test3, test4]

Removed 'test2'.
Capacity: 2, Size: 5
List: [test3, test4]
```
