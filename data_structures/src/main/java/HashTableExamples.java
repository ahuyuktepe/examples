import resources.CustomHashTable;

import java.util.List;

public class HashTableExamples {

    public void exp_example1(List<String> output) {
        //summary: Insert Example
        //resources: CustomHashTable,CustomHashEntry
        try {
            CustomHashTable<String, String> names = new CustomHashTable<>();
            names.put("Test", "User");
            output.add("Inserted ['Test':'User']");
            String lastName = names.get("Test");
            output.add("Fetched value for key 'Test': " + lastName);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }//end

    public void exp_example2(List<String> output) {
        //summary: Delete Example
        try {
            CustomHashTable<String, String> names = new CustomHashTable<>();
            String key = "Test";
            // Insert
            names.put(key, "User");
            output.add("Inserted ['"+key+"':'User']");
            String value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
            // Delete
            names.delete(key);
            output.add("Deleted value by key '"+key+"'.");
            // Fetch Deleted Value
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }//end

    public void exp_example3(List<String> output) {
        //summary: Multiple Insert With Same Key Example
        try {
            CustomHashTable<String, String> names = new CustomHashTable<>();
            String key = "Tst1";
            String value = "User 1";
            names.put(key, value);
            output.add("Inserted ['"+key+"':'"+value+"']");
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
            // Insert
            key = "Tst2";
            value = "User 2";
            names.put(key, value);
            output.add("Inserted ['"+key+"':'"+value+"']");
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }//end

    public void exp_example4(List<String> output) {
        //summary: Multiple Insert With Same Key Example (With Deleted)
        try {
            CustomHashTable<String, String> names = new CustomHashTable<>();
            String key = "Tst1";
            String value = "User 1";
            names.put(key, value);
            output.add("Inserted ['"+key+"':'"+value+"']");
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
            // Insert
            key = "Tst2";
            value = "User 2";
            names.put(key, value);
            output.add("Inserted ['"+key+"':'"+value+"']");
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
            // Insert
            key = "Tst3";
            value = "User 3";
            names.put(key, value);
            output.add("Inserted ['"+key+"':'"+value+"']");
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
            // Delete
            key = "Tst2";
            names.delete("Tst2");
            output.add("Deleted entry with key '"+key+"'.");
            // Fetch
            key = "Tst3";
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
            // Fetch
            key = "Tst2";
            value = names.get(key);
            output.add("Fetched value for key '"+key+"': " + value);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }//end
}