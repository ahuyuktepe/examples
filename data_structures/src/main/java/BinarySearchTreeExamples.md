### Add Operation Example
Example
```
BinarySearchTree<Integer> tree = new BinarySearchTree<>(10);
tree.add(7);
tree.add(12);
tree.add(11);
tree.add(15);
tree.add(5);
tree.add(8);
output.add(tree.toString());
```
Output
```
 10(1)
  7(2)
   5(3)
    x
    x
   8(3)
    x
    x
  12(2)
   11(3)
    x
    x
   15(3)
    x
    x

```
### Find Operation Example
Example
```
BinarySearchTree<Integer> tree = new BinarySearchTree<>(10);
tree.add(7);
tree.add(12);
tree.add(11);
tree.add(15);
tree.add(5);
tree.add(8);
output.add(tree.toString());
TreeNode<Integer> foundNode = tree.find(5);
output.add("Found Node: " + foundNode);
```
Output
```
 10(1)
  7(2)
   5(3)
    x
    x
   8(3)
    x
    x
  12(2)
   11(3)
    x
    x
   15(3)
    x
    x

Found Node: TreeNode[5]
```
### Delete Operation Example
Example
```
BinarySearchTree<Integer> tree = new BinarySearchTree<>(10);
tree.add(7);
tree.add(12);
tree.add(11);
tree.add(15);
tree.add(5);
tree.add(8);
output.add(tree.toString());
Integer removedData = tree.delete(12);
output.add("Removed Data: " + removedData);
output.add(tree.toString());
```
Output
```
 10(1)
  7(2)
   5(3)
    x
    x
   8(3)
    x
    x
  12(2)
   11(3)
    x
    x
   15(3)
    x
    x

Removed Data: 11
 10(1)
  7(2)
   5(3)
    x
    x
   8(3)
    x
    x
  11(2)
   x
   15(3)
    x
    x

```
