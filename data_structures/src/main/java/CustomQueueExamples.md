### Push Operation Example
Dependencies
```
package resources;

public class CustomQueue<T> {
     private CustomNode<T> top;
     private CustomNode<T> tail;
     private int size;

     public CustomQueue() {
         top = tail = null;
         size = 0;
     }

     public CustomQueue(T [] values) {
         top = tail = null;
         size = 0;
         for(T value: values) {
             push(value);
         }
     }

     public void push(T value) {
         CustomNode<T> newNode = new CustomNode<>(null, value);
         if(isEmpty()) {
            top = tail = newNode;
         } else {
            tail.setNext(newNode);
            tail = newNode;
         }
         size++;
     }

     public T pop() {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> retNode = top;
         top = top.getNext();
         size--;
         return retNode.getData();
     }

     public CustomNode<T> get(T value) {
         if(isEmpty()) {
             return null;
         }
         CustomNode<T> currentNode = top;
         T currentData = null;
         while(currentNode != null) {
            currentData = currentNode.getData();
            if(currentData.equals(value)) {
                break;
            }
            currentNode = currentNode.getNext();
         }
         return currentNode;
     }

     public int size() {
         return size;
     }

     @Override
     public String toString() {
         String str = null;
         if(isEmpty()) {
             str = "";
         } else {
             str = "";
             CustomNode<T> currentNode = top;
             while(!currentNode.isLast()) {
                 str += currentNode.getData() + ", ";
                 currentNode = currentNode.getNext();
             }
             str += currentNode.getData().toString();
         }
         return "[" + str + "]";
     }

     // Private Methods

     private boolean isEmpty() {
         return top == null;
     }
}
```
```
package resources;

public class CustomNode<T> {
    private CustomNode<T> next;
    private T data;

    public CustomNode(CustomNode<T> next, T data) {
        this.next = next;
        this.data = data;
    }

    public CustomNode<T> getNext() {
        return next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setNext(CustomNode<T> next) {
        this.next = next;
    }

    public boolean isLast() {
        return next == null;
    }

    public boolean equalsToData(T data) {
        if(data == null && this.data == null) {
            return true;
        } else if(data != null && data.equals(this.data)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object trgNode) {
        if(trgNode == null) {
            return false;
        }
        T trgData = ((CustomNode<T>) trgNode).getData();
        return equalsToData(trgData);
    }

    @Override
    public String toString() {
       return "[Node: " + data + "]";
    }
}
```
Example
```
CustomQueue<String> queue = new CustomQueue<>();
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Variables
String value = "test1";
// Add
queue.push(value);
output.add("\nAdded value '" + value + "' to the tail of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Add
value = "test2";
queue.push(value);
output.add("\nAdded value '" + value + "' to the tail of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Add
value = "test3";
queue.push(value);
output.add("\nAdded value '" + value + "' to the tail of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
```
Output
```
Size: 0
Queue: []

Added value 'test1' to the tail of the queue.
Size: 1
Queue: [test1]

Added value 'test2' to the tail of the queue.
Size: 2
Queue: [test1, test2]

Added value 'test3' to the tail of the queue.
Size: 3
Queue: [test1, test2, test3]
```
### Pop Operation Example
Example
```
String [] values = new String [] {"test1", "test2", "test3"};
CustomQueue<String> queue = new CustomQueue<>(values);
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Pop
String value = queue.pop();
output.add("\nPopped element '"+value+"' from the top of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Pop
value = queue.pop();
output.add("\nPopped element '"+value+"' from the top of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Pop
value = queue.pop();
output.add("\nPopped element '"+value+"' from the top of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
```
Output
```
Size: 3
Queue: [test1, test2, test3]

Popped element 'test1' from the top of the queue.
Size: 2
Queue: [test2, test3]

Popped element 'test2' from the top of the queue.
Size: 1
Queue: [test3]

Popped element 'test3' from the top of the queue.
Size: 0
Queue: []
```
### Pop & Push Operations Example
Example
```
String [] values = new String [] {"test1", "test2", "test3"};
CustomQueue<String> queue = new CustomQueue<>(values);
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Pop
String value = queue.pop();
output.add("\nPopped element '"+value+"' from the top of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Add
value = "test4";
queue.push(value);
output.add("\nAdded value '" + value + "' to the tail of the queue.");
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
```
Output
```
Size: 3
Queue: [test1, test2, test3]

Popped element 'test1' from the top of the queue.
Size: 2
Queue: [test2, test3]

Added value 'test4' to the tail of the queue.
Size: 3
Queue: [test2, test3, test4]
```
### Get Operation Example
Example
```
String[] values = new String[]{"test1", "test2", "test3"};
CustomQueue<String> queue = new CustomQueue<>(values);
output.add("Size: " + queue.size());
output.add("Queue: " + queue);
// Get
CustomNode<String> node = queue.get("test2");
output.add("\nFetched node: " + node);
```
Output
```
Size: 3
Queue: [test1, test2, test3]

Fetched node: [Node: test2]
```
