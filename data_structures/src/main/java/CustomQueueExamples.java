import resources.CustomNode;
import resources.CustomQueue;
import resources.CustomStack;

import java.util.List;

public class CustomQueueExamples {
    public void exp_example1(List<String> output) {
        //summary: Push Operation Example
        //resources: CustomQueue,CustomNode
        CustomQueue<String> queue = new CustomQueue<>();
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Variables
        String value = "test1";
        // Add
        queue.push(value);
        output.add("\nAdded value '" + value + "' to the tail of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Add
        value = "test2";
        queue.push(value);
        output.add("\nAdded value '" + value + "' to the tail of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Add
        value = "test3";
        queue.push(value);
        output.add("\nAdded value '" + value + "' to the tail of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
    }//end

    public void exp_example2(List<String> output) {
        //summary: Pop Operation Example
        String [] values = new String [] {"test1", "test2", "test3"};
        CustomQueue<String> queue = new CustomQueue<>(values);
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Pop
        String value = queue.pop();
        output.add("\nPopped element '"+value+"' from the top of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Pop
        value = queue.pop();
        output.add("\nPopped element '"+value+"' from the top of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Pop
        value = queue.pop();
        output.add("\nPopped element '"+value+"' from the top of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
    }//end

    public void exp_example3(List<String> output) {
        //summary: Pop & Push Operations Example
        String [] values = new String [] {"test1", "test2", "test3"};
        CustomQueue<String> queue = new CustomQueue<>(values);
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Pop
        String value = queue.pop();
        output.add("\nPopped element '"+value+"' from the top of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Add
        value = "test4";
        queue.push(value);
        output.add("\nAdded value '" + value + "' to the tail of the queue.");
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
    }//end

    public void exp_example4(List<String> output) {
        //summary: Get Operation Example
        String[] values = new String[]{"test1", "test2", "test3"};
        CustomQueue<String> queue = new CustomQueue<>(values);
        output.add("Size: " + queue.size());
        output.add("Queue: " + queue);
        // Get
        CustomNode<String> node = queue.get("test2");
        output.add("\nFetched node: " + node);
    }//end
}