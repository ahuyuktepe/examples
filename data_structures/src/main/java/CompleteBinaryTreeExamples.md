### Add Operation Example
Example
```
BinaryTree<String> tree = new CompleteBinaryTree<>("1");
tree.add("2");
tree.add("3");
tree.add("4");
tree.add("5");
tree.add("6");
tree.add("7");
tree.add("8");
tree.add("9");
output.add(tree.toString());
```
Output
```
 1(1)
  x
  2(2)
   3(3)
    5(4)
     9(5)
      x
      x
     x
    6(4)
     x
     x
   4(3)
    7(4)
     x
     x
    8(4)
     x
     x

```
### Delete Operation Example
Example
```
CompleteBinaryTree<String> tree = new CompleteBinaryTree<>("1");
tree.add("2");
tree.add("3");
tree.add("4");
tree.add("5");
output.add(tree.toString());
TreeNode<String> deletedNode = tree.delete();
output.add("Deleted node : " + deletedNode.getData());
output.add(tree.toString());
```
Output
```
 1(1)
  x
  2(2)
   3(3)
    5(4)
     x
     x
    x
   4(3)
    x
    x

Deleted node : 5
 1(1)
  x
  2(2)
   3(3)
    5(4)
     x
     x
    x
   x

```
### Find Operation Example
Example
```
BinaryTree<String> tree = new CompleteBinaryTree<>("1");
tree.add("2");
tree.add("3");
tree.add("4");
tree.add("5");
tree.add("6");
tree.add("7");
tree.add("8");
tree.add("9");
output.add(tree.toString());
String data = tree.find("7").getData();
output.add("Found Data: " + data);
```
Output
```
 1(1)
  x
  2(2)
   3(3)
    5(4)
     9(5)
      x
      x
     x
    6(4)
     x
     x
   4(3)
    7(4)
     x
     x
    8(4)
     x
     x

Found Data: 7
```
