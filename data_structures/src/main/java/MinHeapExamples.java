import resources.MinHeap;

import java.util.List;

public class MinHeapExamples {
    public void exp_example1(List<String> output) {
        //summary: Add Operation Example
        MinHeap<Integer> heap = new MinHeap<Integer>(8);
        heap.add(18);
        heap.add(66);
        heap.add(20);
        heap.add(28);
        heap.add(90);
        heap.add(10);
        heap.add(5);
        output.add(heap.toString());
    }//end

    public void exp_example2(List<String> output) {
        //summary: Find Operation Example
        MinHeap<Integer> heap = new MinHeap<Integer>(8);
        heap.add(18);
        heap.add(66);
        heap.add(20);
        heap.add(28);
        heap.add(90);
        heap.add(10);
        heap.add(5);
        output.add(heap.toString());
        Integer foundData = heap.find(90);
        output.add("Found: " + foundData);
    }//end

    public void exp_example3(List<String> output) {
        //summary: Remove Operation Example
        MinHeap<Integer> heap = new MinHeap<Integer>(8);
        heap.add(18);
        heap.add(66);
        heap.add(20);
        heap.add(28);
        heap.add(90);
        heap.add(10);
        heap.add(5);
        output.add(heap.toString());
        Integer removedData = heap.remove();
        output.add("Removed Data: " + removedData);
        output.add(heap.toString());
        removedData = heap.remove();
        output.add("Removed Data: " + removedData);
        output.add(heap.toString());
        removedData = heap.remove();
        output.add("Removed Data: " + removedData);
        output.add(heap.toString());
    }//end
}
