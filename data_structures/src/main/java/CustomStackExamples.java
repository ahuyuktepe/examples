import resources.CustomArrayList;
import resources.CustomNode;
import resources.CustomStack;

import java.util.List;

public class CustomStackExamples {
    public void exp_example1(List<String> output) {
        //summary: Push Operation Example
        //resources: CustomStack,CustomNode
        CustomStack<String> stack = new CustomStack<>();
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Variables
        String value = "test1";
        // Add
        stack.push(value);
        output.add("\nAdded value '" + value + "' to the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Add
        value = "test2";
        stack.push(value);
        output.add("\nAdded value '" + value + "' to the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Add
        value = "test3";
        stack.push(value);
        output.add("\nAdded value '" + value + "' to the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
    }//end

    public void exp_example2(List<String> output) {
        //summary: Pop Operation Example
        String [] values = new String [] {"test1", "test2", "test3"};
        CustomStack<String> stack = new CustomStack<>(values);
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Pop
        String value = stack.pop();
        output.add("\nPopped element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Pop
        value = stack.pop();
        output.add("\nPopped element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Pop
        value = stack.pop();
        output.add("\nPopped element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Pop
        value = stack.pop();
        output.add("\nPopped element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
    }//end

    public void exp_example3(List<String> output) {
        //summary: Peek Operation Example
        String [] values = new String [] {"test1", "test2", "test3"};
        CustomStack<String> stack = new CustomStack<>(values);
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Peek
        String value = stack.peek();
        output.add("\nPeeked element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Peek
        value = stack.peek();
        output.add("\nPeeked element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
    }//end

    public void exp_example4(List<String> output) {
        //summary: Pop & Push Operations Example
        String [] values = new String [] {"test1", "test2", "test3"};
        CustomStack<String> stack = new CustomStack<>(values);
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Pop
        String value = stack.pop();
        output.add("\nPopped element '"+value+"' from the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Add
        value = "test4";
        stack.push(value);
        output.add("\nAdded value '" + value + "' to the top of the stack.");
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
    }//end

    public void exp_example5(List<String> output) {
        //summary: Pop Operation Example
        String[] values = new String[]{"test1", "test2", "test3"};
        CustomStack<String> stack = new CustomStack<>(values);
        output.add("Size: " + stack.size());
        output.add("Stack: " + stack);
        // Get
        CustomNode<String> node = stack.get("test2");
        output.add("\nFetched node: " + node);
    }//end
}
