### Add Operation Example
Example
```
MinHeap<Integer> heap = new MinHeap<Integer>(8);
heap.add(18);
heap.add(66);
heap.add(20);
heap.add(28);
heap.add(90);
heap.add(10);
heap.add(5);
output.add(heap.toString());
```
Output
```
 5(1)
  8(2)
   18(3)
    20(4)
   28(3)
  10(2)
   90(3)
   66(3)

```
### Find Operation Example
Example
```
MinHeap<Integer> heap = new MinHeap<Integer>(8);
heap.add(18);
heap.add(66);
heap.add(20);
heap.add(28);
heap.add(90);
heap.add(10);
heap.add(5);
output.add(heap.toString());
Integer foundData = heap.find(90);
output.add("Found: " + foundData);
```
Output
```
 5(1)
  8(2)
   18(3)
    20(4)
   28(3)
  10(2)
   90(3)
   66(3)

Found: 90
```
### Remove Operation Example
Example
```
MinHeap<Integer> heap = new MinHeap<Integer>(8);
heap.add(18);
heap.add(66);
heap.add(20);
heap.add(28);
heap.add(90);
heap.add(10);
heap.add(5);
output.add(heap.toString());
Integer removedData = heap.remove();
output.add("Removed Data: " + removedData);
output.add(heap.toString());
removedData = heap.remove();
output.add("Removed Data: " + removedData);
output.add(heap.toString());
removedData = heap.remove();
output.add("Removed Data: " + removedData);
output.add(heap.toString());
```
Output
```
 5(1)
  8(2)
   18(3)
    20(4)
   28(3)
  10(2)
   90(3)
   66(3)

Removed Data: 5
 8(1)
  18(2)
   20(3)
   28(3)
  10(2)
   90(3)
   66(3)

Removed Data: 8
 10(1)
  18(2)
   20(3)
   28(3)
  66(2)
   90(3)

Removed Data: 10
 18(1)
  20(2)
   90(3)
   28(3)
  66(2)

```
