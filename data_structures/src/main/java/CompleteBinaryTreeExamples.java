import resources.BinaryTree;
import resources.CompleteBinaryTree;
import resources.TreeNode;
import java.util.List;

public class CompleteBinaryTreeExamples {
    public void exp_example1(List<String> output) {
        //summary: Add Operation Example
        BinaryTree<String> tree = new CompleteBinaryTree<>("1");
        tree.add("2");
        tree.add("3");
        tree.add("4");
        tree.add("5");
        tree.add("6");
        tree.add("7");
        tree.add("8");
        tree.add("9");
        output.add(tree.toString());
    }//end

    public void exp_example2(List<String> output) {
        //summary: Delete Operation Example
        CompleteBinaryTree<String> tree = new CompleteBinaryTree<>("1");
        tree.add("2");
        tree.add("3");
        tree.add("4");
        tree.add("5");
        output.add(tree.toString());
        TreeNode<String> deletedNode = tree.delete();
        output.add("Deleted node : " + deletedNode.getData());
        output.add(tree.toString());
    }//end

    public void exp_example3(List<String> output) {
        //summary: Find Operation Example
        BinaryTree<String> tree = new CompleteBinaryTree<>("1");
        tree.add("2");
        tree.add("3");
        tree.add("4");
        tree.add("5");
        tree.add("6");
        tree.add("7");
        tree.add("8");
        tree.add("9");
        output.add(tree.toString());
        String data = tree.find("7").getData();
        output.add("Found Data: " + data);
    }//end
}
