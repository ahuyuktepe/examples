### Add Operation Example
Example
```
BinaryTree<String> tree = new CompleteBinaryTree<>("1");
TreeNode<String> node2 = new TreeNode<>("2");
TreeNode<String> node3 = new TreeNode<>("3");
tree.getRoot().setLeft(node2);
tree.getRoot().setRight(node3);
TreeNode<String> node21 = new TreeNode<>("2.1");
TreeNode<String> node22 = new TreeNode<>("2.2");
node2.setLeft(node21);
node2.setRight(node22);
TreeNode<String> node31 = new TreeNode<>("3.1");
node3.setLeft(node31);
output.add(tree.toString());
```
Output
```
 1(1)
  2(2)
   2.1(3)
    x
    x
   2.2(3)
    x
    x
  3(2)
   3.1(3)
    x
    x
   x

```
